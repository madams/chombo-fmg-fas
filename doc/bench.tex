\documentclass[11pt]{amsart}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\usepackage{caption}

\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\newcommand{\Order}[1]{\ensuremath{\mathcal{O}(#1)}}    % big O notation


\title{Adaptive Mesh Refinement Multigrid Solver Experimental Performance Analysis}
\author{Mark F. Adams}
\thanks{Applied Numerical Algorithms Group, Lawrence Berkeley National Laboratory, Berkeley, CA}
\date{\today}                                           % Activate to display a given date or no date

\begin{document}
\maketitle

\begin{abstract} 
We experimentally analyze the costs of solving cell centered finite difference discretizations of Poisson's equation on block structured grid adaptive mesh refinement (BSAMR) discretizations of the constant coefficient 3D Laplacian on rectangular domain, in the Chombo framework.
We investigate the effect of multigrid parameters, such as cycle type and composite vs. level-by-level grids, with three solver implementations: 1) the native Chombo correction scheme (CS) multigrid solver, 2) a nonlinear full approximation scheme (FAS) multigrid solver, and 3) the native Chombo composite grid matrix with PETSc algebraic multigrid (AMG) CS solver.
We describe the experimental procedures and present results herein; one can obtain the raw data files, parsing scripts, input decks, source codes, and current version of this report at https://bitbucket.org/madams/chombo-fmg-fas.
\end{abstract}

Solver performance is measured in time to solution with respect to the accuracy of the solution, or the algebraic error introduced by approximate solvers.
Multigrid methods have over the past 40 years come to dominate science and industry for scalable solvers (\S \ref{sec:mg}).
Algebraic multigrid (AMG) methods are popular for unstructured mesh problems because they are flexible and amenable to deployment in libraries, such as PETSc~\cite{petsc-user-ref}, hypre~\cite{Falgout02hypre:a}, and Trilinos~\cite{Trilinos-Overview}.
Geometric multigrid (GMG) can achieve so called ``textbook multigrid efficiency" where the algebraic error is reduced to the scale of discretization error (and is hence and asymptotically exact solver) with a few (5-20) work units or the equivalent work of a residual calculation, with the full multigrid (FMG) cycle.
BSAMR is an approach to accommodate complex geometry while exploiting the computational advantages of structure grids \cite{Chombo,SCI:Dub2014a}.

This document analyses performance of multigrid solvers for BSAMR applications with the 3D constant coefficient Laplacian and low order cell centered finite difference discretizations.
We investigate the multigrid algorithmic parameters: 
\begin{itemize}
\item full and V--cycle multigrid, 
\item composite vs. level-by-level methods  \cite{SundarBirosBursteddeEtAl12}, 
\item matrix vs. matrix-free methods, and
\item the extra cost of nonlinear multigrid: full approximate storage (FAS).
\end{itemize}

We use three solver implementations with various capabilities to investigate the effect on performance of multigrid algorithmic parameters: 1) the Chombo release multigrid solver (V-cycles, matrix-free, level-by-level), an FAS (FMG, matrix-free, level-by-level), which can be accessed on the repository with this report, and 3) a Chombo release algebraic multigrid (AMG) solver (V-cycles, stored matrix, composite grid).
The experimental data herein suffers from several defects, such as code bugs, and challenges such as different Chombo implementation of the coarse-fine interpolation operators in the FAS and release CS multigrid codes.
FMG and V-cycles are challenging to compare because accuracy is difficult to compare and, given that applications have specific accuracy needs, impossible to compare in a comprehensive and general way.
Namely, FMG is an non-iterative method that is inherently ``tuned" to achieve order of truncation error accuracy, where as a V--cycle solver is iterative and one must select a convergence criteria.
We have selected a relative reduction in the residual of $10^{-6}$ in this study but the AMR test has 20 levels and a truncation error of about $10^{-16}$.

In addition to these inherent challenges there are some exogenous challenges in the form of logic and performance bugs in the code that can be addressed with future work.
This is a list of the current know issues.

\begin{itemize}
\item {\bf Logic bug in FAS codes residual/apply:} There appears to be a bug in the residual calculation and the FAS-V--cycle asymptotes to a slower convergence rate than the comparable Chombo CS multigrid solver.
The FAS codes operator does not work as the operator for the AMG solver.
The solution is however accurate and one application of FAS-FMG shows quadratic convergence on a fixed refinement AMR test, as desired.
\item {\bf Performance bug in Chombo's residual/apply:}  The AMG solver (should) use the Chombo matrix-free operator in the Krylov solver and the composite matrix for the preconditioner.
We have observed that about 80\% of the solve time for the AMG solver is in this apply operator on 128K cores of Edison.
For this reason we use the composite grid matrix as the operator in these numerical experiments.
This results in accurate solutions, apparently, but the lack of refluxing in the composite grid matrix renders them inadequate as a solver for most applicatons.
\item {\bf FAS codes does not fuse restriction of solution and residual} into one restriction operation (with 2x the data) as it should.
\end{itemize}

\section{Benchmark problem: 3D constant coefficient Laplacian on a rectangle}

We use a multigrid refinement ratio of two, piecewise constant restriction and V--cycle prolongation, linear FMG prolongation (see \S \ref{sec:mg}), 7-point finite difference stencil that is $2^{nd}$ order accurate, four iterations of Gauss-Seidel smoothing for pre and post smoothing.
We use a 27-point finite difference stencil that is $2^{nd}$ order accurate stencil for the AMG solver.
FMG is not an iterative solver but the other solvers use V--cycles with a relative residual convergence tolerance of $10^{-6}$.
We test with a solution $u=\prod\limits_{i=1}^D\left( x_i^4 - L_i^2x_i^2 \right)$, of the Laplacian $Lu=f$, with an isotropic grid, a domain of size $L=(1,2,2)$, cube processor subdomains, and homogenous Dirichlet boundary conditions.
We use a Cray XC30, {\it Edison} at NERSC for the numerical results.

We use the AMR test problem of refining around the center of each grid, such that the logical size of each grid is identical.
Figure \ref{fig:verify} shows a 2D example of one level of refinement for our test problem use for verification.
Figure \ref{fig:fullamr} shows a 2D example of a fully refined test problem used for performance analysis.
\begin{figure}[h]
\begin{minipage}[t]{.49\linewidth}
\includegraphics[width=81mm]{./2D-point-1AMR-error.png} 
\caption{One level of AMR refinement example used for verification}
\label{fig:verify}
\end{minipage}
\begin{minipage}[t]{.49\linewidth}
\includegraphics[width=81mm]{./2D-point-AMR-error.png} 
\caption{Full AMR refinement of model problem used for analysis}
\label{fig:fullamr}
\end{minipage}
\caption*{2D examples of mesh with solution error}
\end{figure}
\section{Verification}

We verify the the correctness of the implementations with one level of AMR refinement, and then uniformly refined, and verify that the error is reduced quadratically with each successive level.
Table \ref{tab:verify} show the convergence rate in the infinity norm and one norm of the error ($2.0$ is perfectly quadratic) of one application of the FMG-FAS solver, the Chombo geometric and composite algebraic CS multigrid solvers.
\begin{table}[h!]
\begin{center}
\begin{tabular}{|c|c|c|c|c|c|}
\hline
\multicolumn{2}{|c|}{FMG-FAS} & 
\multicolumn{2}{c|}{Chombo CS V--cycle} &
\multicolumn{2}{c|}{AMG composite V--cycle} \\
\hline
$|\cdot|_\infty$  &  $|\cdot|_1$ &    $|\cdot|_\infty$ &  $|\cdot|_1$&    $|\cdot|_\infty$& $|\cdot|_1$ \\\hline
1.95  &   2.01 &     1.96   &  2.00 &     1.96   &  1.99 \\\hline
\end{tabular}
\end{center}
\caption{Order of convergence, one AMR level, $4^3$ cells/core on finest grid}
\label{tab:verify}
\end{table}

\pagebreak
\section{Weak scaling of total solve}

The coarse grids that do not fully populate the machine have one $8^3$ cell patch on each core.
Figure \ref{fig:uni} shows uniform grid solve times with one $32^3$ cell patch per core (32K cells/core) for the solvers: the builtin Chombo solver (CS), the PETSc AMG solver, and the FMG and V--cycle FAS solvers.
Figure \ref{fig:amr} shows solve times of an AMR test: the grids that fully populate the machine (except the coarsest) have one $8^3$ cell patch per core per level, these finer grids refine the center of the domain such that each grid has the same total number of patches (i.e., the number of cores).
We use 20 of these AMR levels.
Figure \ref{fig:amr} shows the solve time of the AMR solvers.
\begin{figure}[h]
%\begin{center}
\begin{minipage}[t]{.5\linewidth}
\includegraphics[width=81mm]{../exec/TEST/weak_scaling_uni.jpg} 
\caption{Uniform grid solve times}
\label{fig:uni}
\end{minipage}
\begin{minipage}[t]{.48\linewidth}
\includegraphics[width=81mm]{../exec/TEST/weak_scaling_detail_uni.jpg} 
\caption{Uniform grid solve times detail}
\label{fig:amr}
\end{minipage}
\caption{Weak scaling solve times}
%\end{center}
\end{figure}

\begin{figure}[h]
%\begin{center}
\begin{minipage}[t]{.5\linewidth}
\includegraphics[width=81mm]{../exec/TEST/weak_scaling_amr.jpg} 
\caption{(20 levels) AMR solve times}
\label{fig:amr}
\end{minipage}
\begin{minipage}[t]{.48\linewidth}
\includegraphics[width=81mm]{../exec/TEST/weak_scaling_amr.jpg} 
\caption{(20 levels) AMR solve times}
\label{fig:amr-b}
\end{minipage}
\caption{(20 levels) AMR detail solve times}
%\end{center}
\end{figure}


Figure \ref{fig:uni} also shows the solve time for a simple FAS-FMG solver code HPGMG, which can be accessed at https://bitbucket.org/madams/chombo-fmg-fas.
This shows some data from a uniform grid solver code, which is not encumbered with the AMR overhead of Chombo, providing somewhat of an upper bound on performance for solving this problem.
A better upper bound on Edison performance on the uniform grid can be gleaned from highly the highly optimized codes in the HPGMG project \cite{HPGMGv1}.

%\pagebreak
\section{BSAMR performance: V--cycles}

To investigate the performance characteristics of CS vs. FAS multigrid and of composite vs. level-by-level multigrid we measure the time of five V--cycles of the relevant solvers.
Figure \ref{fig:v-a} compares composite vs. level-by-level multigrid, and Figure \ref{fig:v-b} compares CS vs. FAS multigrid.
\begin{figure}[h]
%\begin{center}
\begin{minipage}[t]{.5\linewidth}
\includegraphics[width=81mm]{../exec/TEST/vcycle_composite_vs_lbl.jpg} 
\caption{Composite (AMG) vs. level-by-level multigrid (Chombo)}
\label{fig:v-a}
\end{minipage}
\begin{minipage}[t]{.48\linewidth}
\includegraphics[width=81mm]{../exec/TEST/vcycle_cs_vs_fas.jpg} 
\caption{CS (Chombo) vs. FAS multigrid}
\label{fig:v-b}
\end{minipage}
\caption*{Weak scaling (20 level) AMR (5) V--cycles}
%\end{center}
\end{figure}

\subsection*{Acknowledgments}
This material is based upon work supported by the U.S. Department of Energy, Office of Science, Office of Advanced Scientific Computing Research and performed under the auspices of the U.S. Department of Energy by Lawrence Berkeley National Laboratory under Contract DE-AC02-05CH11231.
This research used resources of the National Energy Research Scientific Computing Center, which is a DOE Office of Science User Facility.
Authors from Lawrence Berkeley National Laboratory were supported by the U.S. Department of Energy's Advanced Scientific Computing Research Program under contract DEAC02-05CH11231.

\bibliographystyle{siam}
\bibliography{bib}

\appendix


\section{Multigrid Methods}
\label{sec:mg}
Multigrid is an effective method for solving systems of algebraic equations that arise from discretized PDEs.
Modern multigrid's antecedents go back to Southwell in the 1930s \cite{RVSouthwell_1940a}, Fedorenko in the early 1960s \cite{RPFedorenko_1961a}, and others \cite{UTrottenberg_CWOosterlee_ASchueller_2000a}.
Brandt developed multigrid's modern form in the 1970s, with orders of magnitude lower work complexity, equivalent to a few residual calculations (work units) -- textbook multigrid efficiency --  applied to complex domains, variable coefficients and nonlinear problems \cite{ABrandt_1973a}.
A substantial body of literature, both theoretical and experimental, exists that proves and demonstrates the efficacy of multigrid \cite{UTrottenberg_CWOosterlee_ASchueller_2000a,Brandt-2011}.
{\it Full Approximation Scheme} (or Full Approximation Storage, FAS) multigrid as also been demonstrated to be an effective nonlinear solver with costs very similar to that of a linearized multigrid solve (\cite{UTrottenberg_CWOosterlee_ASchueller_2000a} \S 5.3.3, \cite{Adams-10a}, and many others).

Multigrid methods are motivated by the observation that a low resolution discretization of an operator can capture modes or components of the error that are expensive to compute directly on a highly resolved discretization.
More generally, any poorly locally-determined solution component has the potential to be resolved with a coarser representation.  
This process can be applied recursively with a series of coarse ``grids", thereby requiring that each grid resolve only the components of the error that it can resolve efficiently.
This process is known as a V--cycle (see Figure \ref{fig:mgv}).
These coarse grids have fewer grid points, typically a factor of two or more in each dimension.
The total amount of work in the multigrid process is a geometric sum that converges to a small factor of the work on the finest mesh.  
The multigrid approach has proven to be effective in separating the near-field from the far-field contributions in the solution of elliptic operators -- the coarse grid correction captures the far-field contribution and the near-field is resolved with a local process called a \textit{smoother}.
These concepts can be applied to problems with particles/atoms or pixels as well as the traditional grid or cell variables considered here.

\subsection{Differential and discretized problems, grids, and domains}
\label{ssec:defs}

%Following the exposition of Brandt and Diskin \cite{ABrandt_BDiskin_1994a}
We consider general nonlinear elliptic problems in an open domain $\Omega$ with boundary $\partial \Omega$.
The partial differential equation 
\begin{equation} 
\label{eq:eq}
Lu\left(x\right) = f\left(x\right)  \qquad \left(x \in \Omega\right)
\end{equation}
is given where $f$ is a know function, $u$ is unknown, and $L$ is a uniformly elliptic operator.
While the methods described herein are general we restrict ourselves to the 3D Poisson equation: $x=(x_1,x_2,x_3)$, $L=\left( \frac{\partial^2 u}{\partial x_1^2}
      + \frac{\partial^2 u}{\partial x_2^2}
      + \frac{\partial^2 u}{\partial x_3^2} \right)$.
In addition to this interior equation, a suitable boundary condition on $\partial \Omega$ is assumed; we assume $u(x) = 0$; $x \in \partial  \Omega$.

The discretization of equation (\ref{eq:eq}) can be fairly general but we restrict ourselves to cell centered finite difference methods on isotropic Cartesian grids on rectangular domains.
For the grid $\Omega_h$ with mesh spacing $h$ covering the domain $\Omega$, the equation can be written 
\begin{equation} 
\label{eq:deq}
L_hu_h(i) = f_h(i)  \qquad \left(i \in \Omega_h\right)
\end{equation}
where $i = \left( x - \frac{h}{2} \right) / h$ is an integer vector, $x = ih + \frac{h}{2}$ is a cell center, and the boundary $\partial \Omega$ lines up with the cell edges.
In 3D $i = \left(i_1,i_2,i_3\right)$ is an index for a cell in grid $\Omega_h$. 
The indexing in equation \ref{eq:deq} is dropped and field variables (eg, $u_h$) are vectors of scalars.

Our grids $\Omega_h$, and subsequent subdomains, are isotropic and can for the most part be expressed as tensor products of 1D grids.
The size of $\Omega_h$ is an integer vector but we use cubical subdomains and can use the integer $N$.
%The number of cells in each dimension is $N$ and all other grid dimensions are expressed in this form.
Multigrid utilizes a sequence of grids $\Omega_0$, $\Omega_1$, $\Omega_2$,..., $\Omega_M$, where with some abuse of notation $\Omega_k \equiv \Omega_{h_k}$, $h_k = h_{k-1}/2$, $h_M=h$, $N_k = 2N_{k-1}$, $N_M = N$, and an accurate and inexpensive  solver on $\Omega_0$.

\subsection{Multigrid Algorithm}

The multigrid coarse grid space can be represented algebraically as the columns of the {\it prolongation} operator $I^h_{H}$ or $I^k_{k-1}$, where $h$ is the fine grid mesh spacing, and $H$ is the coarse grid mesh spacing. 
The prolongation operator is used to map corrections to the solution from the coarse grid to the fine grid.
Residuals are mapped from the fine grid to the coarse grid with the {\it restriction} operator $I^H_{h}$; $I^H_{h}$ is often equal to the (scaled) transpose of $I^h_{H}$.
The coarse grid matrix can be formed in one of two ways (with some exceptions), either algebraically to form Galerkin (or variational) coarse grids ($L_{H} \leftarrow I^H_{h}L_{h}I^h_{H}$) or, by creating a new operator on each coarse grid if an explicit coarse grid with boundary conditions is available.

Multigrid can be expressed, for linear operators, most simply with {\it Correction Scheme} (CS) multigrid, where coarse grids are used to compute corrections to the solution.
Segmental refinement is more naturally expressed with FAS multigrid.
For solving equation \ref{eq:deq} a coarse grid residual equation can be written as 
\begin{equation} 
r_{H} = L_{H} (u_{H} ) - L_{H} ({\hat u}_{H} ) = L_{H} ({\hat u}_{H} +e_{H} ) - L_{H} ({\hat u}_{H} ),
\label{eq:cresid}
\end{equation}
where $u$ is the exact solution, ${\hat u}_{H}$ approximates $I^H_h{u}_h$, the full solution represented on the coarse grid, and $e$ is the error.
With an approximate solution on the fine grid $\tilde u_h$, the coarse grid equation can be written as
\begin{equation} 
L_{H}\left (I^{H}_h {\tilde u}_h+ e_{H}\right) = L_{H} \left(I^{H}_h{\tilde u}_h\right) + I^{H}_h\left( f_h - L_h {\tilde u}_h \right) = f_H =  I^{H}_h\left( f_h\right) + \tau^{H}_h, 
\label{eq:cresid2}
\end{equation}
and is solved approximately; $\tau^{H}_h$ is the {\it tau correction}, which represents a correction to the coarse grid from the fine grid and is instrumental in the use of evanescent data (\S 15 in \cite{Brandt-2011}), and is convenient in formulating SR algorithms.
After $I^{H}_h {\tilde u}_h$ is subtracted from the $I^{H}_h {\tilde u}_h+ e_{H}$ term the correction is applied to the fine grid with the standard prolongation process.

Figure \ref{fig:mgv} shows FAS multigrid $V(\nu 1,\nu 2)$--cycle with nonlinear smoother $u \leftarrow S(L,u,f)$.
%\vskip .2in
\begin{figure}[h]
\vbox{ \raggedright 
$\phantom{}u=$ {\bf  function} $FASMGV(L_k,u_k,f_k)$ \\ 
$\phantom{MM}${\bf if $k > 0$} \\
$\phantom{MMMM}u_k \leftarrow S^{\nu 1}(L_k,u_k,f_k)$ \quad \qquad : $\nu 1$ iterations of the (pre) smoother \\ 
$\phantom{MMMM}r_k \leftarrow f_k - L_ku_k$ \\ 
$\phantom{MMMM}u_{k-1}\leftarrow {\hat I}^{k-1}_k(u_k)$ \qquad \quad \qquad : restriction of solution to coarse grid\\ 
$\phantom{MMMM}r_{k-1}\leftarrow I^{k-1}_k(r_k)$ \qquad \quad \qquad : restriction of residual to coarse grid\\ 
$\phantom{MMMM}t_{k-1}\leftarrow u_{k-1}$ \qquad \quad  \qquad\qquad : temporary store of coarse grid solution\\
$\phantom{MMMM}w_{k-1}\leftarrow FASMGV(L_{k-1},u_{k-1},r_{k-1}+L_{k-1}u_{k-1})$  \\
$\phantom{MMMM}u_k \leftarrow u_k + I^k_{k-1}(w_{k-1} - t_{k-1})$ \quad : update with correction \\
$\phantom{MMMM}u_k \leftarrow S^{\nu 2}(L_k,u_k,f_k)$ \qquad  : $\nu 2$ iterations of the (post) smoother \\
$\phantom{MM}${\bf else}\\ 
$\phantom{MMMM}u_k \leftarrow L_k^{-1}f_k$ \quad \qquad \qquad \quad : exact solve of coarsest grid \\ 
$\phantom{MM}${\bf return} $u_k$}
\caption{FAS Multigrid {\it $V$-$cycle$} Algorithm}
\label{fig:mgv}
\end{figure}

%With $L$ coarse grids the solver for $L_L u_L = f_L $ is $u = FASMGV(L_L,0,f_L)$.
A lower order restriction operator,  ${\hat I}^{H}_h$, can be used to restrict the solution, if a higher order ${I}^{H}_h$ is used for the residual, because this approximate coarse grid solution is subtracted off of the update to produce an increment and is only needed if the operator is nonlinear.
%(RS: why so?, MA: make sense?)
 
\subsection{Full Multigrid}
\label{ssec:fmg}
An effective V--cycle reduces the error by a constant fraction and is thus an iterative method.
The V--cycle can be used to provably build a non-iterative, asymptotically exact, solver that reduces the algebraic error to the order of the discretization error \cite{Bank-81}.
Moreover, the error is reduced at each level at the same rate as the discretization (eg, quadratically), with \Order{N} work complexity.
FMG starts on the coarsest grid, where an inexpensive accurate solve is available, prolongates the solution to the next finest level, applies a V--cycle, and continues until a desired resolution is reached.
FMG is well know to be the most efficient multigrid cycle, when a textbook efficient V--cycle is available.
%This adds a $log$ power to the computational depth and increases the constant in the asymptotic work complexity slightly.
A higher order interpolator between the level solves, $\Pi^h_{H}$, is useful for optimal efficiency of FMG and is required if ${I}^{H}_h$ is not of sufficient order (eg, $\Pi^h_{H}$ must be at least linear for  cell centered $2^{nd}$ order accurate discretizations whereas ${I}^{H}_h$ can be constant).
%FMG has been proven, and demonstrated, to reduce the error to the order of the discretization error and is thus an asymptotically exact direct method \cite{Bank-81}, \S 3.2.2 in \cite{UTrottenberg_CWOosterlee_ASchueller_2000a}.

%(RS: Need to define M below).
Figure \ref{fig:fmg} shows the full multigrid algorithm,
%\vskip .2in
\begin{figure}[h]
\vbox{ \raggedright 
$\phantom{}u=${\bf  function} $FMG$ \\ 
$\phantom{MM}u_{0} \leftarrow 0$ \\
$\phantom{MM}u_{0} \leftarrow FASMGV\left(L_{0}, u_{0}, f_{0}\right)$ \qquad \quad : exact solve of coarsest grid \\ 
$\phantom{MM}${\bf for k=1:M} \\
$\phantom{MMMM}{u}_{k} \leftarrow \Pi^{k}_{k-1} u_{k-1}$  \qquad \qquad  \qquad \quad : FMG prolongation \\
$\phantom{MMMM}{u}_{k} \leftarrow S^{\alpha}(L_k,u_k,f_k)$ \quad \qquad \qquad \\%: $\alpha$ steps of non-linear smoother\\
$\phantom{MMMM}{u}_{k} \leftarrow FASMGV\left(L_k,u_k,f_k\right)$ \quad : V--cycle\\
$\phantom{MM}${\bf return} $u_0$}
\caption{ Full multigrid algorithm}
\label{fig:fmg}
\end{figure}
with $M$ coarse grids, and $\alpha$ steps of the smoother before each V--cycle, in an F($\alpha$,$\nu 1$,$\nu 2$) cycle.

One can analyze FMG with the induction hypothesis that the ratio $r$ of the algebraic error to the discretization error is below some value, and assume that the discretization error is of the form $Ch^{p}$, where $p$ in the order of the accuracy of the discretization, and that the solver on each level (eg, one V--cycle) reduces the error by some factor $\Gamma$, which can be proven or measured experimentally, to derive the relationship between $\Gamma$ and $r$: $\Gamma = \frac{r}{\left(4r+3\right)}$, with $p=2$ and a refinement ratio of two.
Thus one must have a sufficiently powerful level solver such that $\Gamma < 0.25$.
Adams et. al. applied these ideas to compressible resistive magnetohydrodynamics where two V--cycle were required at each level to achieve sufficient error reduction \cite{Adams-10a}.


\end{document}  
