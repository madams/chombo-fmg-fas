#include "CFInterpMatrix.H"

// This file is automatically generated by matlab.

template<>
const int CFInterpMatrix<4,2,2,0,0>::s_int[] =
{4, 2, 2, 256, 1952, 15};
template<>
const int CFInterpMatrix<4,2,2,0,0>::s_lattice[][2] =
{
  {0,0},
  {-1,0},
  {-2,0},
  {-3,0},
  {-4,0},
  {0,-1},
  {-1,-1},
  {-2,-1},
  {-3,-1},
  {0,-2},
  {-1,-2},
  {-2,-2},
  {0,-3},
  {-1,-3},
  {0,-4}
};
template<>
const int CFInterpMatrix<4,2,2,0,0>::s_matrix[][4] =
{
  {42,210,210,562},
  {176,-176,312,-312},
  {-149,149,-203,203},
  {71,-71,81,-81},
  {-14,14,-14,14},
  {176,312,-176,-312},
  {94,-94,-94,94},
  {-31,31,31,-31},
  {5,-5,-5,5},
  {-149,-203,149,203},
  {-31,31,31,-31},
  {4,-4,-4,4},
  {71,81,-71,-81},
  {5,-5,-5,5},
  {-14,-14,14,14}
};


template<>
const int CFInterpMatrix<4,2,2,1,0>::s_int[] =
{4, 2, 2, 256, 1216, 15};
template<>
const int CFInterpMatrix<4,2,2,1,0>::s_lattice[][2] =
{
  {0,0},
  {1,0},
  {-1,0},
  {-2,0},
  {-3,0},
  {0,-1},
  {1,-1},
  {-1,-1},
  {-2,-1},
  {0,-2},
  {1,-2},
  {-1,-2},
  {0,-3},
  {1,-3},
  {0,-4}
};
template<>
const int CFInterpMatrix<4,2,2,1,0>::s_matrix[][4] =
{
  {70,182,322,450},
  {0,0,-28,28},
  {83,-83,125,-125},
  {-33,33,-39,39},
  {6,-6,6,-6},
  {250,238,-250,-238},
  {-28,28,28,-28},
  {25,-25,-25,25},
  {-3,3,3,-3},
  {-191,-161,191,161},
  {19,-19,-19,19},
  {-4,4,4,-4},
  {81,71,-81,-71},
  {-5,5,5,-5},
  {-14,-14,14,14}
};


template<>
const int CFInterpMatrix<4,2,2,2,0>::s_int[] =
{4, 2, 2, 256, 1272, 15};
template<>
const int CFInterpMatrix<4,2,2,2,0>::s_lattice[][2] =
{
  {0,0},
  {1,0},
  {2,0},
  {-1,0},
  {-2,0},
  {0,-1},
  {1,-1},
  {2,-1},
  {-1,-1},
  {0,-2},
  {1,-2},
  {2,-2},
  {0,-3},
  {1,-3},
  {0,-4}
};
template<>
const int CFInterpMatrix<4,2,2,2,0>::s_matrix[][4] =
{
  {100,152,412,360},
  {-6,6,-82,82},
  {-1,1,13,-13},
  {39,-39,49,-49},
  {-6,6,-6,6},
  {292,196,-292,-196},
  {-64,64,64,-64},
  {11,-11,-11,11},
  {5,-5,-5,5},
  {-203,-149,203,149},
  {31,-31,-31,31},
  {-4,4,4,-4},
  {81,71,-81,-71},
  {-5,5,5,-5},
  {-14,-14,14,14}
};


template<>
const int CFInterpMatrix<4,2,2,0,1>::s_int[] =
{4, 2, 2, 256, 1216, 15};
template<>
const int CFInterpMatrix<4,2,2,0,1>::s_lattice[][2] =
{
  {0,0},
  {-1,0},
  {-2,0},
  {-3,0},
  {-4,0},
  {0,1},
  {-1,1},
  {-2,1},
  {-3,1},
  {0,-1},
  {-1,-1},
  {-2,-1},
  {0,-2},
  {-1,-2},
  {0,-3}
};
template<>
const int CFInterpMatrix<4,2,2,0,1>::s_matrix[][4] =
{
  {70,322,182,450},
  {250,-250,238,-238},
  {-191,191,-161,161},
  {81,-81,71,-71},
  {-14,14,-14,14},
  {0,-28,0,28},
  {-28,28,28,-28},
  {19,-19,-19,19},
  {-5,5,5,-5},
  {83,125,-83,-125},
  {25,-25,-25,25},
  {-4,4,4,-4},
  {-33,-39,33,39},
  {-3,3,3,-3},
  {6,6,-6,-6}
};


template<>
const int CFInterpMatrix<4,2,2,1,1>::s_int[] =
{4, 2, 2, 256, 716, 15};
template<>
const int CFInterpMatrix<4,2,2,1,1>::s_lattice[][2] =
{
  {0,0},
  {1,0},
  {-1,0},
  {-2,0},
  {-3,0},
  {0,1},
  {1,1},
  {-1,1},
  {-2,1},
  {0,-1},
  {1,-1},
  {-1,-1},
  {0,-2},
  {1,-2},
  {0,-3}
};
template<>
const int CFInterpMatrix<4,2,2,1,1>::s_matrix[][4] =
{
  {118,274,274,358},
  {-2,2,-26,26},
  {113,-113,95,-95},
  {-39,39,-33,33},
  {6,-6,6,-6},
  {-2,-26,2,26},
  {-2,2,2,-2},
  {-13,13,13,-13},
  {3,-3,-3,3},
  {113,95,-113,-95},
  {-13,13,13,-13},
  {4,-4,-4,4},
  {-39,-33,39,33},
  {3,-3,-3,3},
  {6,6,-6,-6}
};


template<>
const int CFInterpMatrix<4,2,2,2,1>::s_int[] =
{4, 2, 2, 256, 682, 15};
template<>
const int CFInterpMatrix<4,2,2,2,1>::s_lattice[][2] =
{
  {0,0},
  {1,0},
  {2,0},
  {-1,0},
  {-2,0},
  {0,1},
  {1,1},
  {2,1},
  {-1,1},
  {0,-1},
  {1,-1},
  {2,-1},
  {0,-2},
  {1,-2},
  {0,-3}
};
template<>
const int CFInterpMatrix<4,2,2,2,1>::s_matrix[][4] =
{
  {172,220,340,292},
  {-20,20,-68,68},
  {1,-1,11,-11},
  {49,-49,39,-39},
  {-6,6,-6,6},
  {-8,-20,8,20},
  {-2,2,2,-2},
  {1,-1,-1,1},
  {-5,5,5,-5},
  {125,83,-125,-83},
  {-25,25,25,-25},
  {4,-4,-4,4},
  {-39,-33,39,33},
  {3,-3,-3,3},
  {6,6,-6,-6}
};


template<>
const int CFInterpMatrix<4,2,2,0,2>::s_int[] =
{4, 2, 2, 256, 1272, 15};
template<>
const int CFInterpMatrix<4,2,2,0,2>::s_lattice[][2] =
{
  {0,0},
  {-1,0},
  {-2,0},
  {-3,0},
  {-4,0},
  {0,1},
  {-1,1},
  {-2,1},
  {-3,1},
  {0,2},
  {-1,2},
  {-2,2},
  {0,-1},
  {-1,-1},
  {0,-2}
};
template<>
const int CFInterpMatrix<4,2,2,0,2>::s_matrix[][4] =
{
  {100,412,152,360},
  {292,-292,196,-196},
  {-203,203,-149,149},
  {81,-81,71,-71},
  {-14,14,-14,14},
  {-6,-82,6,82},
  {-64,64,64,-64},
  {31,-31,-31,31},
  {-5,5,5,-5},
  {-1,13,1,-13},
  {11,-11,-11,11},
  {-4,4,4,-4},
  {39,49,-39,-49},
  {5,-5,-5,5},
  {-6,-6,6,6}
};


template<>
const int CFInterpMatrix<4,2,2,1,2>::s_int[] =
{4, 2, 2, 256, 682, 15};
template<>
const int CFInterpMatrix<4,2,2,1,2>::s_lattice[][2] =
{
  {0,0},
  {1,0},
  {-1,0},
  {-2,0},
  {-3,0},
  {0,1},
  {1,1},
  {-1,1},
  {-2,1},
  {0,2},
  {1,2},
  {-1,2},
  {0,-1},
  {1,-1},
  {0,-2}
};
template<>
const int CFInterpMatrix<4,2,2,1,2>::s_matrix[][4] =
{
  {172,340,220,292},
  {-8,8,-20,20},
  {125,-125,83,-83},
  {-39,39,-33,33},
  {6,-6,6,-6},
  {-20,-68,20,68},
  {-2,2,2,-2},
  {-25,25,25,-25},
  {3,-3,-3,3},
  {1,11,-1,-11},
  {1,-1,-1,1},
  {4,-4,-4,4},
  {49,39,-49,-39},
  {-5,5,5,-5},
  {-6,-6,6,6}
};


template<>
const int CFInterpMatrix<4,2,2,2,2>::s_int[] =
{4, 2, 2, 256, 592, 15};
template<>
const int CFInterpMatrix<4,2,2,2,2>::s_lattice[][2] =
{
  {0,0},
  {1,0},
  {2,0},
  {-1,0},
  {-2,0},
  {0,1},
  {1,1},
  {2,1},
  {-1,1},
  {0,2},
  {1,2},
  {2,2},
  {0,-1},
  {1,-1},
  {0,-2}
};
template<>
const int CFInterpMatrix<4,2,2,2,2>::s_matrix[][4] =
{
  {262,250,250,262},
  {-62,62,-26,26},
  {13,-13,-1,1},
  {49,-49,39,-39},
  {-6,6,-6,6},
  {-62,-26,62,26},
  {34,-34,-34,34},
  {-11,11,11,-11},
  {-5,5,5,-5},
  {13,-1,-13,1},
  {-11,11,11,-11},
  {4,-4,-4,4},
  {49,39,-49,-39},
  {-5,5,5,-5},
  {-6,-6,6,6}
};


