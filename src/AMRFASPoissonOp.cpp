#ifdef CH_LANG_CC
/*
 *      _______              __
 *     / ___/ /  ___  __ _  / /  ___
 *    / /__/ _ \/ _ \/  V \/ _ \/ _ \
 *    \___/_//_/\___/_/_/_/_.__/\___/
 *    Please refer to Copyright.txt, in Chombo's root directory.
 */
#endif

// MFA, Sept 3, 2012


#include "FORT_PROTO.H"
#include "AMRFASPoissonOp.H"

#include "AMRFASOpF_F.H"
#include "AverageF_F.H"
#include "ParmParse.H"
//#include "NewPoissonOp4.H"
#include "NewPoissonOp4F_F.H"

#include "NamespaceHeader.H"

// ---------------------------------------------------------
// CFInterp
// ---------------------------------------------------------
void 
FASPoissonOp::CFInterp( LevelData<FArrayBox>& a_phi,
			const LevelData<FArrayBox>& a_phiCoarse )
{
  if( m_interpWithCoarser.isDefined() ) 
    {
      //m_interpWithCoarser.coarseFineInterp( a_phi, a_phiCoarse );
      m_interpWithCoarser.interpolate( a_phi, &a_phiCoarse );
    }
  else CH_assert(0);
}

void 
FASPoissonOp::reflux(const LevelData<FArrayBox>&        a_phiFine, // fine grid to get flux from
		     const LevelData<FArrayBox>&        a_phiCrs,  // this grid function to use?
		     LevelData<FArrayBox>&              a_residual,// output
		     AMRFASOp<LevelData<FArrayBox> >*   a_finerOp )
{
  CH_TIMERS("AMRFAS_LDFOp::reflux");

  m_levfluxreg.setToZero();
  Interval interv(0,a_phiCrs.nComp()-1);

  CH_TIMER("AMRFAS_LDFOp::reflux:incrementCoarse", t2);
  CH_START(t2);

  // get current flux at this level
  DataIterator dit = a_phiCrs.dataIterator();
  for (dit.reset(); dit.ok(); ++dit)
    {
      const FArrayBox& coarfab = a_phiCrs[dit];
      
      if (m_levfluxreg.hasCF(dit()))
        {
          for (int idir = 0; idir < SpaceDim; idir++)
            {
              FArrayBox coarflux; // should take out of loop for eff?
              getFlux( coarflux, coarfab, idir );
	      
              Real scale = 1.0;
              m_levfluxreg.incrementCoarse( coarflux, scale, dit(),
					    interv, interv, idir);
            }
        }
    }
  CH_STOP(t2);

  // cast OK I guess because we're changing ghost cells only
  LevelData<FArrayBox>& phiFineRef = (LevelData<FArrayBox>&)a_phiFine;
  AMRFAS_LDFOp* finerAMRPOp = dynamic_cast<AMRFAS_LDFOp*>( a_finerOp );
  CH_assert(finerAMRPOp);
  // fill in fines ghostcells
  finerAMRPOp->CFInterp( phiFineRef, a_phiCrs );

  IntVect phiGhost = phiFineRef.ghostVect();
  int ncomps = a_phiFine.nComp();

  CH_TIMER("AMRFAS_LDFOp::reflux::incrementFine", t3);
  CH_START(t3);

  // get fine flux
  DataIterator ditf = a_phiFine.dataIterator();
  const  DisjointBoxLayout& dblFine = a_phiFine.disjointBoxLayout();
  for (ditf.reset(); ditf.ok(); ++ditf)
    {
      const FArrayBox& phifFab = a_phiFine[ditf];
      const Box& gridbox = dblFine[ditf];

      for (int idir = 0; idir < SpaceDim; idir++)
        {
          SideIterator sit;
          for (sit.begin(); sit.ok(); sit.next())
            {
              if (m_levfluxreg.hasCF(ditf(), sit()))
                {
                  Side::LoHiSide hiorlo = sit();
                  Box fluxBox = bdryBox(gridbox,idir,hiorlo,1);

                  FArrayBox fineflux(fluxBox,ncomps);
                  getFlux(fineflux, phifFab, idir, fluxBox, m_refToFiner);

                  Real scale = 1.0;
                  m_levfluxreg.incrementFine(fineflux, scale, ditf(),
                                             interv, interv, idir, hiorlo);
                }
            }
        }
    }

  CH_STOP(t3);

  // subtract off contents of flux registers: cellValue -= refluxScale*registerContents
  Real scale = 1.0/m_dx[0]; // assume isotropic mesh
  m_levfluxreg.reflux( a_residual, scale );
}

///
/**
   Poisson Op class -- these two classes should be cloned to add operators
*/
FASPoissonOp::FASPoissonOp( int a_o, const DisjointBoxLayout &a_grid ) : AMRFAS_LDFOp( a_o, a_grid )
{
  m_alpha = 0.0;
  m_beta  = -1.0;
}

/* not coarsest grid */
void
FASPoissonOp::define( const DisjointBoxLayout& a_grids,
		      const DisjointBoxLayout& a_coarse,
		      const RealVect&          a_dxLevel,
		      int                      a_refRatioCrs,
		      const ProblemDomain&     a_domain,
		      BCHolder                 a_bc,
		      const Copier&            a_exchange,
		      const CFRegion&          a_cfregion
		      )
{
  CH_TIME("FASPoissonOp::define");

  AMRFASOp<LevelData<FArrayBox> >::define( a_grids, a_coarse, a_dxLevel, a_refRatioCrs, a_domain,
					   a_bc, a_exchange, a_cfregion );

  // sets ghost cells with coarse grid data
  const int nGhost = m_order/2; // hack for number of ghosts
  m_interpWithCoarser.define( a_grids,   // fine (this) grid
			      a_coarse,  // coarse grid
			      m_domain,  // this domain
			      false,     // homogeneneous ???
			      m_order,   // degree of the fitting polynomial
			      a_refRatioCrs, // refinement ratio between this level and the coarser level
			      nGhost,     // number of layers of ghost cells to fill by interpolation
			      (m_order)/a_refRatioCrs+1, // proper nesting width
			      m_boxstencil,     // Should corner ghosts be interpolated?
			      true,      // if this is false, interpolate the whole ghosted fine patch
			      true       // if this is true, abort when there is not enough cells 
			      );
}

// ---------------------------------------------------------
// applyLevel - apply operator on one level - do BC's but no exchange or C-F
// ---------------------------------------------------------
void FASPoissonOp::applyLevel( LevelData<FArrayBox>& a_lhs,
			       const LevelData<FArrayBox>& a_phi )
{
  CH_TIME("FASPoissonOp::applyLevel");

  LevelData<FArrayBox>& phi = (LevelData<FArrayBox>&)a_phi; // fortran chokes on const

  const DisjointBoxLayout& dbl = a_lhs.disjointBoxLayout();
  DataIterator dit = a_phi.dataIterator();
  for (dit.begin(); dit.ok(); ++dit)
    {
      FArrayBox& phiFab = phi[dit];
      const Box& valid = dbl[dit];
      FArrayBox& lhsFab = a_lhs[dit];

      m_bc( phiFab, valid, m_domain, m_dx[0], true );

      if( m_order == 2 )
	{
	  if (m_boxstencil) {
	    FORT_OPERATORLAPBOX(CHF_FRA(lhsFab),
				CHF_CONST_FRA(phiFab),
				CHF_BOX(valid),
				CHF_CONST_REAL(m_dx[0]),
				CHF_CONST_REAL(m_alpha),
				CHF_CONST_REAL(m_beta));
	  }
	  else {
	    FORT_OPERATORLAP( CHF_FRA(lhsFab),
			      CHF_CONST_FRA(phiFab),
			      CHF_BOX(valid),
			      CHF_CONST_REAL(m_dx[0]),
			      CHF_CONST_REAL(m_alpha),
			      CHF_CONST_REAL(m_beta));
	  }
	}
      else  if( m_order == 4 )
	{
	  FORT_OPERATORLAP4(CHF_FRA(lhsFab),
			    CHF_CONST_FRA(phiFab),
			    CHF_BOX(valid),
			    CHF_CONST_REAL(m_dx[0]),
			    CHF_CONST_REAL(m_alpha),
			    CHF_CONST_REAL(m_beta));
	}
      else
	{
	  MayDay::Abort("FASPoissonOp::applyLevel bad order");
	}
    }
}

// ---------------------------------------------------------
void 
FASPoissonOp::getFlux( FArrayBox&       a_flux,
		       const FArrayBox& a_data,
		       int              a_dir,
		       int              a_ref /* =1 */) const
{
  CH_TIME("AMRFAS_LDFOpp::getFlux");
  
  CH_assert(a_dir >= 0);
  CH_assert(a_dir <  SpaceDim);
  CH_assert(!a_data.box().isEmpty());
  
  int nGhost = m_order/2;
  Box edgebox = surroundingNodes( a_data.box(), a_dir ); // increases upper corner of box 1 in a_dir direction
  edgebox.grow(a_dir, -nGhost);
  // if this fails, the data box was too small (one cell wide, in fact)
  CH_assert( !edgebox.isEmpty() );

  a_flux.resize( edgebox, a_data.nComp() );

  getFlux( a_flux, a_data, a_dir, edgebox, a_ref );
}

// ---------------------------------------------------------
// levelGSRB - ghost cells are filled
// ---------------------------------------------------------
void FASPoissonOp::levelGSRB( RefCountedPtr<LevelData<FArrayBox> > a_phi,
			      const RefCountedPtr<LevelData<FArrayBox> > a_rhs )
{
  CH_TIME("FASPoissonOp::levelGSRB");
  
  CH_assert(a_phi->isDefined());
  CH_assert(a_rhs->isDefined());
  CH_assert(a_phi->ghostVect() >= IntVect::Unit);
  CH_assert(a_phi->nComp() == a_rhs->nComp());

  const DisjointBoxLayout& dbl = a_rhs->disjointBoxLayout();

  DataIterator dit = a_rhs->dataIterator();
  
  // do first red, then black passes
  bool do_exchange = false;
  for (int wpass = 0; wpass <= 1; wpass++ )
    {
      CH_TIME("FASPoissonOp::levelGSRB::Compute");

      if( do_exchange ) {
        CH_TIME("FASPoissonOp::levelGSRB::exchange");
	//a_phi->exchange( m_exchangeCopier ); // Chombo default is no overlap
	a_phi->exchangeNoOverlap( m_exchangeCopier );
      }
      do_exchange = true; // just cheat on first iteration

      for (dit.begin(); dit.ok(); ++dit)
        {
          const Box& valid = dbl[dit];
	  FArrayBox& phiFab = (*a_phi)[dit];
	  const FArrayBox& rhsFAB = (*a_rhs)[dit];
	  // BCs
	  m_bc( phiFab, valid, m_domain, m_dx[0], true );
	  
	  if( m_order == 2 )
	    {
	      if (m_alpha==0. && m_beta==1. && m_smoothing_damping_factor==1.)
		{
		  FORT_GSRBLAPLACIAN( CHF_FRA(phiFab), // not use because I use beta=-1
				      CHF_CONST_FRA(rhsFAB),
				      CHF_BOX(valid),
				      CHF_CONST_REAL(m_dx[0]),
				      CHF_CONST_INT(wpass) );
		}
	      else if (m_alpha == 0.0 && m_beta == -1.0) 
		{
		  FORT_GSRBLAPLACIAN_FAS(CHF_FRA(phiFab),
					 CHF_CONST_FRA(rhsFAB),
					 CHF_BOX(valid),
					 CHF_CONST_REAL(m_dx[0]),
					 CHF_CONST_INT(wpass),
					 CHF_CONST_REAL(m_smoothing_damping_factor)
					 );
		}
	      else if (m_smoothing_damping_factor==1.)
		{
		  FORT_GSRBHELMHOLTZ(CHF_FRA(phiFab),
				     CHF_CONST_FRA(rhsFAB),
				     CHF_BOX(valid),
				     CHF_CONST_REAL(m_dx[0]),
				     CHF_CONST_REAL(m_alpha),
				     CHF_CONST_REAL(m_beta),
				     CHF_CONST_INT(wpass));
		}
	      else
		{
		  FORT_GSRBHELMHOLTZ_FAS(CHF_FRA(phiFab),
					 CHF_CONST_FRA(rhsFAB),
					 CHF_BOX(valid),
					 CHF_CONST_REAL(m_dx[0]),
					 CHF_CONST_REAL(m_alpha),
					 CHF_CONST_REAL(m_beta),
					 CHF_CONST_INT(wpass),
					 CHF_CONST_REAL(m_smoothing_damping_factor));
		}
	    }
	  else if( m_order == 4 )
	    {
	      FArrayBox tmp(valid,1);
	      FORT_GSRBHELMHOLTZ4_FAS(CHF_FRA(phiFab),
				      CHF_CONST_FRA(rhsFAB),
				      CHF_BOX(valid),
				      CHF_CONST_REAL(m_dx[0]),
				      CHF_FRA(phiFab),				      
				      CHF_CONST_REAL(m_alpha),
				      CHF_CONST_REAL(m_beta),
				      CHF_CONST_INT(wpass),
				      CHF_CONST_REAL(m_smoothing_damping_factor));
	    }
	  else
	    {
	      MayDay::Abort("FASPoissonOp::levelGSRB bad order");
	    }

        } // end loop through grids
    } // end loop through red-black

}

// ---------------------------------------------------------
// levelRich - ghost cells are filled
// ---------------------------------------------------------
void FASPoissonOp::levelRich( RefCountedPtr<LevelData<FArrayBox> > a_phi,
			      const RefCountedPtr<LevelData<FArrayBox> > a_rhs )
{
  const DisjointBoxLayout& dbl = a_rhs->disjointBoxLayout();
  CH_TIME("FASPoissonOp::levelRich");
  
  CH_assert(a_phi->isDefined());
  CH_assert(a_rhs->isDefined());
  CH_assert(a_phi->ghostVect() >= IntVect::Unit);
  CH_assert(a_phi->nComp() == a_rhs->nComp());

  if (!m_diag.isDefined()) make_diag(dbl); 
  CH_assert(m_diag.isDefined());

  // BCs
  {
    LevelData<FArrayBox> tmp;
    create( tmp, *a_rhs );

    apply( tmp, *a_phi, 0, false ); // applies BCs, exchange done above

    // x = x + omega D^-1 (b - Ax) -- D == alaph - 2*beta*Dim/h^2
    // axby( tmp, tmp, *a_rhs, -1.0, 1.0 );        
    DataIterator dit = a_rhs->dataIterator();
    for (dit.begin(); dit.ok(); ++dit)
      {
	Box box = dbl[dit];
	FArrayBox& diagFAB = m_diag[dit];
	FArrayBox& tmpFAB = tmp[dit];
	const FArrayBox& rhsFAB = (*a_rhs)[dit];
	FArrayBox& phiFAB = (*a_phi)[dit];

	tmpFAB.minus(rhsFAB,box,0,0,1);	
	tmpFAB.divide(diagFAB,box,0,0,1);
	tmpFAB *= m_smoothing_damping_factor;
	phiFAB.minus(tmpFAB,box,0,0,1);	
      }
    // x = x +...
    // axby( *a_phi, *a_phi, tmp, 1.0, 1.0 );
  }
}

void
FASPoissonOp::make_diag(const DisjointBoxLayout& a_grids) // hard wired for home Diri !!!
{  
  // form diag with linear interp, homo Diri
  
  Real dx2 = m_dx[0]*m_dx[0], diag;
  Real bdry_diag = m_beta/dx2; // estimate of bdiag
  const Box& domainBox = m_domain.domainBox();
  
  m_diag.define(a_grids,1,IntVect::Zero);
  if (!m_boxstencil) diag = m_alpha - m_beta*2.*SpaceDim/dx2;
  else if (SpaceDim==2) diag = m_alpha - m_beta*20./6./dx2;
  else diag = m_alpha - m_beta*64./15./dx2;

  for (DataIterator dit(a_grids); dit.ok(); ++dit) {
    FArrayBox& diagfab = m_diag[dit];
    diagfab.setVal(diag);
    Box valid = a_grids[dit];
    for (int idir = 0; idir < SpaceDim; idir++) {
      if (!m_domain.isPeriodic(idir)) {
	for (SideIterator sit; sit.ok(); ++sit) {
	  Side::LoHiSide side = sit();		
	  if (valid.sideEnd(side)[idir] == domainBox.sideEnd(side)[idir]) {
	    // Dirichlet BC
	    int isign = sign(side);
	    Box toRegion = adjCellBox(valid,idir,side,1);
	    for (BoxIterator bit(toRegion); bit.ok(); ++bit) {
	      IntVect ivTo = bit();
	      IntVect ivClose = ivTo - isign*BASISV(idir);
	      diagfab(ivClose,0) -= bdry_diag;
	    }
	  }
	}
      }
    }
  }
}

// smooth_cheby
// ---------------------------------------------------------
void 
FASPoissonOp::smooth_cheby( RefCountedPtr<LevelData<FArrayBox> > a_phi,
	      const RefCountedPtr<LevelData<FArrayBox> > a_rhs,
	      int &a_numits,
	      bool a_print )
{
  CH_TIME("FASPoissonOp::smooth_cheby");
  int nits = a_numits,m;
  Real au,rhok,beta,alpha2,lMax,lMin,delta,theta,diag,dx2=m_dx[0]*m_dx[0];
  Real sigma,rho_n,rho_nm1,cheby_c1[16],cheby_c2[16],c1,c2;
  LevelData<FArrayBox> tmpLDF;
  LevelData<FArrayBox> *x_np1,*x_n;

  if (!m_boxstencil) diag = m_alpha - m_beta*2.*SpaceDim/dx2;
  else if (SpaceDim==2) diag = m_alpha - m_beta*20./6./dx2;
  else diag = m_alpha - m_beta*64./15./dx2;

  create(tmpLDF,*a_phi); // zero out to avoid invalid read?

  rhok = 2.0; // max eigen of D^-1A
  lMax = 1.0*rhok; 
  lMin = 0.5*rhok;
  theta=(lMax+lMin)/2.;
  delta=(lMax-lMin)/2.;
  sigma = theta/delta;
  rho_n = 1./sigma;
  cheby_c1[0] = 0.;
  cheby_c2[0] = 1./theta;
  if (nits>16) nits = 16;
  for (int m=1; m<nits;m++) {
    rho_nm1 = rho_n;
    rho_n = 1./(2.0*sigma - rho_nm1);
    cheby_c1[m] = rho_n*rho_nm1;
    cheby_c2[m] = rho_n*2./delta;
  }

  const  DisjointBoxLayout& dbl = a_phi->disjointBoxLayout();
  for (m = 0; m < a_numits; m++) {
    if (m%2==0) {
      x_n    = &(*a_phi);
      x_np1  = &tmpLDF;
    }
    else {
      x_n    = &tmpLDF;
      x_np1  = &(*a_phi);
    }
    c1 = cheby_c1[m%16];
    c2 = cheby_c2[m%16];

    x_n->exchange( m_exchangeCopier );

    for (DataIterator dit= a_phi->dataIterator(); dit.ok(); ++dit) {
      FArrayBox &phiFAB =  (*x_n)[dit];
      const FArrayBox &rhsFAB =  (*a_rhs)[dit];
      FArrayBox &destFAB =  (*x_np1)[dit];
      Box valid = dbl[dit];

      m_bc( phiFAB, valid, m_domain, m_dx[0], true );

      // temp needs C-F ghosts (not exch. or bc), all in L1 anyway
      if (m == 0 && a_numits > 1) destFAB.copy(phiFAB);

      if (m_order==2) {
	if (m_boxstencil) {
	  FORT_CHEBYLAP2BOX(CHF_FRA(destFAB),
			    CHF_CONST_FRA(phiFAB),
			    CHF_CONST_FRA(rhsFAB),
			    CHF_BOX(valid),
			    CHF_CONST_REAL(m_dx[0]),
			    CHF_CONST_REAL(diag),
			    CHF_CONST_REAL(m_beta),
			    CHF_CONST_REAL(c1),
			    CHF_CONST_REAL(c2));
	}
	else {
	  FORT_CHEBYLAP2(CHF_FRA(destFAB),
			 CHF_CONST_FRA(phiFAB),
			 CHF_CONST_FRA(rhsFAB),
			 CHF_BOX(valid),
			 CHF_CONST_REAL(m_dx[0]),
			 CHF_CONST_REAL(diag),
			 CHF_CONST_REAL(m_beta),
			 CHF_CONST_REAL(c1),
			 CHF_CONST_REAL(c2));
	}
      }
      else { // HO
	FORT_CHEBYLAP4(CHF_FRA(destFAB),
		       CHF_CONST_FRA(phiFAB),
		       CHF_CONST_FRA(rhsFAB),
		       CHF_BOX(valid),
		       CHF_CONST_REAL(m_dx[0]),
		       CHF_CONST_REAL(diag),
		       CHF_CONST_REAL(m_beta),
		       CHF_CONST_REAL(c1),
		       CHF_CONST_REAL(c2));
      }
      
      // copy back for odd iterates      
      if (m+1 == a_numits && a_numits%2 != 0) phiFAB.copy(destFAB);
    }
  }
}

// ---------------------------------------------------------
void FASPoissonOp::getFlux(FArrayBox&       a_flux,
			   const FArrayBox& a_data,
			   int              a_dir,
			   const Box&       a_edgebox,
			   int              a_ref ) const
{
  // In this version of getFlux, the edgebox is passed in, and the flux array
  // is already defined.

  CH_TIME("FASPoissonOp::getFlux");
  CH_assert(a_dir >= 0 && a_dir <  SpaceDim);
  CH_assert(!a_data.box().isEmpty());
  // if this fails, the data box was too small (one cell wide, in fact)
  CH_assert(!a_edgebox.isEmpty());

  Real scale = m_beta * a_ref / m_dx[0];
  if( m_order == 2 )
    {
      FORT_NEWGETFLUX(CHF_FRA(a_flux),
		      CHF_CONST_FRA(a_data),
		      CHF_BOX(a_edgebox),
		      CHF_CONST_REAL(scale),
		      CHF_CONST_INT(a_dir));
    }
  else  if( m_order == 4 )
    {
      FORT_NEWGETFLUX4( CHF_FRA(a_flux),
			CHF_CONST_FRA(a_data),
			CHF_BOX(a_edgebox),
			CHF_CONST_REAL(scale),
			CHF_CONST_INT(a_dir));
    }
  else
    {
      MayDay::Abort("FASPoissonOp::getFlux bad order");
    }
}

///
/**
   Poisson derived class for FAS operator factory
*/
// ---------------------------------------------------------
//  AMR Factory define function
void FASPoissonOpFactory::define(BCHolder                         a_bc,
				 Real                             a_alpha,
				 Real                             a_beta )
{
  CH_TIME("FASPoissonOpFactory::define");
  
  AMRFAS_LDFOpFactory::define( a_bc );  
  // 
  m_alpha = a_alpha;
  m_beta = a_beta;
}

// ---------------------------------------------------------
// FASPoissonOpFactory::AMRnewOp
RefCountedPtr<AMRFASOp<LevelData<FArrayBox> > > 
FASPoissonOpFactory::AMRNewOp( int a_ilev,
			       const DisjointBoxLayout &a_grid )
{
  CH_TIME("FASPoissonOpFactory::AMRnewOp");

  RefCountedPtr<FASPoissonOp> Op_out = RefCountedPtr<FASPoissonOp>(new FASPoissonOp(m_order,a_grid));

  AMRFAS_LDFOpFactory::AMRNewOp(a_ilev,Op_out);

  Op_out->m_alpha = m_alpha;
  Op_out->m_beta  = m_beta;

  if(a_ilev+1 < m_grids.size()) // not finest
    {
      Op_out->m_levfluxreg.define( m_grids[a_ilev+1],
				   a_grid,
				   m_domains[a_ilev+1],
				   m_refRatios[a_ilev],
				   1 );
    }
  return Op_out;
}
