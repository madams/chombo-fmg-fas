	[0]PCSetFromOptions_GAMG threshold set -5.000000e-03
	[0]PCSetUp_GAMG level 0 N=8388608, n data rows=1, n data cols=1, nnz/row (ave)=26, np=16384
	[0]PCGAMGFilterGraph 100% nnz after filtering, with threshold -0.005, 26.7197 nnz ave. (N=8388608)
[0]PCGAMGCoarsen_AGG square graph
[0]PCGAMGCoarsen_AGG coarsen graph
	[0]maxIndSetAgg removed 0 of 8388608 vertices. (0 local)  151648 selected.
		[0]PCGAMGProlongator_AGG New grid 151648 nodes
			PCGAMGOptprol_AGG smooth P0: max eigen=1.407907e+00 min=3.780860e-03 PC=jacobi
	[0]createLevel number of equations (loc) 9 with simple aggregation
		[0]PCSetUp_GAMG 1) N=151648, n data cols=1, nnz/row (ave)=39, 1024 active pes
	[0]PCGAMGFilterGraph 100% nnz after filtering, with threshold -0.005, 39.4922 nnz ave. (N=151648)
[0]PCGAMGCoarsen_AGG square graph
[0]PCGAMGCoarsen_AGG coarsen graph
	[0]maxIndSetAgg removed 0 of 151648 vertices. (0 local)  2255 selected.
		[0]PCGAMGProlongator_AGG New grid 2255 nodes
			PCGAMGOptprol_AGG smooth P0: max eigen=1.240480e+00 min=8.235352e-03 PC=jacobi
	[0]createLevel number of equations (loc) 4 with simple aggregation
		[0]PCSetUp_GAMG 2) N=2255, n data cols=1, nnz/row (ave)=60, 8 active pes
	[0]PCGAMGFilterGraph 100% nnz after filtering, with threshold -0.005, 60.7481 nnz ave. (N=2255)
[0]PCGAMGCoarsen_AGG square graph
[0]PCGAMGCoarsen_AGG coarsen graph
	[0]maxIndSetAgg removed 0 of 2255 vertices. (0 local)  22 selected.
		[0]PCGAMGProlongator_AGG New grid 22 nodes
			PCGAMGOptprol_AGG smooth P0: max eigen=1.274278e+00 min=6.064694e-02 PC=jacobi
	[0]createLevel number of equations (loc) 22 with simple aggregation
		[0]PCSetUp_GAMG 3) N=22, n data cols=1, nnz/row (ave)=16, 1 active pes
	[0]PCGAMGFilterGraph 100% nnz after filtering, with threshold -0.005, 16.4545 nnz ave. (N=22)
[0]PCGAMGCoarsen_AGG square graph
[0]PCGAMGCoarsen_AGG coarsen graph
	[0]maxIndSetAgg removed 0 of 22 vertices. (0 local)  1 selected.
		[0]PCGAMGProlongator_AGG New grid 1 nodes
			PCGAMGOptprol_AGG smooth P0: max eigen=1.176939e+00 min=7.490080e-01 PC=jacobi
		[0]PCSetUp_GAMG 4) N=1, n data cols=1, nnz/row (ave)=1, 1 active pes
	[0]PCSetUp_GAMG 5 levels, grid complexity = 1.02733
  0 KSP Residual norm 1.366523402049e+03 
  1 KSP Residual norm 1.477328354898e+02 
Linear solve converged due to CONVERGED_RTOL iterations 1
  0 KSP Residual norm 1.366523402049e+03 
  1 KSP Residual norm 1.477328354898e+02 
  2 KSP Residual norm 2.911520036101e+01 
  3 KSP Residual norm 6.143762952933e+00 
  4 KSP Residual norm 1.641909431380e+00 
  5 KSP Residual norm 3.825419728756e-01 
Linear solve did not converge due to DIVERGED_ITS iterations 5
  0 KSP Residual norm 1.366523402049e+03 
  1 KSP Residual norm 1.477328354898e+02 
  2 KSP Residual norm 2.911520036101e+01 
  3 KSP Residual norm 6.143762952933e+00 
  4 KSP Residual norm 1.641909431380e+00 
  5 KSP Residual norm 3.825419728755e-01 
Linear solve did not converge due to DIVERGED_ITS iterations 5
  0 KSP Residual norm 1.366523402049e+03 
  1 KSP Residual norm 1.477328354898e+02 
  2 KSP Residual norm 2.911520036101e+01 
  3 KSP Residual norm 6.143762952933e+00 
  4 KSP Residual norm 1.641909431381e+00 
  5 KSP Residual norm 3.825419728755e-01 
Linear solve did not converge due to DIVERGED_ITS iterations 5
  0 KSP Residual norm 1.366523402049e+03 
  1 KSP Residual norm 1.477328354898e+02 
  2 KSP Residual norm 2.911520036101e+01 
  3 KSP Residual norm 6.143762952933e+00 
  4 KSP Residual norm 1.641909431380e+00 
  5 KSP Residual norm 3.825419728755e-01 
Linear solve did not converge due to DIVERGED_ITS iterations 5
  0 KSP Residual norm 1.366523402049e+03 
  1 KSP Residual norm 1.477328354898e+02 
  2 KSP Residual norm 2.911520036101e+01 
  3 KSP Residual norm 6.143762952933e+00 
  4 KSP Residual norm 1.641909431380e+00 
  5 KSP Residual norm 3.825419728756e-01 
Linear solve did not converge due to DIVERGED_ITS iterations 5
  0 KSP Residual norm 1.366523402049e+03 
  1 KSP Residual norm 1.477328354898e+02 
  2 KSP Residual norm 2.911520036101e+01 
  3 KSP Residual norm 6.143762952933e+00 
  4 KSP Residual norm 1.641909431380e+00 
  5 KSP Residual norm 3.825419728756e-01 
Linear solve did not converge due to DIVERGED_ITS iterations 5
  0 KSP Residual norm 1.366523402049e+03 
  1 KSP Residual norm 1.477328354898e+02 
  2 KSP Residual norm 2.911520036101e+01 
  3 KSP Residual norm 6.143762952933e+00 
  4 KSP Residual norm 1.641909431380e+00 
  5 KSP Residual norm 3.825419728756e-01 
Linear solve did not converge due to DIVERGED_ITS iterations 5
  0 KSP Residual norm 1.366523402049e+03 
  1 KSP Residual norm 1.477328354898e+02 
  2 KSP Residual norm 2.911520036101e+01 
  3 KSP Residual norm 6.143762952933e+00 
  4 KSP Residual norm 1.641909431380e+00 
  5 KSP Residual norm 3.825419728755e-01 
Linear solve did not converge due to DIVERGED_ITS iterations 5
	[0]PCSetFromOptions_GAMG threshold set -5.000000e-03
	[0]PCSetUp_GAMG level 0 N=155189248, n data rows=1, n data cols=1, nnz/row (ave)=33, np=16384
	[0]PCGAMGFilterGraph 100% nnz after filtering, with threshold -0.005, 33.3846 nnz ave. (N=155189248)
[0]PCGAMGCoarsen_AGG square graph
[0]PCGAMGCoarsen_AGG coarsen graph
	[0]maxIndSetAgg removed 0 of 155189248 vertices. (0 local)  2412692 selected.
		[0]PCGAMGProlongator_AGG New grid 2412692 nodes
			PCGAMGOptprol_AGG smooth P0: max eigen=1.407427e+00 min=4.172864e-03 PC=jacobi
	[0]createLevel aggregate processors noop: new_size=16384, neq(loc)=45
		[0]PCSetUp_GAMG 1) N=2412692, n data cols=1, nnz/row (ave)=49, 16384 active pes
	[0]PCGAMGFilterGraph 100% nnz after filtering, with threshold -0.005, 49.3926 nnz ave. (N=2412692)
[0]PCGAMGCoarsen_AGG square graph
[0]PCGAMGCoarsen_AGG coarsen graph
	[0]maxIndSetAgg removed 0 of 2412692 vertices. (0 local)  33752 selected.
		[0]PCGAMGProlongator_AGG New grid 33752 nodes
			PCGAMGOptprol_AGG smooth P0: max eigen=1.367920e+00 min=1.163925e-02 PC=jacobi
	[0]createLevel number of equations (loc) 0 with simple aggregation
		[0]PCSetUp_GAMG 2) N=33752, n data cols=1, nnz/row (ave)=105, 128 active pes
	[0]PCGAMGFilterGraph 100% nnz after filtering, with threshold -0.005, 105.428 nnz ave. (N=33752)
[0]PCGAMGCoarsen_AGG square graph
[0]PCGAMGCoarsen_AGG coarsen graph
	[0]maxIndSetAgg removed 0 of 33752 vertices. (0 local)  180 selected.
		[0]PCGAMGProlongator_AGG New grid 180 nodes
			PCGAMGOptprol_AGG smooth P0: max eigen=1.419352e+00 min=2.208380e-02 PC=jacobi
	[0]createLevel number of equations (loc) 1 with simple aggregation
		[0]PCSetUp_GAMG 3) N=180, n data cols=1, nnz/row (ave)=54, 1 active pes
	[0]PCGAMGFilterGraph 100% nnz after filtering, with threshold -0.005, 54.5389 nnz ave. (N=180)
[0]PCGAMGCoarsen_AGG square graph
[0]PCGAMGCoarsen_AGG coarsen graph
	[0]maxIndSetAgg removed 0 of 180 vertices. (0 local)  3 selected.
		[0]PCGAMGProlongator_AGG New grid 3 nodes
			PCGAMGOptprol_AGG smooth P0: max eigen=1.363892e+00 min=1.378605e-01 PC=jacobi
		[0]PCSetUp_GAMG 4) N=3, n data cols=1, nnz/row (ave)=3, 1 active pes
	[0]PCSetUp_GAMG 5 levels, grid complexity = 1.02369
  0 KSP Residual norm 1.979870294381e+05 
  1 KSP Residual norm 1.725719031144e+03 
Linear solve converged due to CONVERGED_RTOL iterations 1
  0 KSP Residual norm 1.979870294381e+05 
  1 KSP Residual norm 1.723211680251e+03 
  2 KSP Residual norm 7.325938508481e+02 
  3 KSP Residual norm 6.678803536838e+02 
  4 KSP Residual norm 5.143663963812e+02 
  5 KSP Residual norm 3.644781089121e+02 
Linear solve did not converge due to DIVERGED_ITS iterations 5
  0 KSP Residual norm 1.979870294381e+05 
  1 KSP Residual norm 1.725595779204e+03 
  2 KSP Residual norm 7.326345737992e+02 
  3 KSP Residual norm 6.675760427803e+02 
  4 KSP Residual norm 5.141109230122e+02 
  5 KSP Residual norm 3.655902900631e+02 
Linear solve did not converge due to DIVERGED_ITS iterations 5
  0 KSP Residual norm 1.979870294381e+05 
  1 KSP Residual norm 1.726457733501e+03 
  2 KSP Residual norm 7.326492783535e+02 
  3 KSP Residual norm 6.674827266853e+02 
  4 KSP Residual norm 5.140388811461e+02 
  5 KSP Residual norm 3.656619348319e+02 
Linear solve did not converge due to DIVERGED_ITS iterations 5
  0 KSP Residual norm 1.979870294381e+05 
  1 KSP Residual norm 1.725595779204e+03 
  2 KSP Residual norm 7.326345690825e+02 
  3 KSP Residual norm 6.675751715965e+02 
  4 KSP Residual norm 5.141140826166e+02 
  5 KSP Residual norm 3.653368143490e+02 
Linear solve did not converge due to DIVERGED_ITS iterations 5
  0 KSP Residual norm 1.979870294381e+05 
  1 KSP Residual norm 1.725595779204e+03 
  2 KSP Residual norm 7.326345731238e+02 
  3 KSP Residual norm 6.675818326526e+02 
  4 KSP Residual norm 5.141232510042e+02 
  5 KSP Residual norm 3.652691358056e+02 
Linear solve did not converge due to DIVERGED_ITS iterations 5
  0 KSP Residual norm 1.979870294381e+05 
  1 KSP Residual norm 1.725719031144e+03 
  2 KSP Residual norm 7.326366505168e+02 
  3 KSP Residual norm 6.675588017280e+02 
  4 KSP Residual norm 5.141008961020e+02 
  5 KSP Residual norm 3.654566614715e+02 
Linear solve did not converge due to DIVERGED_ITS iterations 5
  0 KSP Residual norm 1.979870294381e+05 
  1 KSP Residual norm 1.725719031144e+03 
  2 KSP Residual norm 7.326366505098e+02 
  3 KSP Residual norm 6.675546748849e+02 
  4 KSP Residual norm 5.140910305169e+02 
  5 KSP Residual norm 3.657858604113e+02 
Linear solve did not converge due to DIVERGED_ITS iterations 5
  0 KSP Residual norm 1.979870294381e+05 
  1 KSP Residual norm 1.726457733501e+03 
  2 KSP Residual norm 7.326492774305e+02 
  3 KSP Residual norm 6.674767591471e+02 
  4 KSP Residual norm 5.140339046884e+02 
  5 KSP Residual norm 3.656372992577e+02 
Linear solve did not converge due to DIVERGED_ITS iterations 5
************************************************************************************************************************
***             WIDEN YOUR WINDOW TO 120 CHARACTERS.  Use 'enscript -r -fCourier9' to print this document            ***
************************************************************************************************************************

---------------------------------------------- PETSc Performance Summary: ----------------------------------------------

../testAMRBC3d.Linux.64.CC.ftn.OPTHIGH.MPI.PETSC.ex on a arch-xc30-opt64-intel named nid03560 with 16384 processors, by madams Thu Mar 12 00:46:44 2015
Using Petsc Release Version 3.5.3, unknown 

                         Max       Max/Min        Avg      Total 
Time (sec):           3.637e+01      1.00129   3.633e+01
Objects:              1.854e+03      1.00108   1.852e+03
Flops:                1.077e+09     46.05228   3.148e+08  5.158e+12
Flops/sec:            2.962e+07     46.00405   8.663e+06  1.419e+11
MPI Messages:         2.460e+05      5.89455   9.455e+04  1.549e+09
MPI Message Lengths:  3.143e+08     63.02532   1.174e+03  1.819e+12
MPI Reductions:       2.096e+03      1.00000

Flop counting convention: 1 flop = 1 real number operation of type (multiply/divide/add/subtract)
                            e.g., VecAXPY() for real vectors of length N --> 2N flops
                            and VecAXPY() for complex vectors of length N --> 8N flops

Summary of Stages:   ----- Time ------  ----- Flops -----  --- Messages ---  -- Message Lengths --  -- Reductions --
                        Avg     %Total     Avg     %Total   counts   %Total     Avg         %Total   counts   %Total 
 0:      Main Stage: 9.2435e+00  25.4%  1.9267e+11   3.7%  1.406e+08   9.1%  2.238e+01        1.9%  1.000e+03  47.7% 
 1:       The solve: 2.7090e+01  74.6%  4.9649e+12  96.3%  1.408e+09  90.9%  1.152e+03       98.1%  1.095e+03  52.2% 

------------------------------------------------------------------------------------------------------------------------
See the 'Profiling' chapter of the users' manual for details on interpreting output.
Phase summary info:
   Count: number of times phase was executed
   Time and Flops: Max - maximum over all processors
                   Ratio - ratio of maximum to minimum over all processors
   Mess: number of messages sent
   Avg. len: average message length (bytes)
   Reduct: number of global reductions
   Global: entire computation
   Stage: stages of a computation. Set stages with PetscLogStagePush() and PetscLogStagePop().
      %T - percent time in this phase         %F - percent flops in this phase
      %M - percent messages in this phase     %L - percent message lengths in this phase
      %R - percent reductions in this phase
   Total Mflop/s: 10e-6 * (sum of flops over all processors)/(max time over all processors)
------------------------------------------------------------------------------------------------------------------------
Event                Count      Time (sec)     Flops                             --- Global ---  --- Stage ---   Total
                   Max Ratio  Max     Ratio   Max  Ratio  Mess   Avg len Reduct  %T %F %M %L %R  %T %F %M %L %R Mflop/s
------------------------------------------------------------------------------------------------------------------------

--- Event Stage 0: Main Stage

Chombo-solve           8 1.0 1.3957e+00 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  4  0  0  0  0  15  0  0  0  0     0
PETSc-AMG-solve        8 1.0 1.0409e+00 1.0 9.83e+0711.0 1.1e+08 1.5e+02 8.8e+01  3  3  7  1  4  11 84 76 45  9 155864
FAS-Vcycle-solve       8 1.0 5.2867e-01 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   6  0  0  0  0     0
Grid setup             2 1.0 2.9536e-01 1.1 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   3  0  0  0  0     0
createMatrix           2 1.0 6.1300e-01 1.0 0.00e+00 0.0 2.3e+06 7.3e+02 3.0e+01  2  0  0  0  1   7  0  2  5  3     0
MatMult              281 1.0 1.7648e-02 2.6 1.92e+07 8.0 4.0e+07 1.6e+02 0.0e+00  0  1  3  0  0   0 23 29 19  0 2558621
MatMultAdd           200 1.0 1.0288e+0016.5 1.05e+06 5.9 7.2e+06 4.2e+01 0.0e+00  3  0  0  0  0  11  2  5  1  0  3332
MatMultTranspose     200 1.0 8.8823e-011020.4 1.05e+06 5.9 7.2e+06 4.2e+01 0.0e+00  0  0  0  0  0   0  2  5  1  0  3860
MatSolve              50 0.0 1.1921e-05 0.0 5.00e+01 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     4
MatSOR               400 1.0 9.3658e-02 6.6 8.36e+0713.2 6.1e+07 1.6e+02 0.0e+00  0  2  4  1  0   0 59 43 29  0 1222637
MatLUFactorSym         1 1.0 7.5102e-05 9.5 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatLUFactorNum         1 1.0 4.6968e-0549.2 1.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatConvert             4 1.0 2.0001e-03 6.6 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatScale              12 1.0 1.1303e-0314.5 3.28e+0511.7 4.1e+05 1.6e+02 0.0e+00  0  0  0  0  0   0  0  0  0  0 468086
MatResidual          200 1.0 1.3370e-02 3.3 1.53e+0712.6 2.0e+07 1.6e+02 0.0e+00  0  0  1  0  0   0 12 14 10  0 1722293
MatAssemblyBegin      78 1.0 3.6565e-01 3.8 0.00e+00 0.0 2.7e+06 9.8e+02 8.8e+01  1  0  0  0  4   4  0  2  8  9     0
MatAssemblyEnd        78 1.0 6.4404e-01 1.0 0.00e+00 0.0 6.7e+06 3.0e+02 2.6e+02  2  0  0  0 12   7  0  5  6 26     0
MatGetRow          14665 5.7 1.6398e-03 7.6 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatGetRowIJ            1 0.0 5.0068e-06 0.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatGetOrdering         1 0.0 2.5034e-05 0.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatCoarsen             4 1.0 2.5046e-02 1.1 0.00e+00 0.0 5.2e+06 3.8e+02 7.3e+01  0  0  0  0  3   0  0  4  6  7     0
MatAXPY                4 1.0 3.5660e-03 1.2 0.00e+00 0.0 0.0e+00 0.0e+00 8.0e+00  0  0  0  0  0   0  0  0  0  1     0
MatTranspose           4 1.0 6.7629e-02 1.1 0.00e+00 0.0 3.0e+06 4.9e+02 4.8e+01  0  0  0  0  2   1  0  2  4  5     0
MatMatMult             4 1.0 3.2803e-02 1.0 3.07e+0512.6 2.2e+06 1.2e+02 6.4e+01  0  0  0  0  3   0  0  2  1  6 14039
MatMatMultSym          4 1.0 2.8512e-02 1.0 0.00e+00 0.0 1.8e+06 1.1e+02 5.6e+01  0  0  0  0  3   0  0  1  1  6     0
MatMatMultNum          4 1.0 4.3442e-03 1.1 3.07e+0512.6 4.1e+05 1.6e+02 8.0e+00  0  0  0  0  0   0  0  0  0  1 106011
MatPtAP                4 1.0 7.1601e-02 1.0 1.31e+0610.9 3.3e+06 2.8e+02 6.8e+01  0  0  0  0  3   1  1  2  3  7 35709
MatPtAPSymbolic        4 1.0 2.7795e-02 1.1 0.00e+00 0.0 2.1e+06 3.8e+02 2.8e+01  0  0  0  0  1   0  0  2  2  3     0
MatPtAPNumeric         4 1.0 4.5013e-02 1.0 1.31e+0610.9 1.2e+06 9.9e+01 4.0e+01  0  0  0  0  2   0  1  1  0  4 56801
MatTrnMatMult          4 1.0 3.4195e-01 1.0 1.89e+0730.1 2.5e+06 3.5e+03 7.6e+01  1  0  0  0  4   4  7  2 25  8 39713
MatTrnMatMultSym       4 1.0 2.1545e-01 1.0 0.00e+00 0.0 2.1e+06 2.2e+03 6.8e+01  1  0  0  0  3   2  0  1 13  7     0
MatTrnMatMultNum       4 1.0 1.2693e-01 1.0 1.89e+0730.1 4.1e+05 1.0e+04 8.0e+00  0  0  0  0  0   1  7  0 12  1 106986
MatGetLocalMat        20 1.0 1.5433e-03 5.8 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatGetBrAoCol         12 1.0 2.5101e-03 3.9 0.00e+00 0.0 2.8e+06 3.3e+02 0.0e+00  0  0  0  0  0   0  0  2  3  0     0
MatGetSymTrans         8 1.0 8.7261e-05 6.3 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecMDot               81 1.0 4.3756e-02 4.8 4.46e+05 2.5 0.0e+00 0.0e+00 8.1e+01  0  0  0  0  4   0  2  0  0  8 67802
VecNorm               94 1.0 2.1562e-02 1.6 1.16e+05 1.9 0.0e+00 0.0e+00 9.4e+01  0  0  0  0  4   0  1  0  0  9 47621
VecScale             694 1.0 4.0030e-04 2.3 1.97e+05 3.1 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  1  0  0  0 4371064
VecCopy               13 1.0 1.8358e-05 6.4 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecSet               839 1.0 4.2605e-04 3.8 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecAXPY               13 1.0 3.3140e-05 6.6 1.51e+04 1.5 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0 5071789
VecAYPX              200 1.0 1.8740e-04 6.5 1.47e+05 5.7 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0 2279261
VecMAXPY              94 1.0 3.4046e-04 4.2 5.47e+05 2.4 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  2  0  0  0 11244842
VecAssemblyBegin      32 1.0 1.3369e-01 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 8.7e+01  0  0  0  0  4   1  0  0  0  9     0
VecAssemblyEnd        32 1.0 4.5061e-05 2.7 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecPointwiseMult      44 1.0 5.4121e-0518.9 3.23e+04 5.7 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0 1736255
VecScatterBegin     1316 1.0 6.6966e-0223.6 0.00e+00 0.0 1.2e+08 1.5e+02 0.0e+00  0  0  8  1  0   0  0 85 52  0     0
VecScatterEnd       1316 1.0 1.1039e+00 1.1 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  3  0  0  0  0  11  0  0  0  0     0
VecSetRandom           4 1.0 8.1301e-0517.1 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecNormalize          94 1.0 2.1685e-02 1.6 1.74e+05 1.9 0.0e+00 0.0e+00 9.4e+01  0  0  0  0  4   0  1  0  0  9 71027
KSPGMRESOrthog        81 1.0 4.3979e-02 4.7 8.93e+05 2.5 0.0e+00 0.0e+00 8.1e+01  0  0  0  0  4   0  3  0  0  8 134986
KSPSetUp              11 1.0 3.4435e-03 1.3 0.00e+00 0.0 0.0e+00 0.0e+00 8.0e+00  0  0  0  0  0   0  0  0  0  1     0
KSPSolve               9 1.0 2.6694e+00 1.0 1.27e+0812.1 1.4e+08 2.4e+02 9.5e+02  7  4  9  2 45  29100 98 95 95 72179
PCGAMGgraph_AGG        4 1.0 1.5326e-01 1.1 3.07e+0512.6 5.5e+06 5.3e+02 1.1e+02  0  0  0  0  5   2  0  4  8 11  3005
PCGAMGcoarse_AGG       4 1.0 4.4894e-01 1.0 1.89e+0730.1 9.8e+06 1.2e+03 2.1e+02  1  0  1  1 10   5  7  7 33 21 30249
PCGAMGProl_AGG         4 1.0 6.3332e-02 1.0 0.00e+00 0.0 1.2e+06 3.0e+02 9.6e+01  0  0  0  0  5   1  0  1  1 10     0
PCGAMGPOpt_AGG         4 1.0 6.8998e-02 1.0 4.20e+0610.2 6.3e+06 1.5e+02 2.0e+02  0  0  0  0 10   1  4  4  3 20 108571
PCSetUp                2 1.0 1.5873e+00 1.0 2.47e+0720.8 2.7e+07 6.3e+02 8.6e+02  4  0  2  1 41  17 13 19 48 86 15176
PCSetUpOnBlocks       50 1.0 1.2827e-04 3.4 1.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
PCApply               50 1.0 1.0615e+00 1.0 1.01e+0812.7 9.6e+07 1.4e+02 0.0e+00  3  3  6  1  0  11 75 68 40  0 136031
SFSetGraph             8 1.0 2.0814e-04 6.2 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
SFBcastBegin          73 1.0 7.2260e-03 3.1 0.00e+00 0.0 5.2e+06 3.8e+02 0.0e+00  0  0  0  0  0   0  0  4  6  0     0
SFBcastEnd            73 1.0 8.6472e-0364.3 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
SFReduceBegin          4 1.0 2.1486e-02 2.7 0.00e+00 0.0 1.0e+06 1.3e+02 0.0e+00  0  0  0  0  0   0  0  1  0  0     0
SFReduceEnd            4 1.0 1.1315e-02753.3 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0

--- Event Stage 1: The solve

Chombo-solve           8 1.0 3.5279e+00 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00 10  0  0  0  0  13  0  0  0  0     0
PETSc-AMG-solve        8 1.0 4.9563e+00 1.0 5.37e+0854.1 4.3e+08 1.0e+03 8.8e+01 14 70 28 24  4  18 72 31 24  8 725970
FAS-Vcycle-solve       8 1.0 8.9921e+00 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00 25  0  0  0  0  33  0  0  0  0     0
MatMult              281 1.0 1.7013e+00 8.7 1.67e+0860.1 1.3e+08 1.3e+03 0.0e+00  1 20  9 10  0   1 21  9 10  0 614491
MatMultAdd           200 1.0 3.9078e+0032.2 6.17e+0630.3 4.1e+07 1.4e+02 0.0e+00 10  1  3  0  0  14  1  3  0  0 16882
MatMultTranspose     200 1.0 3.3716e+0091.4 6.17e+0630.3 4.1e+07 1.4e+02 0.0e+00  0  1  3  0  0   1  1  3  0  0 19566
MatSolve              50 0.0 1.3065e-04 0.0 7.50e+02 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     6
MatSOR               400 1.0 4.1131e+0014.4 3.88e+0855.4 2.5e+08 1.1e+03 0.0e+00  2 49 16 15  0   3 51 18 16  0 618571
MatLUFactorSym         1 1.0 2.5034e-05 6.6 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatLUFactorNum         1 1.0 1.0014e-05 0.0 1.60e+01 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     2
MatConvert             4 1.0 9.3431e-0319.2 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatScale              12 1.0 8.0802e-0339.3 1.80e+0655.6 1.7e+06 1.1e+03 0.0e+00  0  0  0  0  0   0  0  0  0  0 1476035
MatResidual          200 1.0 1.6022e+0016.1 8.41e+0759.5 8.4e+07 1.1e+03 0.0e+00  1 10  5  5  0   1 11  6  5  0 331020
MatAssemblyBegin      72 1.0 2.8432e+00 5.6 0.00e+00 0.0 1.2e+07 6.6e+03 8.0e+01  5  0  1  4  4   7  0  1  4  7     0
MatAssemblyEnd        72 1.0 2.2031e+00 1.0 0.00e+00 0.0 2.9e+07 3.5e+02 2.2e+02  6  0  2  1 11   8  0  2  1 20     0
MatGetRow          5670021.8 7.8728e-0340.4 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatGetRowIJ            1 0.0 7.8678e-06 0.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatGetOrdering         1 0.0 3.0994e-05 0.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatCoarsen             4 1.0 1.1903e+00 1.0 0.00e+00 0.0 8.4e+08 5.8e+02 2.6e+02  3  0 54 26 12   4  0 59 27 24     0
MatAXPY                4 1.0 8.0931e-03 1.4 0.00e+00 0.0 0.0e+00 0.0e+00 8.0e+00  0  0  0  0  0   0  0  0  0  1     0
MatTranspose           4 1.0 2.7005e-01 1.0 0.00e+00 0.0 1.3e+07 3.5e+03 4.8e+01  1  0  1  2  2   1  0  1  2  4     0
MatMatMult             4 1.0 1.0335e-01 1.0 1.68e+0659.5 1.1e+07 7.1e+02 6.4e+01  0  0  1  0  3   0  0  1  0  6 102638
MatMatMultSym          4 1.0 8.3524e-02 1.0 0.00e+00 0.0 9.0e+06 6.4e+02 5.6e+01  0  0  1  0  3   0  0  1  0  5     0
MatMatMultNum          4 1.0 1.9952e-02 1.1 1.68e+0659.5 1.7e+06 1.1e+03 8.0e+00  0  0  0  0  0   0  0  0  0  1 531647
MatPtAP                4 1.0 5.2586e-01 1.2 1.18e+0779.5 2.0e+07 1.5e+03 6.8e+01  1  1  1  2  3   2  1  1  2  6 126320
MatPtAPSymbolic        4 1.0 1.9124e-01 1.7 0.00e+00 0.0 1.1e+07 2.3e+03 2.8e+01  0  0  1  1  1   1  0  1  1  3     0
MatPtAPNumeric         4 1.0 3.3588e-01 1.0 1.18e+0779.5 8.7e+06 4.1e+02 4.0e+01  1  1  1  0  2   1  1  1  0  4 197771
MatTrnMatMult          4 1.0 5.2044e+00 1.0 3.57e+08425.3 1.7e+07 4.0e+04 7.6e+01 14 19  1 37  4  19 20  1 38  7 189745
MatTrnMatMultSym       4 1.0 3.0521e+00 1.0 0.00e+00 0.0 1.5e+07 2.3e+04 6.8e+01  8  0  1 19  3  11  0  1 19  6     0
MatTrnMatMultNum       4 1.0 2.1526e+00 1.0 3.57e+08425.3 1.7e+06 2.0e+05 8.0e+00  6 19  0 18  0   8 20  0 18  1 458741
MatGetLocalMat        20 1.0 1.7372e-0252.3 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatGetBrAoCol         12 1.0 1.1810e-01 3.2 0.00e+00 0.0 1.2e+07 2.5e+03 0.0e+00  0  0  1  2  0   0  0  1  2  0     0
MatGetSymTrans         8 1.0 4.5514e-0428.9 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecMDot               81 1.0 4.4257e-01 6.4 3.85e+0621.3 0.0e+00 0.0e+00 8.1e+01  1  1  0  0  4   1  1  0  0  7 124029
VecNorm               94 1.0 1.1664e-01 6.3 1.32e+0621.1 0.0e+00 0.0e+00 9.4e+01  0  0  0  0  4   0  0  0  0  9 162780
VecScale             694 1.0 6.0189e-0328.1 5.16e+0644.0 0.0e+00 0.0e+00 0.0e+00  0  1  0  0  0   0  1  0  0  0 7395244
VecCopy               13 1.0 4.8590e-0492.6 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecSet               834 1.0 2.7704e-0321.1 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecAXPY               13 1.0 2.2602e-0437.9 2.16e+0521.1 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0 13753947
VecAYPX              200 1.0 1.9007e-0338.5 5.67e+0521.8 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0 4146844
VecMAXPY              94 1.0 3.1514e-0326.2 4.96e+0621.2 0.0e+00 0.0e+00 0.0e+00  0  1  0  0  0   0  1  0  0  0 22457829
VecAssemblyBegin      31 1.0 1.7339e-01 1.2 0.00e+00 0.0 0.0e+00 0.0e+00 8.7e+01  0  0  0  0  4   1  0  0  0  8     0
VecAssemblyEnd        31 1.0 4.1723e-05 2.2 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecPointwiseMult      44 1.0 6.2823e-04125.5 1.25e+0521.8 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0 2760114
VecScatterBegin     1315 1.0 1.3813e+00114.3 0.00e+00 0.0 5.0e+08 1.0e+03 0.0e+00  0  0 32 28  0   0  0 36 29  0     0
VecScatterEnd       1315 1.0 4.8694e+00 1.6 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00 12  0  0  0  0  16  0  0  0  0     0
VecSetRandom           4 1.0 1.5903e-0441.7 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecNormalize          94 1.0 1.1674e-01 6.2 1.99e+0621.1 0.0e+00 0.0e+00 9.4e+01  0  1  0  0  4   0  1  0  0  9 243956
KSPGMRESOrthog        81 1.0 4.4276e-01 6.3 7.70e+0621.3 0.0e+00 0.0e+00 8.1e+01  1  2  0  0  4   1  2  0  0  7 247962
KSPSetUp              11 1.0 2.7082e-03 1.1 0.00e+00 0.0 0.0e+00 0.0e+00 8.0e+00  0  0  0  0  0   0  0  0  0  1     0
KSPSolve               9 1.0 1.4326e+01 1.0 9.50e+0880.6 1.4e+09 1.3e+03 1.1e+03 39 96 91 98 52  53100100100 99 346574
PCGAMGgraph_AGG        4 1.0 6.6921e-01 1.0 1.68e+0659.5 2.3e+07 3.8e+03 1.1e+02  2  0  1  5  5   2  0  2  5 10 15851
PCGAMGcoarse_AGG       4 1.0 6.5378e+00 1.0 3.57e+08425.3 8.7e+08 1.3e+03 4.0e+02 18 19 56 65 19  24 20 62 66 37 151045
PCGAMGProl_AGG         4 1.0 1.4453e-01 1.2 0.00e+00 0.0 1.3e+07 1.0e+03 9.6e+01  0  0  1  1  5   1  0  1  1  9     0
PCGAMGPOpt_AGG         4 1.0 1.9544e-01 1.0 2.17e+0747.3 2.8e+07 9.5e+02 2.0e+02  1  3  2  1 10   1  3  2  1 18 826378
PCSetUp                2 1.0 9.1828e+00 1.0 3.92e+08265.7 9.6e+08 1.4e+03 1.0e+03 25 24 62 73 47  34 25 68 75 91 133515
PCSetUpOnBlocks       50 1.0 9.4891e-05 3.3 1.60e+01 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
PCApply               50 1.0 4.9337e+00 1.1 4.84e+0854.8 4.2e+08 9.2e+02 0.0e+00 13 62 27 21  0  18 65 30 22  0 649927
SFSetGraph             8 1.0 7.3583e-0368.7 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
SFBcastBegin         260 1.0 3.0897e-01 4.5 0.00e+00 0.0 8.4e+08 5.8e+02 0.0e+00  0  0 54 26  0   1  0 59 27  0     0
SFBcastEnd           260 1.0 5.3374e-01 9.9 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   1  0  0  0  0     0
SFReduceBegin          4 1.0 1.7042e-02 3.5 0.00e+00 0.0 4.2e+06 8.9e+02 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
SFReduceEnd            4 1.0 2.0328e-0312.5 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
------------------------------------------------------------------------------------------------------------------------

Memory usage is given in bytes:

Object Type          Creations   Destructions     Memory  Descendants' Mem.
Reports information only for process 0.

--- Event Stage 0: Main Stage

              Matrix   145            144     57390488     0
      Matrix Coarsen     4              4         2768     0
              Vector   627            625      4433128     0
      Vector Scatter    39             39        47204     0
           Index Set   101            101       302648     0
       Krylov Solver    13             12       177664     0
      Preconditioner    13             12        12664     0
              Viewer     1              0            0     0
Star Forest Bipartite Graph     8              8         7424     0
         PetscRandom     4              4         2720     0

--- Event Stage 1: The solve

              Matrix   129            130    195884840     0
      Matrix Coarsen     4              4         2768     0
              Vector   613            615     16830728     0
      Vector Scatter    34             34        40680     0
           Index Set    85             85      1111112     0
       Krylov Solver    11             12       177664     0
      Preconditioner    11             12        12664     0
Star Forest Bipartite Graph     8              8         7424     0
         PetscRandom     4              4         2720     0
========================================================================================================================
Average time to get PetscTime(): 9.53674e-08
Average time for MPI_Barrier(): 3.86238e-05
Average time for zero size MPI_Send(): 1.159e-06
#PETSc Option Table entries:
-amg true
-bc_order 1
-box_stencil false
-chmg true
-chombo_bottom_amg false
-crs_box_size 8
-domain_length 1,2,2
-fine_box_size 8
-fmg false
-ksp_converged_reason
-ksp_gmres_restart 50
-ksp_max_it 50
-ksp_monitor
-ksp_norm_type preconditioned
-ksp_type gmres
-log_summary
-mat_partitioning_type parmetis
-mg_coarse_ksp_type preonly
-mg_levels_ksp_type richardson
-mg_levels_pc_type sor
-n_amr_refine 100
-n_coarse_patches 16
-n_inc 20
-nlevels 20
-npost 2
-npre 2
-num_repeat 8
-pc_gamg_agg_nsmooths 1
-pc_gamg_coarse_eq_limit 10
-pc_gamg_process_eq_limit 200
-pc_gamg_repartition false
-pc_gamg_square_graph true
-pc_gamg_sym_graph true
-pc_gamg_threshold -.005
-pc_gamg_verbose 2
-pc_type gamg
-refinement_type point
-smoother_type gsrb
-vcycle_type 2
-verbosity 2
#End of PETSc Option Table entries
Compiled without FORTRAN kernels
Compiled with full precision matrices (default)
sizeof(short) 2 sizeof(int) 4 sizeof(long) 8 sizeof(void*) 8 sizeof(PetscScalar) 8 sizeof(PetscInt) 8
Configure options: --COPTFLAGS="-fast -no-ipo" --CXXOPTFLAGS="-fast -no-ipoi" --FOPTFLAGS="-fast -no-ipo" --download-hypre --download-parmetis --download-metis --with-cc=cc --with-clib-autodetect=0 --with-cxx=CC --with-cxxlib-autodetect=0 --with-debugging=0 --with-fc=ftn --with-fortranlib-autodetect=0 --with-hdf5-dir=/opt/cray/hdf5-parallel/1.8.13/intel/140/ --with-shared-libraries=0 --with-x=0 --with-mpiexec=aprun LIBS=-lstdc++ --with-64-bit-indices PETSC_ARCH=arch-xc30-opt64-intel
-----------------------------------------
Libraries compiled on Sat Feb 14 05:21:21 2015 on edison02 
Machine characteristics: Linux-3.0.101-0.46-default-x86_64-with-SuSE-11-x86_64
Using PETSc directory: /global/homes/m/madams/petsc_maint
Using PETSc arch: arch-xc30-opt64-intel
-----------------------------------------

Using C compiler: cc  -fast -no-ipo  ${COPTFLAGS} ${CFLAGS}
Using Fortran compiler: ftn  -fast -no-ipo   ${FOPTFLAGS} ${FFLAGS} 
-----------------------------------------

Using include paths: -I/global/homes/m/madams/petsc_maint/arch-xc30-opt64-intel/include -I/global/homes/m/madams/petsc_maint/include -I/global/homes/m/madams/petsc_maint/include -I/global/homes/m/madams/petsc_maint/arch-xc30-opt64-intel/include -I/global/u2/m/madams/petsc_maint/arch-xc30-opt64-intel/include -I/opt/cray/hdf5-parallel/1.8.13/intel/140/include
-----------------------------------------

Using C linker: cc
Using Fortran linker: ftn
Using libraries: -Wl,-rpath,/global/homes/m/madams/petsc_maint/arch-xc30-opt64-intel/lib -L/global/homes/m/madams/petsc_maint/arch-xc30-opt64-intel/lib -lpetsc -Wl,-rpath,/global/u2/m/madams/petsc_maint/arch-xc30-opt64-intel/lib -L/global/u2/m/madams/petsc_maint/arch-xc30-opt64-intel/lib -lHYPRE -lparmetis -lmetis -lpthread -lssl -lcrypto -Wl,-rpath,/opt/cray/hdf5-parallel/1.8.13/intel/140/lib -L/opt/cray/hdf5-parallel/1.8.13/intel/140/lib -lhdf5hl_fortran -lhdf5_fortran -lhdf5_hl -lhdf5 -ldl -lstdc++ 
-----------------------------------------

Application 10700419 resources: utime ~599649s, stime ~13282s, Rss ~861852, inblocks ~38745361, outblocks ~109555772
