%
% plot convergence test
%
close all
%set(0,'DefaultFigureWindowStyle','docked')
lw = 3.0; fz = 18; 
set(0,'DefaultTextFontSize',fz)
set(0,'DefaultAxesFontSize',fz)
set(0,'defaultlinelinewidth',lw)
%
% weak SCALING full AMR (18.5 patches/core?)
%
figure
% str(nprocs_scale[idx])+' '+str(FAS_F_time_scale[idx])+' '+str(CH_V_time_scale[idx])+' '+str(AMG_time_scale[idx])+' '+str(AMG_its[idx])
load scaling.dat
scaling(:,5) = scaling(:,4) ./ scaling(:,5);
% fout.write(str(nprocs_amg_bot[idx])+' '+str(CH_V_time_amg_bot[idx])
semilogx(scaling(:,1),scaling(:,2),'b-.h'), hold on % FAS - blue
semilogx(scaling(:,1),scaling(:,3),'r--*'), hold on % Chombo - red
semilogx(scaling(:,1),scaling(:,4),'k-.s'), hold on % AMG - black
set(gca,'XTick',scaling(:,1))
V = axis;
V(1) = scaling(1,1); V(2) = scaling(end,1); V(3) = 0; 
axis(V)
AX=legend('FAS-FMG (Chombo add on)','CS-V-cycle (Chombo release)','AMG V-cycle (Chombo release)',2);
LEG = findobj(AX,'type','text');
set(LEG,'FontSize',fz)
ylabel('Solve Time (8 solves)');
xlabel('# Edison cores');   
title('3D AMR scaling, 9,472 cells/core')
%grid
print( gcf, '-djpeg100', 'weak_scaling_amr' )
%
% DETAIL
%
%figure
%V = axis;
%semilogx(scaling(:,1),scaling(:,2),'b-.h'), hold on % FAS - blue
%semilogx(scaling(:,1),scaling(:,3),'r--*'), hold on % Chombo - red
%semilogx(scaling(:,1),scaling(:,4),'k-.s'), hold on % AMG - black
%set(gca,'XTick',scaling(:,1))
%max_solve=25;
%V(1) = scaling(1,1); V(2) = scaling(end,1); V(3) = 0; V(4) = max_solve;
%axis(V)
%grid
%AX=legend('FAS-FMG','CS-V-cycle (Chombo release)','AMG V-cycle (Chombo release)',2);
%LEG = findobj(AX,'type','text');
%set(LEG,'FontSize',fz)
%ylabel('Solve Time (8 solves)');
%xlabel('# Edison cores');   
%title('3D AMR scaling detail, 9,472 cells/core')
%print(gcf, '-djpeg100', 'weak_scaling_detail_amr')
%
% weak scaling UNIFORM grids
%
figure
load uniform.dat
[m n] = size(uniform);
load srgmg.dat
% 2:str(FAS_F_time_uni[idx])+' '+ 3:str(CH_V_time_uni[idx])+' '+ 4:str(AMG_time_uni[idx])+' '+ 5: str(FAS_V_time_uni[idx])
semilogx(uniform(:,1),uniform(:,3),'r--*'), hold on % Chombo - red
if uniform(m,5)==0,
  semilogx(uniform(1:m-1,1),uniform(1:m-1,5),'k-.s'), hold on % AMG - black  
else
  semilogx(uniform(:,1),uniform(:,5),'k-.s'), hold on % AMG - black
end
semilogx(uniform(:,1),uniform(:,2),'b-.h'), hold on % FAS - blue
semilogx(uniform(:,1),uniform(:,4),'b:o'), hold on  % FAS-V
semilogx(srgmg(:,1),srgmg(:,2),    'm-.o'), hold on % HPGMG - megenta
set(gca,'XTick',srgmg(:,1))
V = axis;
V(1) = srgmg(1,1); V(2) = srgmg(end,1);  V(3) = 0; V(4) = 7;
axis(V)
AX=legend('CS V-cycle (Chombo release)','AMG V-cycle (Chombo release)','FAS - FMG (Chombo add on)','FAS - V-cycle (Chombo add on)','HPGMG FAS-FMG',2);
LEG = findobj(AX,'type','text');
set(LEG,'FontSize',fz)
ylabel('Solve Time (8 solves)');
xlabel('# Edison cores');   
title('3D uniform grid scaling, 32K cells/core')
%grid
print( gcf, '-djpeg100', 'weak_scaling_uni' )
%
% DETAIL - FMG
%
figure
semilogx(uniform(:,1),uniform(:,2),'b-.h'), hold on % FAS - blue
semilogx(srgmg(:,1),srgmg(:,2),    'm-.o'), hold on % HPGMG - megenta
semilogx(uniform(:,1),uniform(:,3),'r--*'), hold on % Chombo - red
set(gca,'XTick',srgmg(:,1))
V = axis;
V(1) = srgmg(1,1); V(2) = srgmg(end,1);  V(3) = 0; V(4) = 1;
axis(V)
AX=legend('Chombo FAS-FMG (not released)','HPGMG FAS-FMG','Chombo CS-V-cycle (release)',2);
LEG = findobj(AX,'type','text');
set(LEG,'FontSize',fz)
ylabel('Solve Time (8 solves)');
xlabel('# Edison cores');   
title('3D uniform grid scaling detail, 32K cells/core')
print( gcf, '-djpeg100', 'weak_scaling_detail_uni' )
%
% weak VCYCLE
%
max_solve=20;
load vcycle.dat
% 2: str(CH_V_time_v[idx])+' '+ 3: str(AMG_time_v[idx])+' '+ 4: str(FAS_V_time_v[idx])
%
% composit vs LbL - AMG & CH
%
figure
semilogx(vcycle(:,1),vcycle(:,2),'r-.h'), hold on % CH
semilogx(vcycle(:,1),vcycle(:,3),'k-.*'), hold on % AMG
set(gca,'XTick',vcycle(:,1))
V = axis;
V(1) = vcycle(1,1); V(2) = vcycle(end,1);  V(3) = 0;
axis(V)
AX=legend('Level-by-level GMG','Compisite grid AMG',2);
LEG = findobj(AX,'type','text');
set(LEG,'FontSize',fz)
ylabel('V-Cycle (5 its) Time (x8)');
xlabel('# Edison cores');   
title('3D AMR Level-by-level vs. Composite grid, 9,472 cells/core');
%rid
print( gcf, '-djpeg100', 'vcycle_composite_vs_lbl' )
%
% CS .vs. FAS
%
figure
semilogx(vcycle(:,1),vcycle(:,2),'r-.h'), hold on % CH
semilogx(vcycle(:,1),vcycle(:,4),'b-.s'), hold on % FAS-V
set(gca,'XTick',vcycle(:,1))
V = axis;
V(1) = vcycle(1,1); V(2) = vcycle(end,1);  V(3) = 0;
axis(V)
AX=legend('CS V-cycle','FAS - V-cycle',2);
LEG = findobj(AX,'type','text');
set(LEG,'FontSize',fz)
ylabel('V-Cycle (5 its) Time (x8)');
xlabel('# Edison cores');   
title('3D AMR CS vs. FAS multigrid, 9,472 cells/core')
%grid
print( gcf, '-djpeg100', 'vcycle_cs_vs_fas' )

