#PBS -q regular
#PBS -l mppwidth=131072
#PBS -l walltime=0:60:00
#PBS -N hpgmg-chombo
#PBS -V
#PBS -j eo
##PBS -A m499

cd $PBS_O_WORKDIR

#
# June PM fixes ???
#
export MPICH_GNI_MDD_SHARING=disabled
module load craype-hugepages2M

date
export ex='../testAMRBC3d.Linux.64.CC.ftn.OPTHIGH.MPI.PETSC.ex'

#
# HPGMG - my uniform grid code
#
export ARGS='-dom_x_hi 2.0 -dom_y_hi 2.0 -verbose 2 -show_options true -nsmoothsfmg 0 -nsmoothsdown 2 -nsmoothsup 2 -out_error true -problem_type 0 -smoother_type 0 -num_solves 8'
echo "start HPGMG"
aprun -n 4 ../srgmg ${ARGS} -nxloc 32  -nxpe 2 -nype 2 -nzpe 1          >& out.000004.srgmg.txt
date
aprun -n 32 ../srgmg ${ARGS} -nxloc 32  -nxpe 4 -nype 4 -nzpe 2         >& out.000032.srgmg.txt
date
aprun -n 256 ../srgmg ${ARGS} -nxloc 32  -nxpe 8 -nype 8 -nzpe 4        >& out.000256.srgmg.txt
date
aprun -n 2048 ../srgmg ${ARGS} -nxloc 32  -nxpe 16 -nype 16 -nzpe 8     >& out.002048.srgmg.txt
date
aprun -n 16384 ../srgmg ${ARGS} -nxloc 32  -nxpe 32 -nype 32 -nzpe 16   >& out.016384.srgmg.txt
date
aprun -n 131072 ../srgmg ${ARGS} -nxloc 32  -nxpe 64 -nype 64 -nzpe 32  >& out.131072.srgmg.txt
date
#
# point VCYCLE = 5
#
export ARGS='-log_summary -fmg false -vcycle_type 2 -refinement_type point -nlevels 20 -n_inc 20 -n_amr_refine 100 -crs_box_size 8 -fine_box_size 8 -num_repeat 8'
echo "start VCYCLE"
aprun -n 256 ${ex} ${ARGS} -n_coarse_patches 4 >& out.000256.vcycle.txt
/bin/mv pout.0 pout.000256.vcycle.dummy
date
aprun -n 2048 ${ex} ${ARGS} -n_coarse_patches 8 >& out.002048.vcycle.txt 
/bin/mv pout.0 pout.002048.vcycle.dummy
date
aprun -n 16384 ${ex} ${ARGS} -n_coarse_patches 16 >& out.016384.vcycle.txt
/bin/mv pout.0 pout.016384.vcycle.dummy
date
aprun -n 131072 ${ex} ${ARGS} -n_coarse_patches 32 >& out.131072.vcycle.txt
/bin/mv pout.0 pout.131072.vcycle.dummy
date
#
# UNIFORM scaling (32K/core)
#
export ARGS='-log_summary -vcycle_type 1 -refinement_type uniform -nlevels 0 -crs_box_size 8 -fine_box_size 32 -num_repeat 8'
echo "start UNIFORM"
aprun -n 4 ${ex} ${ARGS} -n_coarse_patches 1 >& out.000004.uniform.txt
/bin/mv pout.0 pout.000004.uniform.dummy
/bin/mv time.table.0 time.table.000004.uniform.0
date
aprun -n 32 ${ex} ${ARGS} -n_coarse_patches 2 >& out.000032.uniform.txt
/bin/mv pout.0 pout.000032.uniform.dummy
/bin/mv time.table.0 time.table.000032.uniform.0
date
aprun -n 256 ${ex} ${ARGS} -n_coarse_patches 4 >& out.000256.uniform.txt
/bin/mv pout.0 pout.000256.uniform.dummy
/bin/mv time.table.0 time.table.000256.uniform.0
date
aprun -n 2048 ${ex} ${ARGS} -n_coarse_patches 8 >& out.002048.uniform.txt 
/bin/mv pout.0 pout.002048.uniform.dummy
/bin/mv time.table.0 time.table.002048.uniform.0
date
aprun -n 16384 ${ex} ${ARGS} -n_coarse_patches 16 >& out.016384.uniform.txt
/bin/mv pout.0 pout.016384.uniform.dummy
/bin/mv time.table.0 time.table.016384.uniform.0
date
aprun -n 131072 ${ex} ${ARGS} -n_coarse_patches 32 -amg false >& out.131072.uniform.txt
/bin/mv pout.0 pout.131072.uniform.dummy
/bin/mv time.table.0 time.table.131072.uniform.0
date
#
# point VERIFY - FAS-FMG, CH, FAS-V
#
export ARGS='-log_summary -vcycle_type 0 -refinement_type point -n_amr_refine 1 -nlevels 3 -crs_box_size 4 -fine_box_size 8 -num_repeat 1'
echo "start VERIFY"
aprun -n 256 ${ex} ${ARGS} -n_coarse_patches 4 >& out.000256.verify.dummy.txt
/bin/mv pout.0 pout.000256.verify.0
date
aprun -n 2048 ${ex} ${ARGS} -n_coarse_patches 8 >& out.002048.verify.dummy.txt 
/bin/mv pout.0 pout.002048.verify.0
date
aprun -n 16384 ${ex} ${ARGS} -n_coarse_patches 16 >& out.016384.verify.dummy.txt
/bin/mv pout.0 pout.016384.verify.0
date
aprun -n 131072 ${ex} ${ARGS} -n_coarse_patches 32 -amg false >& out.131072.verify.dummy.txt
/bin/mv pout.0 pout.131072.verify.0
date
#
# point SCALING
#
export ARGS='-log_summary -vcycle_type 0 -refinement_type point -nlevels 20 -n_inc 20 -n_amr_refine 100 -crs_box_size 8 -fine_box_size 8 -num_repeat 8'
echo "start SCALING"
aprun -n 256 ${ex} ${ARGS} -n_coarse_patches 4 >& out.000256.scaling.txt
/bin/mv pout.0 pout.000256.scaling.dummy
/bin/mv time.table.0 time.table.000256.scaling.0
date
aprun -n 2048 ${ex} ${ARGS} -n_coarse_patches 8 >& out.002048.scaling.txt 
/bin/mv pout.0 pout.002048.scaling.dummy
/bin/mv time.table.0 time.table.002048.scaling.0
date
aprun -n 16384 ${ex} ${ARGS} -n_coarse_patches 16 >& out.016384.scaling.txt
/bin/mv pout.0 pout.016384.scaling.dummy
/bin/mv time.table.0 time.table.016384.scaling.0
date
aprun -n 131072 ${ex} ${ARGS} -n_coarse_patches 32 >& out.131072.scaling.txt
/bin/mv pout.0 pout.131072.scaling.dummy
/bin/mv time.table.0 time.table.131072.scaling.0
date
