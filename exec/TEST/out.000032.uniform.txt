	[0]PCSetFromOptions_GAMG threshold set -5.000000e-03
[0] FAS solver |r|=2.27768, |r|/|b|=0.0143276
	[0]PCSetUp_GAMG level 0 N=1048576, n data rows=1, n data cols=1, nnz/row (ave)=26, np=32
	[0]PCGAMGFilterGraph 100% nnz after filtering, with threshold -0.005, 26.4412 nnz ave. (N=1048576)
[0]PCGAMGCoarsen_AGG square graph
[0]PCGAMGCoarsen_AGG coarsen graph
	[0]maxIndSetAgg removed 0 of 1048576 vertices. (0 local)  22536 selected.
		[0]PCGAMGProlongator_AGG New grid 22536 nodes
			PCGAMGOptprol_AGG smooth P0: max eigen=1.407148e+00 min=5.454112e-03 PC=jacobi
		[0]PCSetUp_GAMG 1) N=22536, n data cols=1, nnz/row (ave)=41, 32 active pes
	[0]PCGAMGFilterGraph 100% nnz after filtering, with threshold -0.005, 41.8644 nnz ave. (N=22536)
[0]PCGAMGCoarsen_AGG square graph
[0]PCGAMGCoarsen_AGG coarsen graph
	[0]maxIndSetAgg removed 0 of 22536 vertices. (0 local)  321 selected.
		[0]PCGAMGProlongator_AGG New grid 321 nodes
			PCGAMGOptprol_AGG smooth P0: max eigen=1.228940e+00 min=1.597642e-02 PC=jacobi
	[0]createLevel number of equations (loc) 8 with simple aggregation
		[0]PCSetUp_GAMG 2) N=321, n data cols=1, nnz/row (ave)=46, 2 active pes
	[0]PCGAMGFilterGraph 100% nnz after filtering, with threshold -0.005, 46.4766 nnz ave. (N=321)
[0]PCGAMGCoarsen_AGG square graph
[0]PCGAMGCoarsen_AGG coarsen graph
	[0]maxIndSetAgg removed 0 of 321 vertices. (0 local)  5 selected.
		[0]PCGAMGProlongator_AGG New grid 5 nodes
			PCGAMGOptprol_AGG smooth P0: max eigen=1.230009e+00 min=2.352541e-01 PC=jacobi
	[0]createLevel number of equations (loc) 1 with simple aggregation
		[0]PCSetUp_GAMG 3) N=5, n data cols=1, nnz/row (ave)=5, 1 active pes
	[0]PCSetUp_GAMG 4 levels, grid complexity = 1.03457
  0 KSP Residual norm 5.732937977177e+02 
  1 KSP Residual norm 5.840667404535e+01 
Linear solve converged due to CONVERGED_RTOL iterations 1
  0 KSP Residual norm 5.732937977177e+02 
  1 KSP Residual norm 5.840667404535e+01 
  2 KSP Residual norm 1.067089547777e+01 
  3 KSP Residual norm 2.312506660991e+00 
  4 KSP Residual norm 4.829769491453e-01 
  5 KSP Residual norm 9.072126975483e-02 
  6 KSP Residual norm 1.687922075179e-02 
  7 KSP Residual norm 3.561523700685e-03 
  8 KSP Residual norm 6.983388394539e-04 
  9 KSP Residual norm 1.613134544335e-04 
Linear solve converged due to CONVERGED_RTOL iterations 9
  0 KSP Residual norm 5.732937977177e+02 
  1 KSP Residual norm 5.840667404535e+01 
  2 KSP Residual norm 1.067089547777e+01 
  3 KSP Residual norm 2.312506660991e+00 
  4 KSP Residual norm 4.829769491453e-01 
  5 KSP Residual norm 9.072126975483e-02 
  6 KSP Residual norm 1.687922075179e-02 
  7 KSP Residual norm 3.561523700685e-03 
  8 KSP Residual norm 6.983388394541e-04 
  9 KSP Residual norm 1.613134544335e-04 
Linear solve converged due to CONVERGED_RTOL iterations 9
  0 KSP Residual norm 5.732937977177e+02 
  1 KSP Residual norm 5.840667404535e+01 
  2 KSP Residual norm 1.067089547777e+01 
  3 KSP Residual norm 2.312506660991e+00 
  4 KSP Residual norm 4.829769491453e-01 
  5 KSP Residual norm 9.072126975483e-02 
  6 KSP Residual norm 1.687922075179e-02 
  7 KSP Residual norm 3.561523700685e-03 
  8 KSP Residual norm 6.983388394536e-04 
  9 KSP Residual norm 1.613134544336e-04 
Linear solve converged due to CONVERGED_RTOL iterations 9
  0 KSP Residual norm 5.732937977177e+02 
  1 KSP Residual norm 5.840667404535e+01 
  2 KSP Residual norm 1.067089547777e+01 
  3 KSP Residual norm 2.312506660991e+00 
  4 KSP Residual norm 4.829769491453e-01 
  5 KSP Residual norm 9.072126975483e-02 
  6 KSP Residual norm 1.687922075179e-02 
  7 KSP Residual norm 3.561523700685e-03 
  8 KSP Residual norm 6.983388394535e-04 
  9 KSP Residual norm 1.613134544337e-04 
Linear solve converged due to CONVERGED_RTOL iterations 9
  0 KSP Residual norm 5.732937977177e+02 
  1 KSP Residual norm 5.840667404535e+01 
  2 KSP Residual norm 1.067089547777e+01 
  3 KSP Residual norm 2.312506660991e+00 
  4 KSP Residual norm 4.829769491453e-01 
  5 KSP Residual norm 9.072126975483e-02 
  6 KSP Residual norm 1.687922075179e-02 
  7 KSP Residual norm 3.561523700685e-03 
  8 KSP Residual norm 6.983388394535e-04 
  9 KSP Residual norm 1.613134544336e-04 
Linear solve converged due to CONVERGED_RTOL iterations 9
  0 KSP Residual norm 5.732937977177e+02 
  1 KSP Residual norm 5.840667404535e+01 
  2 KSP Residual norm 1.067089547777e+01 
  3 KSP Residual norm 2.312506660991e+00 
  4 KSP Residual norm 4.829769491453e-01 
  5 KSP Residual norm 9.072126975483e-02 
  6 KSP Residual norm 1.687922075179e-02 
  7 KSP Residual norm 3.561523700685e-03 
  8 KSP Residual norm 6.983388394539e-04 
  9 KSP Residual norm 1.613134544335e-04 
Linear solve converged due to CONVERGED_RTOL iterations 9
  0 KSP Residual norm 5.732937977177e+02 
  1 KSP Residual norm 5.840667404535e+01 
  2 KSP Residual norm 1.067089547777e+01 
  3 KSP Residual norm 2.312506660991e+00 
  4 KSP Residual norm 4.829769491453e-01 
  5 KSP Residual norm 9.072126975483e-02 
  6 KSP Residual norm 1.687922075179e-02 
  7 KSP Residual norm 3.561523700685e-03 
  8 KSP Residual norm 6.983388394537e-04 
  9 KSP Residual norm 1.613134544336e-04 
Linear solve converged due to CONVERGED_RTOL iterations 9
  0 KSP Residual norm 5.732937977177e+02 
  1 KSP Residual norm 5.840667404535e+01 
  2 KSP Residual norm 1.067089547777e+01 
  3 KSP Residual norm 2.312506660991e+00 
  4 KSP Residual norm 4.829769491453e-01 
  5 KSP Residual norm 9.072126975483e-02 
  6 KSP Residual norm 1.687922075179e-02 
  7 KSP Residual norm 3.561523700685e-03 
  8 KSP Residual norm 6.983388394537e-04 
  9 KSP Residual norm 1.613134544336e-04 
Linear solve converged due to CONVERGED_RTOL iterations 9
************************************************************************************************************************
***             WIDEN YOUR WINDOW TO 120 CHARACTERS.  Use 'enscript -r -fCourier9' to print this document            ***
************************************************************************************************************************

---------------------------------------------- PETSc Performance Summary: ----------------------------------------------

../testAMRBC3d.Linux.64.CC.ftn.OPTHIGH.MPI.PETSC.ex on a arch-xc30-opt64-intel named nid03561 with 32 processors, by madams Thu Mar 12 00:52:42 2015
Using Petsc Release Version 3.5.3, unknown 

                         Max       Max/Min        Avg      Total 
Time (sec):           5.534e+00      1.00730   5.503e+00
Objects:              9.070e+02      1.00221   9.051e+02
Flops:                1.271e+09      1.01817   1.261e+09  4.034e+10
Flops/sec:            2.314e+08      1.02122   2.291e+08  7.330e+09
MPI Messages:         1.712e+04      2.37541   1.186e+04  3.795e+05
MPI Message Lengths:  3.332e+07      1.72458   2.219e+03  8.424e+08
MPI Reductions:       7.800e+02      1.00000

Flop counting convention: 1 flop = 1 real number operation of type (multiply/divide/add/subtract)
                            e.g., VecAXPY() for real vectors of length N --> 2N flops
                            and VecAXPY() for complex vectors of length N --> 8N flops

Summary of Stages:   ----- Time ------  ----- Flops -----  --- Messages ---  -- Message Lengths --  -- Reductions --
                        Avg     %Total     Avg     %Total   counts   %Total     Avg         %Total   counts   %Total 
 0:      Main Stage: 5.6762e-01  10.3%  0.0000e+00   0.0%  7.360e+02   0.2%  2.881e+00        0.1%  2.100e+01   2.7% 
 1:       The solve: 4.9358e+00  89.7%  4.0339e+10 100.0%  3.788e+05  99.8%  2.216e+03       99.9%  7.580e+02  97.2% 

------------------------------------------------------------------------------------------------------------------------
See the 'Profiling' chapter of the users' manual for details on interpreting output.
Phase summary info:
   Count: number of times phase was executed
   Time and Flops: Max - maximum over all processors
                   Ratio - ratio of maximum to minimum over all processors
   Mess: number of messages sent
   Avg. len: average message length (bytes)
   Reduct: number of global reductions
   Global: entire computation
   Stage: stages of a computation. Set stages with PetscLogStagePush() and PetscLogStagePop().
      %T - percent time in this phase         %F - percent flops in this phase
      %M - percent messages in this phase     %L - percent message lengths in this phase
      %R - percent reductions in this phase
   Total Mflop/s: 10e-6 * (sum of flops over all processors)/(max time over all processors)
------------------------------------------------------------------------------------------------------------------------
Event                Count      Time (sec)     Flops                             --- Global ---  --- Stage ---   Total
                   Max Ratio  Max     Ratio   Max  Ratio  Mess   Avg len Reduct  %T %F %M %L %R  %T %F %M %L %R Mflop/s
------------------------------------------------------------------------------------------------------------------------

--- Event Stage 0: Main Stage

Grid setup             1 1.0 3.6249e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   1  0  0  0  0     0
createMatrix           1 1.0 3.4037e-01 1.0 0.00e+00 0.0 7.4e+02 1.5e+03 1.5e+01  6  0  0  0  2  60  0100100 71     0
MatAssemblyBegin       1 1.0 6.7880e-0340.4 0.00e+00 0.0 0.0e+00 0.0e+00 2.0e+00  0  0  0  0  0   1  0  0  0 10     0
MatAssemblyEnd         1 1.0 1.7502e-02 1.0 0.00e+00 0.0 7.4e+02 1.5e+03 8.0e+00  0  0  0  0  1   3  0100100 38     0
VecSet                 1 1.0 1.0014e-05 1.4 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0

--- Event Stage 1: The solve

FAS-FMG-solve          8 1.0 1.5140e-01 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  3  0  0  0  0   3  0  0  0  0     0
Chombo-solve           8 1.0 2.0502e-01 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  4  0  0  0  0   4  0  0  0  0     0
PETSc-AMG-solve        8 1.0 2.8398e+00 1.0 1.15e+09 1.0 3.3e+05 1.5e+03 1.5e+02 52 91 86 58 19  58 91 86 58 20 12885
FAS-Vcycle-solve       8 1.0 3.9447e-01 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  7  0  0  0  0   8  0  0  0  0     0
MatMult              349 1.0 7.4052e-01 1.4 2.89e+08 1.0 9.5e+04 2.1e+03 0.0e+00 11 23 25 23  0  12 23 25 23  0 12357
MatMultAdd           246 1.0 7.5298e-02 1.3 2.30e+07 1.0 3.2e+04 2.1e+02 0.0e+00  1  2  9  1  0   1  2  9  1  0  9609
MatMultTranspose     246 1.0 5.7141e-02 1.3 2.30e+07 1.0 3.2e+04 2.1e+02 0.0e+00  1  2  9  1  0   1  2  9  1  0 12662
MatSolve              82 0.0 1.8597e-04 0.0 3.69e+03 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0    20
MatSOR               492 1.0 2.2123e+00 1.2 7.98e+08 1.0 1.8e+05 1.7e+03 0.0e+00 38 63 48 36  0  43 63 48 36  0 11465
MatLUFactorSym         1 1.0 2.0981e-05 2.6 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatLUFactorNum         1 1.0 1.5020e-0515.8 7.30e+01 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     5
MatConvert             3 1.0 1.3057e-02 1.6 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatScale               9 1.0 1.1622e-02 1.2 2.09e+06 1.0 7.4e+02 1.7e+03 0.0e+00  0  0  0  0  0   0  0  0  0  0  5695
MatResidual          246 1.0 4.6278e-01 1.8 1.49e+08 1.0 6.1e+04 1.7e+03 0.0e+00  6 12 16 12  0   6 12 16 12  0 10165
MatAssemblyBegin      56 1.0 5.1319e-02 4.0 0.00e+00 0.0 5.6e+03 1.2e+04 6.2e+01  1  0  1  8  8   1  0  1  8  8     0
MatAssemblyEnd        56 1.0 9.5469e-02 1.1 0.00e+00 0.0 8.2e+03 6.7e+02 1.8e+02  2  0  2  1 23   2  0  2  1 23     0
MatGetRow         168060 1.0 1.7257e-02 1.1 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatGetRowIJ            1 0.0 5.0068e-06 0.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatGetOrdering         1 0.0 3.6001e-05 0.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatCoarsen             3 1.0 1.7099e-02 2.2 0.00e+00 0.0 5.2e+03 3.2e+03 1.5e+01  0  0  1  2  2   0  0  1  2  2     0
MatAXPY                3 1.0 9.7458e-03 1.1 0.00e+00 0.0 0.0e+00 0.0e+00 6.0e+00  0  0  0  0  1   0  0  0  0  1     0
MatTranspose           3 1.0 5.7372e-02 1.0 0.00e+00 0.0 5.5e+03 6.5e+03 3.6e+01  1  0  1  4  5   1  0  1  4  5     0
MatMatMult             3 1.0 3.4243e-02 1.0 1.81e+06 1.0 4.3e+03 1.2e+03 4.8e+01  1  0  1  1  6   1  0  1  1  6  1675
MatMatMultSym          3 1.0 2.1413e-02 1.0 0.00e+00 0.0 3.5e+03 1.1e+03 4.2e+01  0  0  1  0  5   0  0  1  0  6     0
MatMatMultNum          3 1.0 1.3036e-02 1.0 1.81e+06 1.0 7.4e+02 1.7e+03 6.0e+00  0  0  0  0  1   0  0  0  0  1  4401
MatPtAP                3 1.0 1.2798e-01 1.2 1.06e+07 1.1 6.4e+03 2.8e+03 5.1e+01  2  1  2  2  7   2  1  2  2  7  2578
MatPtAPSymbolic        3 1.0 6.5009e-02 1.5 0.00e+00 0.0 4.2e+03 3.7e+03 2.1e+01  1  0  1  2  3   1  0  1  2  3     0
MatPtAPNumeric         3 1.0 6.2980e-02 1.0 1.06e+07 1.1 2.2e+03 1.1e+03 3.0e+01  1  1  1  0  4   1  1  1  0  4  5238
MatTrnMatMult          3 1.0 7.2226e-01 1.0 5.08e+07 1.1 4.4e+03 4.5e+04 5.7e+01 13  4  1 24  7  15  4  1 24  8  2196
MatTrnMatMultSym       3 1.0 3.8183e-01 1.0 0.00e+00 0.0 3.7e+03 2.8e+04 5.1e+01  7  0  1 12  7   8  0  1 12  7     0
MatTrnMatMultNum       3 1.0 3.4054e-01 1.0 5.08e+07 1.1 7.4e+02 1.3e+05 6.0e+00  6  4  0 11  1   7  4  0 11  1  4657
MatGetLocalMat        15 1.0 2.4156e-02 1.5 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatGetBrAoCol          9 1.0 2.6351e-02 6.3 0.00e+00 0.0 5.2e+03 3.5e+03 0.0e+00  0  0  1  2  0   0  0  1  2  0     0
MatGetSymTrans         6 1.0 1.4298e-03 1.3 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecMDot              103 1.0 2.0884e-01 7.4 2.74e+07 1.0 0.0e+00 0.0e+00 1.0e+02  1  2  0  0 13   2  2  0  0 14  4189
VecNorm              115 1.0 2.9027e-02 8.0 6.11e+06 1.0 0.0e+00 0.0e+00 1.2e+02  0  0  0  0 15   0  0  0  0 15  6736
VecScale             853 1.0 2.1291e-03 1.3 4.57e+06 1.2 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0 63919
VecCopy               12 1.0 1.3635e-03 1.7 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecSet              1053 1.0 7.0806e-03 1.7 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecAXPY               12 1.0 7.2289e-04 1.7 6.57e+05 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0 29074
VecAYPX              246 1.0 6.0384e-03 1.8 2.76e+06 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0 14550
VecMAXPY             115 1.0 1.3915e-02 1.4 3.28e+07 1.0 0.0e+00 0.0e+00 0.0e+00  0  3  0  0  0   0  3  0  0  0 75418
VecAssemblyBegin      24 1.0 2.9120e-03 1.9 0.00e+00 0.0 0.0e+00 0.0e+00 6.6e+01  0  0  0  0  8   0  0  0  0  9     0
VecAssemblyEnd        24 1.0 1.0085e-04 6.6 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecPointwiseMult      33 1.0 1.3874e-03 1.6 3.70e+05 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0  8495
VecScatterBegin     1605 1.0 4.3389e-02 2.2 0.00e+00 0.0 3.5e+05 1.5e+03 0.0e+00  1  0 91 63  0   1  0 92 63  0     0
VecScatterEnd       1605 1.0 8.7047e-0124.3 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  5  0  0  0  0   5  0  0  0  0     0
VecSetRandom           3 1.0 2.5582e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecNormalize         115 1.0 3.0225e-02 6.3 9.17e+06 1.0 0.0e+00 0.0e+00 1.2e+02  0  1  0  0 15   0  1  0  0 15  9704
KSPGMRESOrthog       103 1.0 2.1794e-01 5.7 5.47e+07 1.0 0.0e+00 0.0e+00 1.0e+02  2  4  0  0 13   2  4  0  0 14  8029
KSPSetUp               9 1.0 1.2443e-03 1.5 0.00e+00 0.0 0.0e+00 0.0e+00 6.0e+00  0  0  0  0  1   0  0  0  0  1     0
KSPSolve               9 1.0 4.1768e+00 1.0 1.27e+09 1.0 3.8e+05 2.2e+03 7.5e+02 76100100100 96  85100100100 99  9658
PCGAMGgraph_AGG        3 1.0 2.5805e-01 1.0 1.81e+06 1.0 1.0e+04 7.1e+03 8.1e+01  5  0  3  8 10   5  0  3  8 11   222
PCGAMGcoarse_AGG       3 1.0 7.4039e-01 1.0 5.08e+07 1.1 1.3e+04 1.7e+04 1.2e+02 13  4  3 27 15  15  4  4 27 16  2142
PCGAMGProl_AGG         3 1.0 1.2200e-02 1.0 0.00e+00 0.0 3.4e+03 1.9e+03 7.2e+01  0  0  1  1  9   0  0  1  1  9     0
PCGAMGPOpt_AGG         3 1.0 1.0571e-01 1.0 2.95e+07 1.0 1.2e+04 1.5e+03 1.5e+02  2  2  3  2 19   2  2  3  2 20  8851
PCSetUp                2 1.0 1.2653e+00 1.0 9.27e+07 1.0 4.5e+04 7.5e+03 5.9e+02 23  7 12 40 76  26  7 12 40 78  2299
PCSetUpOnBlocks       82 1.0 1.2827e-04 2.0 7.30e+01 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     1
PCApply               82 1.0 2.6227e+00 1.1 9.92e+08 1.0 3.1e+05 1.4e+03 0.0e+00 47 78 81 50  0  52 78 81 50  0 12016
SFSetGraph             6 1.0 5.9819e-04 2.1 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
SFBcastBegin          15 1.0 1.0503e-02 9.6 0.00e+00 0.0 5.2e+03 3.2e+03 0.0e+00  0  0  1  2  0   0  0  1  2  0     0
SFBcastEnd            15 1.0 1.2174e-03 2.7 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
SFReduceBegin          3 1.0 2.9581e-03 3.2 0.00e+00 0.0 1.8e+03 1.4e+03 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
SFReduceEnd            3 1.0 4.9591e-04 7.6 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
------------------------------------------------------------------------------------------------------------------------

Memory usage is given in bytes:

Object Type          Creations   Destructions     Memory  Descendants' Mem.
Reports information only for process 0.

--- Event Stage 0: Main Stage

              Matrix     4              3     30353480     0
              Vector     4              2        28696     0
      Vector Scatter     1              1         1196     0
           Index Set     2              2        27048     0
       Krylov Solver     1              0            0     0
      Preconditioner     1              0            0     0
              Viewer     1              0            0     0

--- Event Stage 1: The solve

              Matrix   101            102    168979304     0
      Matrix Coarsen     3              3         2076     0
              Vector   663            665     57820128     0
      Vector Scatter    27             27        32436     0
           Index Set    72             72       192328     0
       Krylov Solver     9             10       145784     0
      Preconditioner     9             10        10640     0
Star Forest Bipartite Graph     6              6         5568     0
         PetscRandom     3              3         2040     0
========================================================================================================================
Average time to get PetscTime(): 0
Average time for MPI_Barrier(): 7.43866e-06
Average time for zero size MPI_Send(): 1.12504e-06
#PETSc Option Table entries:
-amg true
-bc_order 1
-box_stencil false
-chmg true
-chombo_bottom_amg false
-crs_box_size 8
-domain_length 1,2,2
-fine_box_size 32
-fmg true
-ksp_converged_reason
-ksp_gmres_restart 50
-ksp_max_it 50
-ksp_monitor
-ksp_norm_type preconditioned
-ksp_type gmres
-log_summary
-mat_partitioning_type parmetis
-mg_coarse_ksp_type preonly
-mg_levels_ksp_type richardson
-mg_levels_pc_type sor
-n_amr_refine 1
-n_coarse_patches 2
-n_inc 1
-nlevels 0
-npost 2
-npre 2
-num_repeat 8
-pc_gamg_agg_nsmooths 1
-pc_gamg_coarse_eq_limit 10
-pc_gamg_process_eq_limit 200
-pc_gamg_repartition false
-pc_gamg_square_graph true
-pc_gamg_sym_graph true
-pc_gamg_threshold -.005
-pc_gamg_verbose 2
-pc_type gamg
-refinement_type uniform
-smoother_type gsrb
-vcycle_type 1
-verbosity 2
#End of PETSc Option Table entries
Compiled without FORTRAN kernels
Compiled with full precision matrices (default)
sizeof(short) 2 sizeof(int) 4 sizeof(long) 8 sizeof(void*) 8 sizeof(PetscScalar) 8 sizeof(PetscInt) 8
Configure options: --COPTFLAGS="-fast -no-ipo" --CXXOPTFLAGS="-fast -no-ipoi" --FOPTFLAGS="-fast -no-ipo" --download-hypre --download-parmetis --download-metis --with-cc=cc --with-clib-autodetect=0 --with-cxx=CC --with-cxxlib-autodetect=0 --with-debugging=0 --with-fc=ftn --with-fortranlib-autodetect=0 --with-hdf5-dir=/opt/cray/hdf5-parallel/1.8.13/intel/140/ --with-shared-libraries=0 --with-x=0 --with-mpiexec=aprun LIBS=-lstdc++ --with-64-bit-indices PETSC_ARCH=arch-xc30-opt64-intel
-----------------------------------------
Libraries compiled on Sat Feb 14 05:21:21 2015 on edison02 
Machine characteristics: Linux-3.0.101-0.46-default-x86_64-with-SuSE-11-x86_64
Using PETSc directory: /global/homes/m/madams/petsc_maint
Using PETSc arch: arch-xc30-opt64-intel
-----------------------------------------

Using C compiler: cc  -fast -no-ipo  ${COPTFLAGS} ${CFLAGS}
Using Fortran compiler: ftn  -fast -no-ipo   ${FOPTFLAGS} ${FFLAGS} 
-----------------------------------------

Using include paths: -I/global/homes/m/madams/petsc_maint/arch-xc30-opt64-intel/include -I/global/homes/m/madams/petsc_maint/include -I/global/homes/m/madams/petsc_maint/include -I/global/homes/m/madams/petsc_maint/arch-xc30-opt64-intel/include -I/global/u2/m/madams/petsc_maint/arch-xc30-opt64-intel/include -I/opt/cray/hdf5-parallel/1.8.13/intel/140/include
-----------------------------------------

Using C linker: cc
Using Fortran linker: ftn
Using libraries: -Wl,-rpath,/global/homes/m/madams/petsc_maint/arch-xc30-opt64-intel/lib -L/global/homes/m/madams/petsc_maint/arch-xc30-opt64-intel/lib -lpetsc -Wl,-rpath,/global/u2/m/madams/petsc_maint/arch-xc30-opt64-intel/lib -L/global/u2/m/madams/petsc_maint/arch-xc30-opt64-intel/lib -lHYPRE -lparmetis -lmetis -lpthread -lssl -lcrypto -Wl,-rpath,/opt/cray/hdf5-parallel/1.8.13/intel/140/lib -L/opt/cray/hdf5-parallel/1.8.13/intel/140/lib -lhdf5hl_fortran -lhdf5_fortran -lhdf5_hl -lhdf5 -ldl -lstdc++ 
-----------------------------------------

Application 10700434 resources: utime ~178s, stime ~11s, Rss ~297956, inblocks ~101211, outblocks ~296000
