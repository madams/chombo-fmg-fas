3D: show_options=T, plot_hdf5=F, out_error=T, redundant_crs=F, verbose= 2
       bc_order=   1
       nvcycles=   0
       nfcycles=   1
        ncycles=   1
    nfmgvcycles=   1
     bot_min_sz=   2
      pe_min_sz=   4
     num_solves=   8
  sr_max_loc_sz=   0
  sr_base_bufsz=   2
   sr_bufsz_inc=   2
    nsmoothsfmg= 0, nsmoothsdown= 2, nsmoothsup= 2
              F( 0, 2, 2)
     error_norm= infinity
7-point stencil
red/black Gausse-Siedel smoother
new level: 0) allocatd lo: 0 0 0 hi:   3   3   3 N:   2   2   2, npx=  2  2  1, dx: 0.50E+00 0.50E+00 0.50E+00
new level: 1) allocatd lo: 0 0 0 hi:   5   5   5 N:   4   4   4, npx=  2  2  1, dx: 0.25E+00 0.25E+00 0.25E+00
new level: 2) allocatd lo: 0 0 0 hi:   9   9   9 N:   8   8   8, npx=  2  2  1, dx: 0.12E+00 0.12E+00 0.12E+00
new level: 3) allocatd lo: 0 0 0 hi:  17  17  17 N:  16  16  16, npx=  2  2  1, dx: 0.62E-01 0.62E-01 0.62E-01
new level: 4) allocatd lo: 0 0 0 hi:  33  33  33 N:  32  32  32, npx=  2  2  1, dx: 0.31E-01 0.31E-01 0.31E-01
       FMG     level: 1) solve: |f-Au|_3 =  1.630E+00, |e|_3 =  7.4880455156E-01
       FMG     level: 2) solve: |f-Au|_3 =  2.610E+00, |e|_3 =  2.3741605673E-01
       FMG     level: 3) solve: |f-Au|_3 =  1.152E+00, |e|_3 =  6.7924400928E-02
       FMG     level: 4) solve: |f-Au|_3 =  3.570E-01, |e|_3 =  1.8150497311E-02
coarse grid 0: errors rates i-1 --> i
      grid i: 1 convergence order of u = 1.111E+00, |f-Au|= 1.630E+00   
      grid i: 2 convergence order of u = 1.657E+00, |f-Au|= 2.610E+00   
      grid i: 3 convergence order of u = 1.805E+00, |f-Au|= 1.152E+00   
      grid i: 4 convergence order of u = 1.904E+00, |f-Au|= 3.570E-01   
driver: solve     1 |f-Au|/|f|= 2.261E-03
************************************************************************************************************************
***             WIDEN YOUR WINDOW TO 120 CHARACTERS.  Use 'enscript -r -fCourier9' to print this document            ***
************************************************************************************************************************

---------------------------------------------- PETSc Performance Summary: ----------------------------------------------

../srgmg on a arch-xc30-opt64-intel named nid03560 with 4 processors, by madams Thu Mar 12 00:43:50 2015
Using Petsc Release Version 3.5.3, unknown 

                         Max       Max/Min        Avg      Total 
Time (sec):           2.342e-01      1.00044   2.341e-01
Objects:              1.000e+00      1.00000   1.000e+00
Flops:                5.007e+07      1.00000   5.007e+07  2.003e+08
Flops/sec:            2.139e+08      1.00044   2.139e+08  8.554e+08
MPI Messages:         0.000e+00      0.00000   0.000e+00  0.000e+00
MPI Message Lengths:  0.000e+00      0.00000   0.000e+00  0.000e+00
MPI Reductions:       1.000e+00      1.00000

Flop counting convention: 1 flop = 1 real number operation of type (multiply/divide/add/subtract)
                            e.g., VecAXPY() for real vectors of length N --> 2N flops
                            and VecAXPY() for complex vectors of length N --> 8N flops

Summary of Stages:   ----- Time ------  ----- Flops -----  --- Messages ---  -- Message Lengths --  -- Reductions --
                        Avg     %Total     Avg     %Total   counts   %Total     Avg         %Total   counts   %Total 
 0:      Main Stage: 2.3010e-01  98.3%  1.7802e+08  88.9%  0.000e+00   0.0%  0.000e+00        0.0%  0.000e+00   0.0% 
 1:             pre: 4.0130e-03   1.7%  2.2253e+07  11.1%  0.000e+00   0.0%  0.000e+00        0.0%  0.000e+00   0.0% 

------------------------------------------------------------------------------------------------------------------------
See the 'Profiling' chapter of the users' manual for details on interpreting output.
Phase summary info:
   Count: number of times phase was executed
   Time and Flops: Max - maximum over all processors
                   Ratio - ratio of maximum to minimum over all processors
   Mess: number of messages sent
   Avg. len: average message length (bytes)
   Reduct: number of global reductions
   Global: entire computation
   Stage: stages of a computation. Set stages with PetscLogStagePush() and PetscLogStagePop().
      %T - percent time in this phase         %F - percent flops in this phase
      %M - percent messages in this phase     %L - percent message lengths in this phase
      %R - percent reductions in this phase
   Total Mflop/s: 10e-6 * (sum of flops over all processors)/(max time over all processors)
------------------------------------------------------------------------------------------------------------------------
Event                Count      Time (sec)     Flops                             --- Global ---  --- Stage ---   Total
                   Max Ratio  Max     Ratio   Max  Ratio  Mess   Avg len Reduct  %T %F %M %L %R  %T %F %M %L %R Mflop/s
------------------------------------------------------------------------------------------------------------------------

--- Event Stage 0: Main Stage

SR  smooth          1200 1.0 4.1258e-03 1.0 2.06e+07 1.0 0.0e+00 0.0e+00 0.0e+00  2 41  0  0  0   2 46  0  0  0 19928
SR  apply            200 1.0 1.2748e-03 1.1 1.23e+07 1.0 0.0e+00 0.0e+00 0.0e+00  1 25  0  0  0   1 28  0  0  0 38651
SR  BC              1512 1.0 1.3413e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   1  0  0  0  0     0
SR  u,f               40 1.0 2.1255e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   1  0  0  0  0     0
SR  prolong          112 1.0 3.3257e-03 1.0 1.03e+07 1.0 0.0e+00 0.0e+00 0.0e+00  1 21  0  0  0   1 23  0  0  0 12346
SR  restrict          80 1.0 4.8780e-04 1.1 1.37e+06 1.0 0.0e+00 0.0e+00 0.0e+00  0  3  0  0  0   0  3  0  0  0 11218
SR  AXPY             480 1.0 1.9150e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  1  0  0  0  0   1  0  0  0  0     0
SR  exchange        1512 1.0 4.7307e-03 1.2 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  2  0  0  0  0   2  0  0  0  0     0
SR  *crs solv         40 1.0 1.0965e-03 1.0 3.36e+04 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0   123
SR solve               8 1.0 2.4469e-02 1.0 4.45e+07 1.0 0.0e+00 0.0e+00 0.0e+00 10 89  0  0  0  11100  0  0  0  7275

--- Event Stage 1: pre

SR  smooth           150 1.0 5.5647e-04 1.0 2.57e+06 1.0 0.0e+00 0.0e+00 0.0e+00  0  5  0  0  0  14 46  0  0  0 18469
SR  apply             25 1.0 1.5593e-04 1.1 1.54e+06 1.0 0.0e+00 0.0e+00 0.0e+00  0  3  0  0  0   4 28  0  0  0 39501
SR  BC               189 1.0 1.8716e-04 1.1 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   4  0  0  0  0     0
SR  u,f               10 1.0 4.0388e-04 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0  10  0  0  0  0     0
SR  prolong           14 1.0 4.3416e-04 1.0 1.28e+06 1.0 0.0e+00 0.0e+00 0.0e+00  0  3  0  0  0  11 23  0  0  0 11821
SR  restrict          10 1.0 7.3195e-05 1.1 1.71e+05 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   2  3  0  0  0  9345
SR  AXPY              65 1.0 3.5977e-04 1.1 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   8  0  0  0  0     0
SR  exchange         189 1.0 9.8896e-04 1.3 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0  23  0  0  0  0     0
SR  *crs solv          5 1.0 1.9526e-04 1.0 4.20e+03 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   5  0  0  0  0    86
------------------------------------------------------------------------------------------------------------------------

Memory usage is given in bytes:

Object Type          Creations   Destructions     Memory  Descendants' Mem.
Reports information only for process 0.

--- Event Stage 0: Main Stage

              Viewer     1              0            0     0

--- Event Stage 1: pre

========================================================================================================================
Average time to get PetscTime(): 0
Average time for MPI_Barrier(): 2.00272e-06
Average time for zero size MPI_Send(): 1.2517e-06
#PETSc Option Table entries:
-amg true
-bc_order 1
-box_stencil false
-chmg true
-chombo_bottom_amg false
-crs_box_size 4
-dom_x_hi 2.0
-dom_y_hi 2.0
-domain_length 1,2,2
-fine_box_size 4
-fmg true
-ksp_converged_reason
-ksp_gmres_restart 50
-ksp_max_it 50
-ksp_monitor
-ksp_norm_type preconditioned
-ksp_type gmres
-log_summary
-mat_partitioning_type parmetis
-mg_coarse_ksp_type preonly
-mg_levels_ksp_type richardson
-mg_levels_pc_type sor
-n_amr_refine 1
-n_coarse_patches 4
-n_inc 1
-nlevels 3
-npost 2
-npre 2
-nsmoothsdown 2
-nsmoothsfmg 0
-nsmoothsup 2
-num_repeat 1
-num_solves 8
-nxloc 32
-nxpe 2
-nype 2
-nzpe 1
-out_error true
-pc_gamg_agg_nsmooths 1
-pc_gamg_coarse_eq_limit 10
-pc_gamg_process_eq_limit 200
-pc_gamg_repartition false
-pc_gamg_square_graph true
-pc_gamg_sym_graph true
-pc_gamg_threshold -.005
-pc_gamg_verbose 2
-pc_type gamg
-problem_type 0
-refinement_type point
-show_options true
-smoother_type 0
-vcycle_type 1
-verbose 2
-verbosity 2
#End of PETSc Option Table entries
Compiled without FORTRAN kernels
Compiled with full precision matrices (default)
sizeof(short) 2 sizeof(int) 4 sizeof(long) 8 sizeof(void*) 8 sizeof(PetscScalar) 8 sizeof(PetscInt) 8
Configure options: --COPTFLAGS="-fast -no-ipo" --CXXOPTFLAGS="-fast -no-ipoi" --FOPTFLAGS="-fast -no-ipo" --download-hypre --download-parmetis --download-metis --with-cc=cc --with-clib-autodetect=0 --with-cxx=CC --with-cxxlib-autodetect=0 --with-debugging=0 --with-fc=ftn --with-fortranlib-autodetect=0 --with-hdf5-dir=/opt/cray/hdf5-parallel/1.8.13/intel/140/ --with-shared-libraries=0 --with-x=0 --with-mpiexec=aprun LIBS=-lstdc++ --with-64-bit-indices PETSC_ARCH=arch-xc30-opt64-intel
-----------------------------------------
Libraries compiled on Sat Feb 14 05:21:21 2015 on edison02 
Machine characteristics: Linux-3.0.101-0.46-default-x86_64-with-SuSE-11-x86_64
Using PETSc directory: /global/homes/m/madams/petsc_maint
Using PETSc arch: arch-xc30-opt64-intel
-----------------------------------------

Using C compiler: cc  -fast -no-ipo  ${COPTFLAGS} ${CFLAGS}
Using Fortran compiler: ftn  -fast -no-ipo   ${FOPTFLAGS} ${FFLAGS} 
-----------------------------------------

Using include paths: -I/global/homes/m/madams/petsc_maint/arch-xc30-opt64-intel/include -I/global/homes/m/madams/petsc_maint/include -I/global/homes/m/madams/petsc_maint/include -I/global/homes/m/madams/petsc_maint/arch-xc30-opt64-intel/include -I/global/u2/m/madams/petsc_maint/arch-xc30-opt64-intel/include -I/opt/cray/hdf5-parallel/1.8.13/intel/140/include
-----------------------------------------

Using C linker: cc
Using Fortran linker: ftn
Using libraries: -Wl,-rpath,/global/homes/m/madams/petsc_maint/arch-xc30-opt64-intel/lib -L/global/homes/m/madams/petsc_maint/arch-xc30-opt64-intel/lib -lpetsc -Wl,-rpath,/global/u2/m/madams/petsc_maint/arch-xc30-opt64-intel/lib -L/global/u2/m/madams/petsc_maint/arch-xc30-opt64-intel/lib -lHYPRE -lparmetis -lmetis -lpthread -lssl -lcrypto -Wl,-rpath,/opt/cray/hdf5-parallel/1.8.13/intel/140/lib -L/opt/cray/hdf5-parallel/1.8.13/intel/140/lib -lhdf5hl_fortran -lhdf5_fortran -lhdf5_hl -lhdf5 -ldl -lstdc++ 
-----------------------------------------

Application 10700411 resources: utime ~1s, stime ~0s, Rss ~9664, inblocks ~14891, outblocks ~38399
