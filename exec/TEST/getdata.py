#!/usr/bin/env python

import sys, os, math

#print "argument it my script:", sys.argv[0]

#
# weak SCALING full AMR, solve times (FAS-F, Chombo, AMG, AMG its) vs procs
#
nprocs_scale = []
AMG_time_scale = [0 for i in range(20)]
AMG_its = [0 for i in range(20)]
FAS_F_time_scale = [0 for i in range(20)]
CH_V_time_scale = [0 for i in range(20)]
idx = 0
fout = open('scaling.dat', 'w')
for ifile in sorted(os.listdir('./')):
    if ifile.endswith('.scaling.txt'):
        f = open(ifile,'r')
        num_lines = sum(1 for line in f)
        f.seek(0,0)
        if num_lines < 2:
            print '********** skip file ',ifile; f.close(); continue
        for text in f:  # lines
            words = text.split() # words in line
            n = len(words)
            if n > 1:
                if words[0].startswith('../testAMRBC'):
                    s = words[7]; 
                    nprocs_scale.append(int(s))
                elif words[0] == 'FAS-FMG-solve':
                    s = words[3]; v = float(s)
                    FAS_F_time_scale[idx] = v  # clobbers first one
                elif words[0] == 'Chombo-solve':
                    s = words[3]; v = float(s)
                    CH_V_time_scale[idx] = v  # clobbers first one
                elif words[0] == 'PETSc-AMG-solve':
                    s = words[3]; v = float(s)
                    AMG_time_scale[idx] = v  # clobbers first one
                elif words[0] == 'Linear' and words[2] == 'converged':
                    s = words[7]
                    AMG_its[idx] = (int(s)) # clobbers several
        #print idx,len(nprocs_scale)
        if len(nprocs_scale)==idx+1:
            fout.write(str(nprocs_scale[idx])+' '+str(FAS_F_time_scale[idx])+' '+str(CH_V_time_scale[idx])+' '+str(AMG_time_scale[idx])+' '+str(AMG_its[idx])+os.linesep)
        idx += 1
        f.close()
fout.close()
print idx, ' entries processed in weak SCALING study'
#
# weak scaling UNIFORM, solve times (FAS-F, Chombo, AMG, FAS-V) vs procs
#
nprocs_uni = []
AMG_time_uni = [0 for i in range(20)]
FAS_V_time_uni = [0 for i in range(20)]
FAS_F_time_uni = [0 for i in range(20)]
CH_V_time_uni = [0 for i in range(20)]
idx = 0
fout = open('uniform.dat', 'w')
for ifile in sorted(os.listdir('./')):
    if ifile.endswith('.uniform.txt'):
        f = open(ifile,'r')
        num_lines = sum(1 for line in f)
        f.seek(0,0)
        if num_lines < 2:
            print '********** skip file ',ifile; f.close(); continue
        for text in f:  # lines
            words = text.split() # words in line
            n = len(words)
            if n > 1:
                if words[0].startswith('../testAMRBC'):
                    s = words[7]
                    nprocs_uni.append(int(s))
                elif words[0] == 'FAS-FMG-solve':
                    s = words[3]
                    FAS_F_time_uni[idx] = float(s)  # clobbers first one
                elif words[0] == 'Chombo-solve':
                    s = words[3]
                    CH_V_time_uni[idx] = float(s)  # clobbers first one
                elif words[0] == 'PETSc-AMG-solve':
                    s = words[3]
                    AMG_time_uni[idx] = float(s)  # clobbers first one
                elif words[0] == 'FAS-Vcycle-solve':
                    s = words[3]
                    FAS_V_time_uni[idx] = float(s) # clobbers
        if len(nprocs_uni)==idx+1:
            fout.write(str(nprocs_uni[idx])+' '+str(FAS_F_time_uni[idx])+' '+str(CH_V_time_uni[idx])+' '+str(FAS_V_time_uni[idx])+' '+str(AMG_time_uni[idx])+os.linesep)
        idx += 1
        f.close()
fout.close()
print idx, ' entries processed in UNIFORM study'
#
# weak scaling V-CYCLES (5), solve times (Chombo, AMG, FAS-V) vs procs
#
nprocs_v = []
AMG_time_v = [0 for i in range(20)]
FAS_V_time_v = [0 for i in range(20)]
CH_V_time_v = [0 for i in range(20)]
idx = 0
fout = open('vcycle.dat', 'w')
for ifile in sorted(os.listdir('./')):
    if ifile.endswith('.vcycle.txt'):
        f = open(ifile,'r')
        num_lines = sum(1 for line in f)
        f.seek(0,0)
        if num_lines < 2:
            print '********** skip file ',ifile; f.close(); continue
        for text in f:  # lines
            words = text.split() # words in line
            n = len(words)
            if n > 1:
                if words[0].startswith('../testAMRBC'):
                    s = words[7]
                    nprocs_v.append(int(s))
                elif words[0] == 'Chombo-solve':
                    s = words[3]
                    CH_V_time_v[idx] = float(s)  # clobbers first one
                elif words[0] == 'PETSc-AMG-solve':
                    s = words[3]
                    AMG_time_v[idx] = float(s)  # clobbers first one
                elif words[0] == 'FAS-Vcycle-solve':
                    s = words[3]
                    FAS_V_time_v[idx] = float(s) # clobbers
        if len(nprocs_v)==idx+1:
#        print idx,len(nprocs_v),len(CH_V_time_v),len(AMG_time_v),len(FAS_V_time_v)
            fout.write(str(nprocs_v[idx])+' '+str(CH_V_time_v[idx])+' '+str(AMG_time_v[idx])+' '+str(FAS_V_time_v[idx])+os.linesep)
        idx += 1
        f.close()
fout.close()
print idx, ' entries processed in V-CYCLES study'
#
# weak scaling SRGMG, solve times HPGMG vs procs
#
nprocs_sr = []
solve_time_sr = [0 for i in range(20)]
idx = 0
fout = open('srgmg.dat', 'w')
for ifile in sorted(os.listdir('./')):
    if ifile.endswith('.srgmg.txt'):
        f = open(ifile,'r')
        num_lines = sum(1 for line in f)
        f.seek(0,0)
        if num_lines < 2:
            print '********** skip file ',ifile; f.close(); continue
        for text in f:  # lines
            words = text.split() # words in line
            n = len(words)
            if n > 1:
                if words[0].startswith('../srgmg'):
                    s = words[7]
                    nprocs_sr.append(int(s))
                elif words[0] == 'SR' and words[1] == 'solve':
                    s = words[4]
                    solve_time_sr[idx] = float(s)  # clobbers first one
        if len(nprocs_sr)==idx+1:
            fout.write(str(nprocs_sr[idx])+' '+str(solve_time_sr[idx])+os.linesep)
        idx += 1
        f.close()
fout.close()
print idx, ' entries processed in SRGMG study'
