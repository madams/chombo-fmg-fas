	[0]PCSetFromOptions_GAMG threshold set -5.000000e-03
[0] FAS solver |r|=2.3583, |r|/|b|=0.0147863
	[0]PCSetUp_GAMG level 0 N=8388608, n data rows=1, n data cols=1, nnz/row (ave)=26, np=256
	[0]PCGAMGFilterGraph 100% nnz after filtering, with threshold -0.005, 26.7197 nnz ave. (N=8388608)
[0]PCGAMGCoarsen_AGG square graph
[0]PCGAMGCoarsen_AGG coarsen graph
	[0]maxIndSetAgg removed 0 of 8388608 vertices. (0 local)  176274 selected.
		[0]PCGAMGProlongator_AGG New grid 176274 nodes
			PCGAMGOptprol_AGG smooth P0: max eigen=1.407961e+00 min=3.773237e-03 PC=jacobi
		[0]PCSetUp_GAMG 1) N=176274, n data cols=1, nnz/row (ave)=44, 256 active pes
	[0]PCGAMGFilterGraph 100% nnz after filtering, with threshold -0.005, 44.146 nnz ave. (N=176274)
[0]PCGAMGCoarsen_AGG square graph
[0]PCGAMGCoarsen_AGG coarsen graph
	[0]maxIndSetAgg removed 0 of 176274 vertices. (0 local)  2209 selected.
		[0]PCGAMGProlongator_AGG New grid 2209 nodes
			PCGAMGOptprol_AGG smooth P0: max eigen=1.230466e+00 min=7.977449e-03 PC=jacobi
	[0]createLevel number of equations (loc) 8 with simple aggregation
		[0]PCSetUp_GAMG 2) N=2209, n data cols=1, nnz/row (ave)=60, 8 active pes
	[0]PCGAMGFilterGraph 100% nnz after filtering, with threshold -0.005, 60.9602 nnz ave. (N=2209)
[0]PCGAMGCoarsen_AGG square graph
[0]PCGAMGCoarsen_AGG coarsen graph
	[0]maxIndSetAgg removed 0 of 2209 vertices. (0 local)  25 selected.
		[0]PCGAMGProlongator_AGG New grid 25 nodes
			PCGAMGOptprol_AGG smooth P0: max eigen=1.239085e+00 min=6.691468e-02 PC=jacobi
	[0]createLevel number of equations (loc) 3 with simple aggregation
		[0]PCSetUp_GAMG 3) N=25, n data cols=1, nnz/row (ave)=19, 1 active pes
	[0]PCGAMGFilterGraph 100% nnz after filtering, with threshold -0.005, 19.64 nnz ave. (N=25)
[0]PCGAMGCoarsen_AGG square graph
[0]PCGAMGCoarsen_AGG coarsen graph
	[0]maxIndSetAgg removed 0 of 25 vertices. (0 local)  1 selected.
		[0]PCGAMGProlongator_AGG New grid 1 nodes
			PCGAMGOptprol_AGG smooth P0: max eigen=1.192793e+00 min=7.360814e-01 PC=jacobi
		[0]PCSetUp_GAMG 4) N=1, n data cols=1, nnz/row (ave)=1, 1 active pes
	[0]PCSetUp_GAMG 5 levels, grid complexity = 1.03532
  0 KSP Residual norm 1.235315204866e+03 
  1 KSP Residual norm 1.492638270389e+02 
Linear solve converged due to CONVERGED_RTOL iterations 1
  0 KSP Residual norm 1.235315204866e+03 
  1 KSP Residual norm 1.492638270389e+02 
  2 KSP Residual norm 3.133531199322e+01 
  3 KSP Residual norm 8.712624807479e+00 
  4 KSP Residual norm 2.244540223057e+00 
  5 KSP Residual norm 5.638919404134e-01 
  6 KSP Residual norm 1.592830182378e-01 
  7 KSP Residual norm 4.195227629677e-02 
  8 KSP Residual norm 1.144211047038e-02 
  9 KSP Residual norm 2.802821534650e-03 
 10 KSP Residual norm 8.272846684841e-04 
Linear solve converged due to CONVERGED_RTOL iterations 10
  0 KSP Residual norm 1.235315204866e+03 
  1 KSP Residual norm 1.492638270389e+02 
  2 KSP Residual norm 3.133531199322e+01 
  3 KSP Residual norm 8.712624807479e+00 
  4 KSP Residual norm 2.244540223057e+00 
  5 KSP Residual norm 5.638919404134e-01 
  6 KSP Residual norm 1.592830182378e-01 
  7 KSP Residual norm 4.195227629677e-02 
  8 KSP Residual norm 1.144211047038e-02 
  9 KSP Residual norm 2.802821534651e-03 
 10 KSP Residual norm 8.272846684842e-04 
Linear solve converged due to CONVERGED_RTOL iterations 10
  0 KSP Residual norm 1.235315204866e+03 
  1 KSP Residual norm 1.492638270389e+02 
  2 KSP Residual norm 3.133531199322e+01 
  3 KSP Residual norm 8.712624807479e+00 
  4 KSP Residual norm 2.244540223057e+00 
  5 KSP Residual norm 5.638919404134e-01 
  6 KSP Residual norm 1.592830182378e-01 
  7 KSP Residual norm 4.195227629677e-02 
  8 KSP Residual norm 1.144211047038e-02 
  9 KSP Residual norm 2.802821534651e-03 
 10 KSP Residual norm 8.272846684841e-04 
Linear solve converged due to CONVERGED_RTOL iterations 10
  0 KSP Residual norm 1.235315204866e+03 
  1 KSP Residual norm 1.492638270389e+02 
  2 KSP Residual norm 3.133531199322e+01 
  3 KSP Residual norm 8.712624807479e+00 
  4 KSP Residual norm 2.244540223057e+00 
  5 KSP Residual norm 5.638919404133e-01 
  6 KSP Residual norm 1.592830182379e-01 
  7 KSP Residual norm 4.195227629677e-02 
  8 KSP Residual norm 1.144211047038e-02 
  9 KSP Residual norm 2.802821534638e-03 
 10 KSP Residual norm 8.272846684856e-04 
Linear solve converged due to CONVERGED_RTOL iterations 10
  0 KSP Residual norm 1.235315204866e+03 
  1 KSP Residual norm 1.492638270389e+02 
  2 KSP Residual norm 3.133531199322e+01 
  3 KSP Residual norm 8.712624807479e+00 
  4 KSP Residual norm 2.244540223057e+00 
  5 KSP Residual norm 5.638919404134e-01 
  6 KSP Residual norm 1.592830182378e-01 
  7 KSP Residual norm 4.195227629677e-02 
  8 KSP Residual norm 1.144211047038e-02 
  9 KSP Residual norm 2.802821534650e-03 
 10 KSP Residual norm 8.272846684844e-04 
Linear solve converged due to CONVERGED_RTOL iterations 10
  0 KSP Residual norm 1.235315204866e+03 
  1 KSP Residual norm 1.492638270389e+02 
  2 KSP Residual norm 3.133531199322e+01 
  3 KSP Residual norm 8.712624807479e+00 
  4 KSP Residual norm 2.244540223057e+00 
  5 KSP Residual norm 5.638919404134e-01 
  6 KSP Residual norm 1.592830182379e-01 
  7 KSP Residual norm 4.195227629677e-02 
  8 KSP Residual norm 1.144211047038e-02 
  9 KSP Residual norm 2.802821534647e-03 
 10 KSP Residual norm 8.272846684844e-04 
Linear solve converged due to CONVERGED_RTOL iterations 10
  0 KSP Residual norm 1.235315204866e+03 
  1 KSP Residual norm 1.492638270389e+02 
  2 KSP Residual norm 3.133531199322e+01 
  3 KSP Residual norm 8.712624807479e+00 
  4 KSP Residual norm 2.244540223057e+00 
  5 KSP Residual norm 5.638919404134e-01 
  6 KSP Residual norm 1.592830182378e-01 
  7 KSP Residual norm 4.195227629677e-02 
  8 KSP Residual norm 1.144211047038e-02 
  9 KSP Residual norm 2.802821534648e-03 
 10 KSP Residual norm 8.272846684842e-04 
Linear solve converged due to CONVERGED_RTOL iterations 10
  0 KSP Residual norm 1.235315204866e+03 
  1 KSP Residual norm 1.492638270389e+02 
  2 KSP Residual norm 3.133531199322e+01 
  3 KSP Residual norm 8.712624807479e+00 
  4 KSP Residual norm 2.244540223057e+00 
  5 KSP Residual norm 5.638919404134e-01 
  6 KSP Residual norm 1.592830182378e-01 
  7 KSP Residual norm 4.195227629677e-02 
  8 KSP Residual norm 1.144211047038e-02 
  9 KSP Residual norm 2.802821534649e-03 
 10 KSP Residual norm 8.272846684841e-04 
Linear solve converged due to CONVERGED_RTOL iterations 10
************************************************************************************************************************
***             WIDEN YOUR WINDOW TO 120 CHARACTERS.  Use 'enscript -r -fCourier9' to print this document            ***
************************************************************************************************************************

---------------------------------------------- PETSc Performance Summary: ----------------------------------------------

../testAMRBC3d.Linux.64.CC.ftn.OPTHIGH.MPI.PETSC.ex on a arch-xc30-opt64-intel named nid03560 with 256 processors, by madams Thu Mar 12 00:52:54 2015
Using Petsc Release Version 3.5.3, unknown 

                         Max       Max/Min        Avg      Total 
Time (sec):           6.674e+00      1.00438   6.655e+00
Objects:              1.236e+03      1.00000   1.236e+03
Flops:                1.404e+09      1.02517   1.393e+09  3.567e+11
Flops/sec:            2.111e+08      1.02624   2.094e+08  5.360e+10
MPI Messages:         2.848e+04      3.63701   1.998e+04  5.116e+06
MPI Message Lengths:  4.298e+07      2.10003   1.774e+03  9.073e+09
MPI Reductions:       9.600e+02      1.00000

Flop counting convention: 1 flop = 1 real number operation of type (multiply/divide/add/subtract)
                            e.g., VecAXPY() for real vectors of length N --> 2N flops
                            and VecAXPY() for complex vectors of length N --> 8N flops

Summary of Stages:   ----- Time ------  ----- Flops -----  --- Messages ---  -- Message Lengths --  -- Reductions --
                        Avg     %Total     Avg     %Total   counts   %Total     Avg         %Total   counts   %Total 
 0:      Main Stage: 5.9672e-01   9.0%  0.0000e+00   0.0%  9.168e+03   0.2%  2.162e+00        0.1%  2.100e+01   2.2% 
 1:       The solve: 6.0578e+00  91.0%  3.5671e+11 100.0%  5.106e+06  99.8%  1.771e+03       99.9%  9.380e+02  97.7% 

------------------------------------------------------------------------------------------------------------------------
See the 'Profiling' chapter of the users' manual for details on interpreting output.
Phase summary info:
   Count: number of times phase was executed
   Time and Flops: Max - maximum over all processors
                   Ratio - ratio of maximum to minimum over all processors
   Mess: number of messages sent
   Avg. len: average message length (bytes)
   Reduct: number of global reductions
   Global: entire computation
   Stage: stages of a computation. Set stages with PetscLogStagePush() and PetscLogStagePop().
      %T - percent time in this phase         %F - percent flops in this phase
      %M - percent messages in this phase     %L - percent message lengths in this phase
      %R - percent reductions in this phase
   Total Mflop/s: 10e-6 * (sum of flops over all processors)/(max time over all processors)
------------------------------------------------------------------------------------------------------------------------
Event                Count      Time (sec)     Flops                             --- Global ---  --- Stage ---   Total
                   Max Ratio  Max     Ratio   Max  Ratio  Mess   Avg len Reduct  %T %F %M %L %R  %T %F %M %L %R Mflop/s
------------------------------------------------------------------------------------------------------------------------

--- Event Stage 0: Main Stage

Grid setup             1 1.0 3.9730e-03 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   1  0  0  0  0     0
createMatrix           1 1.0 3.4526e-01 1.0 0.00e+00 0.0 9.2e+03 1.2e+03 1.5e+01  5  0  0  0  2  58  0100100 71     0
MatAssemblyBegin       1 1.0 1.0785e-0244.2 0.00e+00 0.0 0.0e+00 0.0e+00 2.0e+00  0  0  0  0  0   0  0  0  0 10     0
MatAssemblyEnd         1 1.0 2.0757e-02 1.0 0.00e+00 0.0 9.2e+03 1.2e+03 8.0e+00  0  0  0  0  1   3  0100100 38     0
VecSet                 1 1.0 1.6928e-05 2.8 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0

--- Event Stage 1: The solve

FAS-FMG-solve          8 1.0 2.1860e-01 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  3  0  0  0  0   4  0  0  0  0     0
Chombo-solve           8 1.0 3.1679e-01 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  5  0  0  0  0   5  0  0  0  0     0
PETSc-AMG-solve        8 1.0 3.2848e+00 1.0 1.28e+09 1.0 4.5e+06 1.2e+03 1.7e+02 49 91 87 60 18  54 91 87 60 18 99313
FAS-Vcycle-solve       8 1.0 6.3385e-01 1.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00 10  0  0  0  0  10  0  0  0  0     0
MatMult              481 1.0 9.5925e-01 1.6 3.21e+08 1.0 1.3e+06 1.7e+03 0.0e+00 10 23 25 24  0  11 23 25 24  0 84635
MatMultAdd           360 1.0 1.2775e-01 1.6 2.55e+07 1.0 4.3e+05 1.7e+02 0.0e+00  2  2  8  1  0   2  2  8  1  0 50430
MatMultTranspose     360 1.0 8.6439e-02 1.6 2.55e+07 1.0 4.3e+05 1.7e+02 0.0e+00  1  2  8  1  0   1  2  8  1  0 74532
MatSolve              90 0.0 6.6996e-05 0.0 9.00e+01 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     1
MatSOR               720 1.0 2.5256e+00 1.4 8.82e+08 1.0 2.5e+06 1.4e+03 0.0e+00 37 63 49 38  0  41 63 49 38  0 88743
MatLUFactorSym         1 1.0 2.5034e-05 2.8 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatLUFactorNum         1 1.0 6.1989e-06 6.5 1.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatConvert             4 1.0 1.4035e-02 3.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatScale              12 1.0 1.2171e-02 1.2 2.12e+06 1.0 9.2e+03 1.4e+03 0.0e+00  0  0  0  0  0   0  0  0  0  0 44014
MatResidual          360 1.0 6.6516e-01 2.3 1.65e+08 1.0 8.3e+05 1.4e+03 0.0e+00  5 12 16 13  0   5 12 16 13  0 62797
MatAssemblyBegin      72 1.0 1.2669e-01 2.5 0.00e+00 0.0 6.8e+04 9.7e+03 8.0e+01  1  0  1  7  8   1  0  1  7  9     0
MatAssemblyEnd        72 1.0 1.7210e-01 1.1 0.00e+00 0.0 1.0e+05 5.7e+02 2.2e+02  3  0  2  1 23   3  0  2  1 24     0
MatGetRow         168975 1.0 1.7532e-02 1.1 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatGetRowIJ            1 0.0 4.0531e-06 0.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatGetOrdering         1 0.0 2.3842e-05 0.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatCoarsen             4 1.0 2.8018e-02 2.4 0.00e+00 0.0 6.5e+04 2.6e+03 2.1e+01  0  0  1  2  2   0  0  1  2  2     0
MatAXPY                4 1.0 1.1169e-02 1.2 0.00e+00 0.0 0.0e+00 0.0e+00 8.0e+00  0  0  0  0  1   0  0  0  0  1     0
MatTranspose           4 1.0 7.3426e-02 1.0 0.00e+00 0.0 6.9e+04 5.2e+03 4.8e+01  1  0  1  4  5   1  0  1  4  5     0
MatMatMult             4 1.0 3.9132e-02 1.0 1.83e+06 1.0 5.2e+04 9.8e+02 6.4e+01  1  0  1  1  7   1  0  1  1  7 11860
MatMatMultSym          4 1.0 2.5009e-02 1.0 0.00e+00 0.0 4.3e+04 9.0e+02 5.6e+01  0  0  1  0  6   0  0  1  0  6     0
MatMatMultNum          4 1.0 1.4339e-02 1.0 1.83e+06 1.0 9.2e+03 1.4e+03 8.0e+00  0  0  0  0  1   0  0  0  0  1 32367
MatPtAP                4 1.0 1.4018e-01 1.2 1.09e+07 1.1 7.7e+04 2.4e+03 6.8e+01  2  1  2  2  7   2  1  2  2  7 19333
MatPtAPSymbolic        4 1.0 7.2668e-02 1.5 0.00e+00 0.0 5.1e+04 3.1e+03 2.8e+01  1  0  1  2  3   1  0  1  2  3     0
MatPtAPNumeric         4 1.0 6.7529e-02 1.0 1.09e+07 1.1 2.6e+04 9.3e+02 4.0e+01  1  1  1  0  4   1  1  1  0  4 40133
MatTrnMatMult          4 1.0 8.3194e-01 1.0 5.25e+07 1.1 5.5e+04 3.7e+04 7.6e+01 12  4  1 22  8  14  4  1 22  8 15629
MatTrnMatMultSym       4 1.0 4.4606e-01 1.0 0.00e+00 0.0 4.6e+04 2.3e+04 6.8e+01  7  0  1 12  7   7  0  1 12  7     0
MatTrnMatMultNum       4 1.0 3.8597e-01 1.0 5.25e+07 1.1 9.2e+03 1.1e+05 8.0e+00  6  4  0 11  1   6  4  0 11  1 33688
MatGetLocalMat        20 1.0 2.5949e-02 2.4 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
MatGetBrAoCol         12 1.0 2.9221e-02 5.1 0.00e+00 0.0 6.4e+04 2.9e+03 0.0e+00  0  0  1  2  0   0  0  1  2  0     0
MatGetSymTrans         8 1.0 1.4532e-03 2.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecMDot              121 1.0 4.0241e-0113.9 3.26e+07 1.0 0.0e+00 0.0e+00 1.2e+02  1  2  0  0 13   1  2  0  0 13 20728
VecNorm              134 1.0 5.2585e-02 7.1 6.64e+06 1.0 0.0e+00 0.0e+00 1.3e+02  0  0  0  0 14   0  0  0  0 14 32299
VecScale            1214 1.0 2.8827e-03 1.4 5.34e+06 1.2 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0 442578
VecCopy               13 1.0 1.3707e-03 2.6 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecSet              1434 1.0 8.1959e-03 2.3 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecAXPY               13 1.0 7.8535e-04 2.1 6.57e+05 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0 214082
VecAYPX              360 1.0 6.2854e-03 1.9 3.04e+06 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0 122671
VecMAXPY             134 1.0 1.7718e-02 1.6 3.86e+07 1.0 0.0e+00 0.0e+00 0.0e+00  0  3  0  0  0   0  3  0  0  0 557147
VecAssemblyBegin      31 1.0 6.4771e-03 1.5 0.00e+00 0.0 0.0e+00 0.0e+00 8.7e+01  0  0  0  0  9   0  0  0  0  9     0
VecAssemblyEnd        31 1.0 1.1945e-04 6.0 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecPointwiseMult      44 1.0 1.4215e-03 2.1 3.72e+05 1.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0 66297
VecScatterBegin     2315 1.0 7.8573e-02 2.9 0.00e+00 0.0 4.7e+06 1.3e+03 0.0e+00  1  0 92 65  0   1  0 92 65  0     0
VecScatterEnd       2315 1.0 1.6862e+0013.6 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  3  0  0  0  0   4  0  0  0  0     0
VecSetRandom           4 1.0 2.7180e-04 1.1 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
VecNormalize         134 1.0 5.3876e-02 6.1 9.96e+06 1.0 0.0e+00 0.0e+00 1.3e+02  0  1  0  0 14   0  1  0  0 14 47287
KSPGMRESOrthog       121 1.0 4.1195e-0110.1 6.52e+07 1.0 0.0e+00 0.0e+00 1.2e+02  1  5  0  0 13   1  5  0  0 13 40496
KSPSetUp              11 1.0 1.4958e-03 2.1 0.00e+00 0.0 0.0e+00 0.0e+00 8.0e+00  0  0  0  0  1   0  0  0  0  1     0
KSPSolve               9 1.0 4.8796e+00 1.0 1.40e+09 1.0 5.1e+06 1.8e+03 9.3e+02 73100100100 97  81100100100 99 73102
PCGAMGgraph_AGG        4 1.0 3.1009e-01 1.0 1.83e+06 1.0 1.2e+05 5.7e+03 1.1e+02  5  0  2  8 11   5  0  2  8 12  1497
PCGAMGcoarse_AGG       4 1.0 8.6263e-01 1.0 5.25e+07 1.1 1.7e+05 1.4e+04 1.6e+02 13  4  3 26 17  14  4  3 26 17 15073
PCGAMGProl_AGG         4 1.0 2.5277e-02 1.6 0.00e+00 0.0 3.9e+04 1.7e+03 9.6e+01  0  0  1  1 10   0  0  1  1 10     0
PCGAMGPOpt_AGG         4 1.0 1.1665e-01 1.0 2.97e+07 1.0 1.4e+05 1.2e+03 2.0e+02  2  2  3  2 21   2  2  3  2 21 64647
PCSetUp                2 1.0 1.5197e+00 1.0 9.44e+07 1.1 5.6e+05 6.2e+03 7.6e+02 23  7 11 38 79  25  7 11 38 81 15607
PCSetUpOnBlocks       90 1.0 1.3995e-04 2.2 1.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
PCApply               90 1.0 3.0195e+00 1.1 1.10e+09 1.0 4.2e+06 1.1e+03 0.0e+00 45 78 82 52  0  49 78 82 52  0 92327
SFSetGraph             8 1.0 9.0480e-04 2.7 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
SFBcastBegin          21 1.0 1.9073e-02 7.2 0.00e+00 0.0 6.5e+04 2.6e+03 0.0e+00  0  0  1  2  0   0  0  1  2  0     0
SFBcastEnd            21 1.0 2.2609e-03 1.9 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
SFReduceBegin          4 1.0 7.6022e-03 3.0 0.00e+00 0.0 2.3e+04 1.1e+03 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
SFReduceEnd            4 1.0 5.2691e-04 5.3 0.00e+00 0.0 0.0e+00 0.0e+00 0.0e+00  0  0  0  0  0   0  0  0  0  0     0
------------------------------------------------------------------------------------------------------------------------

Memory usage is given in bytes:

Object Type          Creations   Destructions     Memory  Descendants' Mem.
Reports information only for process 0.

--- Event Stage 0: Main Stage

              Matrix     4              3     30353480     0
              Vector     4              2        28696     0
      Vector Scatter     1              1         1196     0
           Index Set     2              2        27048     0
       Krylov Solver     1              0            0     0
      Preconditioner     1              0            0     0
              Viewer     1              0            0     0

--- Event Stage 1: The solve

              Matrix   129            130    170947624     0
      Matrix Coarsen     4              4         2768     0
              Vector   938            940     62869328     0
      Vector Scatter    34             34        40840     0
           Index Set    83             83       214064     0
       Krylov Solver    11             12       177664     0
      Preconditioner    11             12        12664     0
Star Forest Bipartite Graph     8              8         7424     0
         PetscRandom     4              4         2720     0
========================================================================================================================
Average time to get PetscTime(): 0
Average time for MPI_Barrier(): 1.4019e-05
Average time for zero size MPI_Send(): 1.08592e-06
#PETSc Option Table entries:
-amg true
-bc_order 1
-box_stencil false
-chmg true
-chombo_bottom_amg false
-crs_box_size 8
-domain_length 1,2,2
-fine_box_size 32
-fmg true
-ksp_converged_reason
-ksp_gmres_restart 50
-ksp_max_it 50
-ksp_monitor
-ksp_norm_type preconditioned
-ksp_type gmres
-log_summary
-mat_partitioning_type parmetis
-mg_coarse_ksp_type preonly
-mg_levels_ksp_type richardson
-mg_levels_pc_type sor
-n_amr_refine 1
-n_coarse_patches 4
-n_inc 1
-nlevels 0
-npost 2
-npre 2
-num_repeat 8
-pc_gamg_agg_nsmooths 1
-pc_gamg_coarse_eq_limit 10
-pc_gamg_process_eq_limit 200
-pc_gamg_repartition false
-pc_gamg_square_graph true
-pc_gamg_sym_graph true
-pc_gamg_threshold -.005
-pc_gamg_verbose 2
-pc_type gamg
-refinement_type uniform
-smoother_type gsrb
-vcycle_type 1
-verbosity 2
#End of PETSc Option Table entries
Compiled without FORTRAN kernels
Compiled with full precision matrices (default)
sizeof(short) 2 sizeof(int) 4 sizeof(long) 8 sizeof(void*) 8 sizeof(PetscScalar) 8 sizeof(PetscInt) 8
Configure options: --COPTFLAGS="-fast -no-ipo" --CXXOPTFLAGS="-fast -no-ipoi" --FOPTFLAGS="-fast -no-ipo" --download-hypre --download-parmetis --download-metis --with-cc=cc --with-clib-autodetect=0 --with-cxx=CC --with-cxxlib-autodetect=0 --with-debugging=0 --with-fc=ftn --with-fortranlib-autodetect=0 --with-hdf5-dir=/opt/cray/hdf5-parallel/1.8.13/intel/140/ --with-shared-libraries=0 --with-x=0 --with-mpiexec=aprun LIBS=-lstdc++ --with-64-bit-indices PETSC_ARCH=arch-xc30-opt64-intel
-----------------------------------------
Libraries compiled on Sat Feb 14 05:21:21 2015 on edison02 
Machine characteristics: Linux-3.0.101-0.46-default-x86_64-with-SuSE-11-x86_64
Using PETSc directory: /global/homes/m/madams/petsc_maint
Using PETSc arch: arch-xc30-opt64-intel
-----------------------------------------

Using C compiler: cc  -fast -no-ipo  ${COPTFLAGS} ${CFLAGS}
Using Fortran compiler: ftn  -fast -no-ipo   ${FOPTFLAGS} ${FFLAGS} 
-----------------------------------------

Using include paths: -I/global/homes/m/madams/petsc_maint/arch-xc30-opt64-intel/include -I/global/homes/m/madams/petsc_maint/include -I/global/homes/m/madams/petsc_maint/include -I/global/homes/m/madams/petsc_maint/arch-xc30-opt64-intel/include -I/global/u2/m/madams/petsc_maint/arch-xc30-opt64-intel/include -I/opt/cray/hdf5-parallel/1.8.13/intel/140/include
-----------------------------------------

Using C linker: cc
Using Fortran linker: ftn
Using libraries: -Wl,-rpath,/global/homes/m/madams/petsc_maint/arch-xc30-opt64-intel/lib -L/global/homes/m/madams/petsc_maint/arch-xc30-opt64-intel/lib -lpetsc -Wl,-rpath,/global/u2/m/madams/petsc_maint/arch-xc30-opt64-intel/lib -L/global/u2/m/madams/petsc_maint/arch-xc30-opt64-intel/lib -lHYPRE -lparmetis -lmetis -lpthread -lssl -lcrypto -Wl,-rpath,/opt/cray/hdf5-parallel/1.8.13/intel/140/lib -L/opt/cray/hdf5-parallel/1.8.13/intel/140/lib -lhdf5hl_fortran -lhdf5_fortran -lhdf5_hl -lhdf5 -ldl -lstdc++ 
-----------------------------------------

Application 10700437 resources: utime ~1732s, stime ~95s, Rss ~356076, inblocks ~598192, outblocks ~1703043
