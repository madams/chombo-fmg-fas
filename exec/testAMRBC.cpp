#ifdef CH_LANG_CC
/*
 *      _______              __
 *     / ___/ /  ___  __ _  / /  ___
 *    / /__/ _ \/ _ \/  V \/ _ \/ _ \
 *    \___/_//_/\___/_/_/_/_.__/\___/
 *    Please refer to Copyright.txt, in Chombo's root directory.
 */
#endif

#include <iostream>
#include <fstream>

#include "ParmParse.H"
#include "AMRFASPoissonOp.H"
#include "AMRMultiGrid.H"
#include "AMRPoissonOp.H"
#include "NewPoissonOp.H"
#include "BiCGStabSolver.H"
#include "PetscCompGridPois.H"
#include "PetscSolver.H"
#ifdef CH_USE_PETSC
#include <petscksp.h>

static char help[] = "Test AMR FAS solvers with Laplacian on tree grids, homo Diri BCs.\n\n";

static PetscInt s_verbosity = 1; 
static PetscInt s_order = 2;
static PetscInt s_bcorder = 1;
static PetscInt s_npre = 4;
static PetscInt s_npost = 4;
static PetscInt s_n_amr_refine = 2;
static PetscReal s_damping_factor = .66;
static PetscInt s_crs_box_size = 8;
static PetscInt s_fine_box_size = 8;
static PetscInt s_num_inter = 0;
static PetscInt s_inc = 1;
static PetscInt s_num_repeat = 4;
static const PetscInt s_refRatio = 2;
static PetscBool s_chmg = PETSC_TRUE;
static PetscBool s_amg = PETSC_TRUE;
static PetscBool s_fmg = PETSC_TRUE;
static PetscBool s_chombo_bottom_amg = PETSC_FALSE;
static PetscBool s_repartition = PETSC_FALSE;
static PetscInt s_vcycle_type = 0;
static int s_n_extra_nnz = 0;
static PetscInt s_n_coarse_patches = 2;
static PetscBool s_boxstencil = PETSC_FALSE;
static FAS_SMOOTHER_type s_smoothertype = FAS_GSRB;
static IntVect s_iLength(IntVect::Unit); // must be an integer
#if defined(PETSC_USE_LOG)
static PetscLogEvent s_events[16];
static PetscLogStage s_stage;
#endif
typedef struct MFree_TAG {
  PetscCompGridPois *m_compgrid;
  AMRMultiGrid<LevelData<FArrayBox> > *m_chmg;
  Vector<LevelData<FArrayBox>*> *m_phi_mfree;
  Vector<LevelData<FArrayBox>*> *m_Lphi_mfree;
  Vector<RefCountedPtr<LevelData<FArrayBox> > > *m_phi_ref;
  Vector<RefCountedPtr<LevelData<FArrayBox> > > *m_Lphi_ref;
}mfree_amr_ctx;

#undef __FUNCT__
#define __FUNCT__ "apply_mfree"
static PetscErrorCode apply_mfree(Mat A, Vec x, Vec f)
{
  CH_TIME("apply_mfree");
  void *ctx;
  PetscErrorCode ierr;
  PetscFunctionBeginUser;

  MatShellGetContext(A, &ctx);
  mfree_amr_ctx *tthis = (mfree_amr_ctx*)ctx;
#if defined(PETSC_USE_LOG)
    PetscLogEventBegin(s_events[6],0,0,0,0);
#endif
    ierr = tthis->m_compgrid->putPetscInChombo(x,*tthis->m_phi_mfree);CHKERRQ(ierr);

  if (false) { // FAS operator does not work right!
    // tthis->m_fasmg->computeAMROperator( *tthis->m_Lphi_ref,
    // 					*tthis->m_phi_ref,
    // 					tthis->m_phi_ref->size()-1,
    // 					0,
    // 					true);
  }
  else {
    tthis->m_chmg->computeAMROperator( *tthis->m_Lphi_mfree,
				       *tthis->m_phi_mfree,
				       tthis->m_phi_mfree->size()-1,
				       0,
				       true);
  }
  ierr = tthis->m_compgrid->putChomboInPetsc(*tthis->m_Lphi_mfree,f);CHKERRQ(ierr);
#if defined(PETSC_USE_LOG)
    PetscLogEventEnd(s_events[6],0,0,0,0);
#endif
  PetscFunctionReturn(0);
}

enum refinementType {uniform_rt,half_rt,plane_rt,line_rt,point_rt};
static enum refinementType s_ref_type = uniform_rt;

static void ParseValue(Real* pos,
		       int* dir,
		       Side::LoHiSide* side,
		       Real* a_values)
{
  a_values[0]=0.;
}
//
// BC
//
static void ParseBC( FArrayBox& a_state,
		     const Box& a_valid,
		     const ProblemDomain& a_domain,
		     Real a_dx,
		     bool a_homogeneous)
{

  if (!a_domain.domainBox().contains(a_state.box())) {
    Box valid = a_valid;
    for (int idir=0; idir<CH_SPACEDIM; ++idir) {
      // don't do anything if periodic
      if (!a_domain.isPeriodic(idir)) {
	Box ghostBoxLo = adjCellBox(valid, idir, Side::Lo, 1);
	Box ghostBoxHi = adjCellBox(valid, idir, Side::Hi, 1);
	if (!a_domain.domainBox().contains(ghostBoxLo)) {
	  Box valid2(valid);
	  if (s_boxstencil) {
	    for (int idir2 = 0; idir2 < CH_SPACEDIM; idir2++) {
	      if (idir2!=idir) valid2.grow(idir2,1);
	    }
	  }
	  DiriBC(a_state,
		 valid2,
		 a_dx,
		 true,
		 ParseValue,
		 idir,
		 Side::Lo,
		 s_bcorder);
	}
	if (!a_domain.domainBox().contains(ghostBoxHi)) {
	  Box valid2(valid);
	  if (s_boxstencil) {
	    for (int idir2 = 0; idir2 < CH_SPACEDIM; idir2++) {
	      if (idir2!=idir) valid2.grow(idir2,1);
	    }
	  }
	  DiriBC(a_state,
		 valid2,
		 a_dx,
		 true,
		 ParseValue,
		 idir,
		 Side::Hi,
		 s_bcorder);
	}
      } // end if is not periodic in ith direction
    }
  }
}
//
// plotting
//
PetscErrorCode plotAll( Vector<LevelData<FArrayBox> *> &a_phi,
			Vector<LevelData<FArrayBox> *> &a_rhs,
			Vector<RefCountedPtr<LevelData<FArrayBox> > > &a_exact,
			Real a_errNorm[2], string a_fname, Real a_cdx,
			Vector<DisjointBoxLayout> &a_grids,
			Vector<int> &a_refratios,
			Vector<ProblemDomain> &a_domains,
			int a_sub_id = -1 )
{
  CH_TIME("plotAll");
  int nLev = a_phi.size();
  Real dx;
  Vector<LevelData<FArrayBox>* > plotData(nLev, NULL);

  for (int ilev=0;ilev<nLev;ilev++) {
    plotData[ilev] = new LevelData<FArrayBox>(a_grids[ilev],4,IntVect::Zero);
    const DisjointBoxLayout& dbl = a_grids[ilev];
    for (DataIterator dit(dbl); dit.ok(); ++dit) {
      FArrayBox& exactfab = (*plotData[ilev])[dit];
      exactfab.setVal(0.);
    }
  }
  
  a_errNorm[0] = a_errNorm[1] = 0;
  dx = a_cdx;
  for (int ilev=0 ; ilev < nLev ; ilev++, dx /= s_refRatio ) {
    Interval phiInterval(0,0);
    a_phi[ilev]->copyTo(phiInterval, *plotData[ilev], phiInterval);
    Interval rhsInterval(1,1);
    a_rhs[ilev]->copyTo(phiInterval, *plotData[ilev], rhsInterval);
    Interval exInterval(2,2);
    a_exact[ilev]->copyTo(phiInterval, *plotData[ilev], exInterval);
    // use phi for error
    const DisjointBoxLayout& dbl = a_grids[ilev];
    for (DataIterator dit(dbl); dit.ok(); ++dit) {
      FArrayBox& exactfab = (*a_exact[ilev])[dit];
      FArrayBox& phiFAB = (*a_phi[ilev])[dit];
      Box region = exactfab.box();
      for (BoxIterator bit(region); bit.ok(); ++bit) {
	IntVect iv = bit();
	phiFAB(iv,0) = phiFAB(iv,0) - exactfab(iv,0);
      }
    }

    // zero error on covered
    if (ilev!=nLev-1) {
      // zero out fine cover
      DisjointBoxLayout dblCoarsenedFine;
      Copier copier;
      coarsen(dblCoarsenedFine, a_grids[ilev+1], a_refratios[ilev]); // coarsens entire grid
      copier.define(dblCoarsenedFine, dbl, IntVect::Zero);
      LevelDataOps<FArrayBox> ops;
      ops.copyToZero(*a_phi[ilev],copier);
    }
      
    // copy in
    Interval errInterval(3,3);
    a_phi[ilev]->copyTo(phiInterval, *plotData[ilev], errInterval);
 
    // get error norms
    for (DataIterator dit(dbl); dit.ok(); ++dit) {
      Box region = dbl[dit];
      FArrayBox& phifab = (*a_phi[ilev])[dit];
      Real mnorm = phifab.norm(region,0);
      if (mnorm>a_errNorm[0]) a_errNorm[0] = mnorm;
      mnorm = phifab.norm(region,1)*pow(dx,SpaceDim);
      a_errNorm[1] += mnorm;
    }
  }
  {
    double error;
    MPI_Allreduce( &a_errNorm[0], &error, 1, MPI_DOUBLE, MPI_MAX, PETSC_COMM_WORLD );
    a_errNorm[0] = error;
    MPI_Allreduce( &a_errNorm[1], &error, 1, MPI_DOUBLE, MPI_SUM, PETSC_COMM_WORLD );
    a_errNorm[1] = error;
  }
  if (s_verbosity>3) pout() << "\t\t plot |e|_inf=" << a_errNorm[0] << " |e|_1=" << a_errNorm[1] << endl;

  // plot
  if (true) {  
    CH_TIME("plot");
    char suffix[30];
    if (a_sub_id>=0) sprintf(suffix, "%dd.%d.hdf5",SpaceDim,a_sub_id);
    else sprintf(suffix, "%dd.hdf5",SpaceDim);
    a_fname += suffix;
    Vector<string> varNames(4);
    varNames[0] = "phi";
    varNames[1] = "rhs";
    varNames[2] = "exact";
    varNames[3] = "error";
#ifdef CH_USE_HDF5
    Real bogusVal = 1.0;
    WriteAMRHierarchyHDF5(a_fname,
			  a_grids,
			  plotData,
			  varNames,
			  a_domains[0].domainBox(),
			  a_cdx,
			  bogusVal,
			  bogusVal,
			  a_refratios,
			  nLev);
#endif
  }

  for (int ilev=0;ilev<nLev;ilev++) delete plotData[ilev];

  PetscFunctionReturn(0);
}
//
// problem defs
//
enum probTypes {zeroRHS = 0,
                unityRHS,
                sinusoidal,
                polynomial,
                numProbTypes};

//int s_probtype = zeroRHS;
static int s_probtype = polynomial;
//int s_probtype = gaussians;

/**
 * Test fourth-order FAS FMG Multigrid Poisson solver:
 *    Laplace(u) = rhs
 *  with Dirichlet boundary condition and
 *  with 's_probtype == sinusoidal' exact u := a sin function
 *
 */

const Real kx[] = {  2*M_PI, 3*M_PI, 4*M_PI };
const Real ky[] = {  M_PI,   2*M_PI, 5*M_PI };
const Real kz[] = {  3*M_PI, 4*M_PI, 7*M_PI };
const int nk = sizeof(kx)/sizeof(kx[0]);

/**
 *  Multigrid Poisson solver:
 *    Laplace(u) = rhs on [0,1]
 *
 *  A numerical test on page 64 of
 *   `A MULTIGRID TUTORIAL by Briggs, Henson & McCormick'
 *   is duplicated here.
 *  For 2D
 *  exact u := (x^4-x^2)(y^4-y^2)(z^4-z^2)
 *
 */
Real rhsFuncPoly(const RealVect& a_x, const RealVect& a_L)
{
  Real r = 0.0;
  const RealVect x2 = a_x*a_x;
  const RealVect a = x2*(a_L*a_L - x2);
  const RealVect b = 2*a_L*a_L - 12*x2;
  // cross products of coordinates prevents the use of D_TERM
  if (SpaceDim==1)
    r = b[0];
  else if (SpaceDim==2)
    r = b[0]*a[1] + a[0]*b[1];
  else if (SpaceDim==3)
    r = b[0]*a[1]*a[2] + a[0]*b[1]*a[2] + a[0]*a[1]*b[2];
  else
    MayDay::Error("Invalid Dimension!");
  return -r; // lets keep soluion positive
}

// exact u
Real exactSolutionPoly(const RealVect& a_x, const RealVect& a_L)
{
  RealVect uu,x2 = a_x*a_x;
  uu = x2*(a_L*a_L - x2);
  return uu.product(); // not sure if this correct for D != 2
}

#define LINE_TOL (1.e-7)
static bool is_above_plane(const RealVect x) // could be fixed with L != 1^3
{
  return (x[0] + x[1] + (CH_SPACEDIM==2 ? 1. : x[2]) > 2.5 + LINE_TOL);
}
static bool is_on_line(const RealVect x) // could be fixed with L != 1^3
{
  return ( abs(x[0]-x[1]) < LINE_TOL && (CH_SPACEDIM==2 || abs(x[0]-x[2]) < LINE_TOL));
}
static bool is_on_point(const IntVect iv, const Box dom)
{
  bool ret = dom.contains(iv);
  return ret;
}
//
// setup grids
//
#undef __FUNCT__
#define __FUNCT__ "setupGrids"
PetscErrorCode setupGrids( Vector<int> &a_refratios,            // out
			   Vector<ProblemDomain> &a_domains,    // out
			   Vector<DisjointBoxLayout> &a_grids,  // out
			   const int a_nLevs,                   // in 
			   const PetscInt a_nrefinelevels,      // in 
			   Real &a_cdx,                         // out
			   const int a_inc                      // in 1+
			   )
{
  CH_TIME("setupGrids");
  bool isperiodic[3] = {false,false,false};
  PetscMPIInt nprocs;
  PetscFunctionBeginUser;
  if (s_refRatio != 2) MayDay::Error("refRatio != 2");
  a_refratios.resize(a_nLevs-1+s_num_inter);
  a_domains.resize(a_nLevs+s_num_inter);
  a_grids.resize(a_nLevs+s_num_inter);

  MPI_Comm_size(PETSC_COMM_WORLD,&nprocs);
  { // coarse uniform grids
    Vector<int> procAssign;
    Vector<Box> boxvector;
    Box levbox(IntVect::Zero,s_n_coarse_patches*s_crs_box_size*s_iLength - IntVect::Unit); 
    a_cdx = 1./(Real)s_crs_box_size/(Real)s_n_coarse_patches; // L / num cells on grid 0: output
    a_domains[0].define(levbox,isperiodic);
    domainSplit(levbox, boxvector, s_crs_box_size, s_crs_box_size);
    if (boxvector.size() != nprocs) MayDay::Error("boxvector.size() != nprocs"); 
    LoadBalance(procAssign,boxvector);
    a_grids[0].define(boxvector,procAssign);
    for (int ilev=1;ilev<=s_num_inter;ilev++) {
      const int rrr = pow(s_refRatio,ilev); // amount of refinement from base
      levbox.refine(s_refRatio);            // incrementally refine
      a_domains[ilev].define(levbox,isperiodic);
      domainSplit(levbox, boxvector, rrr*s_crs_box_size, rrr*s_crs_box_size);
      a_grids[ilev].define(boxvector,procAssign);
      a_refratios[ilev-1] = s_refRatio; // out, just set whole thing
    }
  }
  if (a_nLevs==1) PetscFunctionReturn(0); // one level solve

  // make new domain & other outputs
  if (a_nLevs<a_inc+s_num_inter) MayDay::Error("a_nLevs<a_inc");
  for (int ilev=s_num_inter;ilev<a_inc+s_num_inter;ilev++) {
    Box next_dombox = a_domains[a_nLevs-1-a_inc+ilev].domainBox();
    next_dombox.refine(s_refRatio);
    a_domains[a_nLevs-a_inc+ilev].define(next_dombox,isperiodic); // fine grid out
    a_refratios[a_nLevs-a_inc+ilev-1] = s_refRatio; // out, just set whole thing
  }

  Vector<TreeIntVectSet> tagVects(a_nLevs); // start at base grid
  Box base_box(a_domains[s_num_inter].domainBox());
  base_box.coarsen(s_fine_box_size);    // now patches on 'coarse' grid
  if (base_box.numPts() != nprocs) MayDay::Error("base_box.numPts() != nprocs"); 
  tagVects[0].define(base_box); // start with all tags
  base_box.coarsen(2);          // size of point refinement region
  CH_assert(base_box.numPts()==(nprocs/pow(2,SpaceDim)));

  Real dx = 1./s_n_coarse_patches; // dx for entire patch on grid 0: L/num patches -- not used
  int nref = (a_nrefinelevels>a_nLevs-1) ? a_nLevs-1 : a_nrefinelevels, nUniRef = a_nLevs-nref-1, refFact;
  refFact = pow(s_refRatio,nUniRef);
  for (int ilev=1;ilev<=nref;ilev++,dx/=s_refRatio) {
    // center box for point
    Box pointbox(base_box), cdom(a_domains[ilev-1+s_num_inter].domainBox());
    cdom.coarsen(s_fine_box_size);    // now patches on coarse grid
    IntVect len(cdom.hiVect()), plen(pointbox.hiVect());
    len += IntVect::Unit;   plen += IntVect::Unit;
    len.coarsen(2);         plen.coarsen(2);
    pointbox.shift(len-plen); // move to center

    for (TreeIntVectSetIterator tit(tagVects[ilev-1]); tit.ok(); ++tit) {
      IntVect iv = tit();
      bool ishi,doit=false;
      if (s_ref_type==uniform_rt) doit = true;
      else if (s_ref_type == half_rt) {
	IntVect ivhi(iv); ivhi.shift(IntVect::Unit);
	RealVect x(iv); x *= dx;
	RealVect xhi(ivhi); xhi *= dx;
	x = (x + xhi)/2.;
	doit = (x[0] > .5 && x[1] > .5 && (SpaceDim>2 ? x[2] > .5 : true)); // needs to be fixed with L != 1^3
      }
      else if (s_ref_type==point_rt) {
	doit = pointbox.contains(iv);
      }
      else { // plane or line
	for (int id1=0;id1<2 && !doit;id1++) { // check all corners of patch
	  for (int id2=0;id2<2 && !doit;id2++) {
	    IntVect ivt(iv);
	    ivt.shift(0,id1);
	    ivt.shift(1,id2);
	    RealVect x(ivt); x *= dx;
	    if (s_ref_type==line_rt) doit = is_on_line(x);
	    else { // plane
	      if (id1+id2==0) ishi = is_above_plane(x);
	      else if (ishi != is_above_plane(x)) doit = true;
	    }
	    if (SpaceDim==3 && !doit) {
	      ivt.shift(2,1);
	      RealVect x(ivt); x *= dx;
	      if (s_ref_type==line_rt) doit = is_on_line(x);
	      else if (ishi != is_above_plane(x)) doit = true; // plane
	    } // 3D
	  } //dirs
	} // dirs
      } // not half, line, or uniform

      if (doit) {
	Box b(iv,iv);
	b.refine(s_refRatio);
	tagVects[ilev] |= b;
      }
    } // grid

    if (!tagVects[ilev].numPts()) exit(13); // debug
  } // level

  // from fine to coarse: coarsen, grow, clip, union
  for (int ilev=nref-1;ilev>=1;ilev--) { 
    TreeIntVectSet ivs = tagVects[ilev+1];
    ivs.coarsen(s_refRatio); // destroys fine tags
    ivs.grow(1);             // patches only need nesting of one
    ProblemDomain dom(a_domains[ilev+s_num_inter]);
    dom.coarsen(s_fine_box_size);
    ivs &= dom;
    tagVects[ilev] |= ivs; // union of old tags    
  }

  // move corse grid to fine with uniform refinment
  if (nUniRef>0) {
    for (int ilev=a_nLevs-1,ii=0;ii<nref;ii++,ilev--) {
      int isrc = ilev - nUniRef;
      for (TreeIntVectSetIterator tit(tagVects[isrc]); tit.ok(); ++tit) {
	IntVect iv = tit();
	Box b(iv,iv);
	b.refine(refFact);
	tagVects[ilev] |= b;
      }
    }
  }
  // uniform refinement of coarse
  for (int ilev=1;ilev<=nUniRef;ilev++) {
    for (TreeIntVectSetIterator tit(tagVects[ilev-1]); tit.ok(); ++tit) {
      IntVect iv = tit();
      Box b(iv,iv);
      b.refine(s_refRatio);
      tagVects[ilev] |= b;
    }
  }

  // create fine grid a_nLevs-1
  {
    Vector<int> procIDs;
    Vector<Box> new_grid(tagVects[a_nLevs-1].numPts());
    int ii=0;
    for (TreeIntVectSetIterator tit(tagVects[a_nLevs-1]); tit.ok(); ++tit) {
      IntVect iv = tit();
      Box b(iv,iv);
      b.refine(s_fine_box_size); // get cells back
      new_grid[ii++] = b;
    }
    LoadBalance(procIDs,new_grid);
    const DisjointBoxLayout newDBL(new_grid,procIDs,a_domains[a_nLevs-1+s_num_inter]);
    a_grids[a_nLevs-1+s_num_inter] = newDBL;
    if (s_verbosity>1) pout() << "\tsetupGrids: create fine grid " << a_nLevs-1 << "/" << a_nLevs-1 << 
			 ") num patches: " << tagVects[a_nLevs-1].numPts() << endl;
  }

  // from fine to coarse: coarsen, grow, clip, union
  for (int ilev=a_nLevs-2;ilev>=1;ilev--) {
    Vector<int> procIDs;
    Vector<Box> new_grid( tagVects[ilev].numPts() );
    int ii=0;
    for (TreeIntVectSetIterator tit(tagVects[ilev]); tit.ok(); ++tit) {
      IntVect iv = tit();
      Box b(iv,iv);
      b.refine(s_fine_box_size); // get cells back
      new_grid[ii++] = b;
    }
    LoadBalance(procIDs,new_grid);
    const DisjointBoxLayout newDBL(new_grid,procIDs,a_domains[ilev+s_num_inter]);
    a_grids[ilev+s_num_inter] = newDBL;
    if (s_verbosity>2) pout() << "\tsetupGrids: create grid " << ilev << "/" << a_nLevs-1 << ") num patches: " << 
			 tagVects[ilev].numPts() << endl;
  }
  if (s_verbosity>1) pout() << "\tsetupGrids: coarse grid " << 0 << "/" << a_nLevs-1 << ") num patches: " << 
		       tagVects[0].numPts() << endl;

  PetscFunctionReturn(0);
}
//
// doit
//
#undef __FUNCT__
#define __FUNCT__ "go"
PetscErrorCode go(int nGrids)
{
  CH_TIMERS("go");
  CH_TIMER("the-solve", t1);
  PetscErrorCode ierr;
  Real fasfullerr[30][2],chomboerr[30][2],amgerr[30][2],fasverr[30][2];
  Real convergeRate[2];
  const Real log2r = 1.0/log(2.0);
  const Real targetConvergeRate = 2*0.9;
  Vector<RefCountedPtr<LevelData<FArrayBox> > > phi, rhs, exact;
  Vector<LevelData<FArrayBox> *> phi2, rhs2, exact2;
  Vector<int> refratios;
  Vector<ProblemDomain> domains;
  Real cdx,dx,resnorm,resnorm0,rtol;
  const int nGhost = s_order/2;
  int status,ilev,nn;
  PetscInt M;
  mfree_amr_ctx ctx;
  KSP  ksp;   /* linear solver context */
  Vec  x, b;  /* approx solution, RHS */
  Mat  P,L;   /* linear system matrix */
  PetscFunctionBeginUser;

  // do all levels so we have errors for all levels - convergance test
  RealVect len(s_iLength);
  if (nGrids%s_inc) MayDay::Error("nGrids%s_inc");
  for (int iMaxLev=0 ; iMaxLev <= nGrids ; iMaxLev += s_inc ) {
    // keep last (real) solve in new stage
#if defined(PETSC_USE_LOG)
    PetscLogEventBegin(s_events[5],0,0,0,0);
#endif
    Vector<DisjointBoxLayout> grids;
    ierr = setupGrids( refratios, domains, grids, iMaxLev+1, s_n_amr_refine,
		       cdx, s_inc ); CHKERRQ(ierr);
    int nLev = grids.size(); CH_assert(nLev==iMaxLev+1+s_num_inter);
    // allocate vectors, set RHS and exact
    phi.resize(nLev);  rhs.resize(nLev);  exact.resize(nLev);
    phi2.resize(nLev); rhs2.resize(nLev); exact2.resize(nLev);
    dx = cdx;
    for (ilev=0; ilev < nLev ; ilev++, dx /= s_refRatio) {
      phi[ilev] = RefCountedPtr<LevelData<FArrayBox> >(new LevelData<FArrayBox>(grids[ilev],1,nGhost*IntVect::Unit));
      rhs[ilev] = RefCountedPtr<LevelData<FArrayBox> >(new LevelData<FArrayBox>(grids[ilev],1,IntVect::Zero));
      exact[ilev] = RefCountedPtr<LevelData<FArrayBox> >(new LevelData<FArrayBox>(grids[ilev],1,IntVect::Zero));
      rhs2[ilev] = &(*rhs[ilev]);
      phi2[ilev] = &(*phi[ilev]);
      exact2[ilev] = &(*exact[ilev]);	  
      const DisjointBoxLayout &dbl = grids[ilev];
      for (DataIterator dit(dbl) ; dit.ok(); ++dit) {
	// set RHS
	FArrayBox& rhsFAB = (*rhs[ilev])[dit];
	Box region = dbl[dit];
	for (BoxIterator bit(region); bit.ok(); ++bit) {
	  IntVect iv = bit();
	  RealVect x(iv); x *= dx; x += 0.5*dx*RealVect::Unit;
 	  if (s_probtype == sinusoidal) {
	    rhsFAB(iv,0) = 0.;
	    for (int i=0; i<nk; i++) {
	      Real fact = sqrt(D_TERM(kx[i]*kx[i],+ky[i]*ky[i],+kz[i]*kz[i]));
	      rhsFAB(iv,0) += fact*(D_TERM(sin(kx[i]*x[0]),
					   *sin(ky[i]*x[1]),
					   *sin(kz[i]*x[2])));
	    }
	  }
	  else if (s_probtype == polynomial) {
	    rhsFAB(iv,0) = rhsFuncPoly(x,len);
	  }
	}
	// set exact, zero phi, whole box
	FArrayBox& exactfab = (*exact[ilev])[dit];
	FArrayBox& phifab = (*phi[ilev])[dit];
	//phifab *= .0; // trigger fp_trap	      
	Box eregion = exactfab.box(); // with one ghost for grad
	for (BoxIterator bit(eregion); bit.ok(); ++bit) {
	  IntVect iv = bit();
	  phifab(iv,0) = 0.;
	  RealVect x(iv); x *= dx; x += 0.5*dx*RealVect::Unit;
	  if (s_probtype == sinusoidal) {
	    exactfab(iv,0) = 0.;
	    for (int i=0; i<nk; i++) {
	      Real fact = sqrt(D_TERM(kx[i]*kx[i],+ky[i]*ky[i],+kz[i]*kz[i]));
	      exactfab(iv,0) += (1./fact)
		*(D_TERM(sin(kx[i]*x[0]),
			 *sin(ky[i]*x[1]),
			 *sin(kz[i]*x[2])));
	    }
	  }
	  else if (s_probtype == polynomial) {
	    exactfab(iv,0) = exactSolutionPoly(x,len);
	  }
	}
      }
    }
    rtol = 1.e-6;
#if defined(PETSC_USE_LOG)
    PetscLogEventEnd(s_events[5],0,0,0,0);
#endif
    // construct Chombos solver
    AMRMultiGrid<LevelData<FArrayBox> > CHamrSolver; // make Chombo for CH and AMG
    AMRPoissonOpFactory CHopFactory;
    BiCGStabSolver<LevelData<FArrayBox> > bottomSolver;
    PetscSolverPoisson<LevelData<FArrayBox> > AMGbottomSolver;
    if (s_chmg || s_amg) {
      bottomSolver.m_verbosity = 1;
      CHopFactory.define(domains[0], grids, refratios, cdx, &ParseBC, 0., -1.);
      // bottom solver
      if (s_chombo_bottom_amg) {
	AMGbottomSolver.define( cdx, true ); // will just inherit the AMG parameters, should be OK
	AMGbottomSolver.m_beta = -1.0;       // solving m_alpha u + m_beta del^2 u = f
	CHamrSolver.define( domains[0], CHopFactory, &AMGbottomSolver, nLev);
      }
      else {
	CHamrSolver.define( domains[0], CHopFactory, &bottomSolver, nLev);
      }
      // need to set for FMG solver -- dummy
      // void AMRMultiGrid< T >::setSolverParameters	(	
      // const int & 	a_pre,
      // const int & 	a_post,
      // const int & 	a_bottom,
      // const int & 	a_numMG,
      // const int & 	a_iterMax,
      // const Real & 	a_eps,
      // const Real & 	a_hang,
      // const Real & 	a_normThresh	 
      // )
      CHamrSolver.init(phi2, rhs2, nLev-1, 0);
      CHamrSolver.setSolverParameters(s_npre,s_npost,1,1,1,1.e-20,1.0e-10,1.0e-30);
      CHamrSolver.m_verbosity = 0;
      // pre solve
      CHamrSolver.solve( phi2, rhs2, nLev-1, 0, true, true );
      // need to set for FMG solver
      CHamrSolver.setSolverParameters( s_npre, s_npost,
				       s_chombo_bottom_amg ? 1 : 40,
				       1,
				       (s_vcycle_type==2) ? 5 : 20,
				       (s_vcycle_type==2) ? 1.e-20 : rtol, // hack accurate when comparing 
				       1.0e-10,
				       1.0e-30);
      CHamrSolver.m_verbosity = ((s_vcycle_type || s_chombo_bottom_amg) ? 4 : 2); // print all iterations if looking at vycle
    }
    // construct FAS solver
    AMRFAS<LevelData<FArrayBox> > amrFASSolver;
    if (s_fmg || s_vcycle_type) {
      amrFASSolver.m_pre = s_npre;
      amrFASSolver.m_post = s_npost;
      amrFASSolver.m_verbosity = 2;
      s_fas_verbosity = 0; // this is in AMRFASI.H - yuck
      amrFASSolver.setAvoidNorms(false);
      amrFASSolver.setCycleType(FAS_FULL);
      amrFASSolver.m_max_iter = 1; // could add some V-cycles 
      // solving poisson operator
      FASPoissonOpFactory opFactory( s_order );
      opFactory.setSmootherType( s_smoothertype );
      opFactory.setSmoothingDampingFactor( s_damping_factor );
      opFactory.setFMGProlOrderP( 1 ); 
      opFactory.setProlOrderP( 0 ); // 0 seems to work fine
      opFactory.setBoxStencil( s_boxstencil );
      opFactory.define( &ParseBC, 0., -1. );
      amrFASSolver.define(domains[0], cdx*RealVect::Unit, grids, refratios, opFactory);
      // pre solve
      amrFASSolver.solve(phi,rhs,&resnorm,&resnorm0);
    }
    // AMG
    PetscCompGridPois petscop(0.,-1.,s_order);
    /* ConstDiriBC */
    RefCountedPtr<ConstDiriBC> bcfunc = RefCountedPtr<ConstDiriBC>(new ConstDiriBC(2,IntVect::Unit));
    BCHolder bc(bcfunc);
    if (s_amg) {
      PetscInt m, n, N;
      //char str[] = "-mg_levels_ksp_max_it 2  ";

      petscop.setCornerStencil( true ); // hard wire for 27 poi
      petscop.setMatlab( false );
      petscop.m_num_extra_nnz = s_n_extra_nnz;
      //petscop.m_repartition = s_repartition;
      petscop.setRepartition( s_repartition ? true : false );
      // form matrix
      petscop.define(domains[0],grids,refratios,bc,cdx*RealVect::Unit);
      petscop.setVerbose(2);
      ierr = petscop.createMatrix(); CHKERRQ(ierr);
      P = petscop.getMatrix();

      // make matrix free shell
      ierr = MatViewFromOptions(P,NULL,"-mat_view");CHKERRQ(ierr);
      ierr = MatGetSize(P, &M, &N);CHKERRQ(ierr); CH_assert(M == N);
      ierr = MatGetLocalSize(P, &m, &n);CHKERRQ(ierr);
      ierr = MatCreateShell(PETSC_COMM_WORLD,m,n,N,N,(void*)&ctx,&L);CHKERRQ(ierr);
      ierr = MatShellSetOperation(L,MATOP_MULT,(void(*)(void))apply_mfree);

      //allocate space for a vector and a matrix-vector product in Chombo-land
      ctx.m_phi_mfree = &phi2;
      ctx.m_Lphi_mfree = &rhs2;
      ctx.m_phi_ref = &phi;
      ctx.m_Lphi_ref = &rhs;
      ctx.m_compgrid = &petscop;
      ctx.m_chmg = &CHamrSolver; // this needs to have been setup!

      // solve
#if PETSC_VERSION_LT(3,5,4)
      ierr = MatGetVecs(P,&x,&b); CHKERRQ(ierr);
#else
      ierr = MatCreateVecs(P,&x,&b); CHKERRQ(ierr);
#endif
      ierr = KSPCreate(PETSC_COMM_WORLD, &ksp); CHKERRQ(ierr);
      //str[22] = '0' + s_npre; // sets num smooths -- could use less
      //if (s_npre>9) { str[22] = '0' + s_npre/10; str[23] = '0' + s_npre%10;}
      //if (s_npre>99) MayDay::Error("s_npre>99"); 
      //ierr = PetscOptionsInsertString(str); // can this be done with code???
      ierr = KSPSetFromOptions(ksp); CHKERRQ(ierr);
      //ierr = KSPSetOperators(ksp,L,P); CHKERRQ(ierr);
      ierr = KSPSetOperators(ksp,P,P); CHKERRQ(ierr);
    }

    // keep last (real) solve in new stage
#if defined(PETSC_USE_LOG)
    if (iMaxLev==nGrids) {
      PetscLogStagePush(s_stage);
      CH_START(t1);
    }
#endif
    if (s_fmg) {
      MPI_Barrier(PETSC_COMM_WORLD);
      for (int ii=0;ii<s_num_repeat;ii++) {
	// solve - zero phi
	for (ilev=0; ilev < nLev ; ilev++) {
	  const DisjointBoxLayout &dbl = grids[ilev];
	  for (DataIterator dit(dbl) ; dit.ok(); ++dit) {
	    FArrayBox& phifab = (*phi[ilev])[dit];
	    phifab *= .0; // trigger fp_trap
	  }
	}
#if defined(PETSC_USE_LOG)
	PetscLogEventBegin(s_events[0],0,0,0,0);
#endif
	status = amrFASSolver.solve(phi,rhs,&resnorm,&resnorm0);
#if defined(PETSC_USE_LOG)
	PetscLogEventEnd(s_events[0],0,0,0,0);
#endif
      }
      PetscPrintf(PETSC_COMM_WORLD,"[%D] FAS solver |r|=%g, |r|/|b|=%g\n",iMaxLev,resnorm,resnorm/resnorm0);      
      
      ierr = plotAll( phi2,rhs2, exact,fasfullerr[iMaxLev],"AMRFAS-FMG.",
		      cdx,grids,refratios,domains,nLev-1); CHKERRQ(ierr);
      
      pout() << iMaxLev << " :FAS-FMG-err= " << fasfullerr[iMaxLev][0] << endl;
      
#ifdef CH_USE_HDF5
      if (s_verbosity > 5) {
	PetscMPIInt nprocs;
	MPI_Comm_size(PETSC_COMM_WORLD,&nprocs);
	if (nprocs==1) {
	  pout() << iMaxLev << " :write residuals : " << amrFASSolver.m_temp.size() << endl;
	  writeLevelname(amrFASSolver.m_temp[amrFASSolver.m_temp.size()-1],"fas_0_res.hdf5");
	  writeLevelname(amrFASSolver.m_temp[amrFASSolver.m_temp.size()-2],"fas_1_res.hdf5");
	  if (amrFASSolver.m_temp.size()>2) writeLevelname(amrFASSolver.m_temp[amrFASSolver.m_temp.size()-3],"fas_2_res.hdf5");
	}
      }
#endif
    }

    // Chombo's AMRMultigrid
    if (s_chmg) {
      MPI_Barrier(PETSC_COMM_WORLD);      
      for (int ii=0;ii<s_num_repeat;ii++) {
	// solve - zero phi
	for (ilev=0; ilev < nLev ; ilev++) {
	  const DisjointBoxLayout &dbl = grids[ilev];
	  for (DataIterator dit(dbl) ; dit.ok(); ++dit) {
	    FArrayBox& phifab = (*phi[ilev])[dit];
	    phifab *= .0; // trigger fp_trap
	  }
	}
#if defined(PETSC_USE_LOG)
	PetscLogEventBegin(s_events[1],0,0,0,0);
#endif
	CHamrSolver.solve( phi2, rhs2, nLev-1, 0, true, true );
#if defined(PETSC_USE_LOG)
	PetscLogEventEnd(s_events[1],0,0,0,0);
#endif
      }

      ierr = plotAll( phi2,rhs2,exact,chomboerr[iMaxLev],"ChomboGMG.",
		      cdx,grids,refratios,domains,nLev-1);CHKERRQ(ierr);

      pout() << iMaxLev << " :Chombo-V-err= " << chomboerr[iMaxLev][0] << endl;
    }

    // FAS V-cycle
    if (s_vcycle_type) {
      amrFASSolver.setCycleType( FAS_VCYCLE );
      amrFASSolver.m_verbosity = 3;
      if (s_vcycle_type == 1) { // normal solve
	amrFASSolver.m_max_iter = 25;
	amrFASSolver.m_rtol = rtol;
      }
      else if (s_vcycle_type == 2) { // fixed 5 iterations
	amrFASSolver.m_max_iter = 5;
	amrFASSolver.m_rtol = 1.e-20;
      }
      MPI_Barrier(PETSC_COMM_WORLD);
      for (int ii=0;ii<s_num_repeat;ii++) {
	// solve - zero phi
	for (ilev=0; ilev < nLev ; ilev++) {
	  const DisjointBoxLayout &dbl = grids[ilev];
	  for (DataIterator dit(dbl) ; dit.ok(); ++dit) {
	    FArrayBox& phifab = (*phi[ilev])[dit];
	    phifab *= .0; // trigger fp_trap
	  }
	}
#if defined(PETSC_USE_LOG)
	PetscLogEventBegin(s_events[3],0,0,0,0);
#endif
	status = amrFASSolver.solve(phi,rhs);
#if defined(PETSC_USE_LOG)
	PetscLogEventEnd(s_events[3],0,0,0,0);
#endif
      }
      ierr = plotAll( phi2,rhs2,exact,fasverr[iMaxLev],"AMRFAS-Vcycles.",
		      cdx,grids,refratios,domains,nLev-1); CHKERRQ(ierr);
      
      pout() << iMaxLev << " :FAS-V-err= " << fasverr[iMaxLev][0] << endl;
    }

    // do AMG
    if (s_amg) {
      // put RHS in
      ierr = petscop.putChomboInPetsc(rhs2,b); CHKERRQ(ierr); // doing this before pre solve messed everyone else up!
      // pre solve - do late because dies at 128K cores
      ierr = KSPSetTolerances(ksp,0.9999,PETSC_DEFAULT,PETSC_DEFAULT,1); CHKERRQ(ierr);
      //ierr = VecSet(b,v); CHKERRQ(ierr);  - this messes everything up ?!
      ierr = KSPSolve(ksp, b, x);
      if (!ierr) {
	if (s_vcycle_type == 2) { // fixed 5 iterations
	  ierr = KSPSetTolerances(ksp,rtol,PETSC_DEFAULT,PETSC_DEFAULT,5); CHKERRQ(ierr);
	}
	else {
	  ierr = KSPSetTolerances(ksp,rtol,PETSC_DEFAULT,PETSC_DEFAULT,50); CHKERRQ(ierr);
	}
	MPI_Barrier(PETSC_COMM_WORLD);
	for (int ii=0;ii<s_num_repeat;ii++) {
#if defined(PETSC_USE_LOG)
	  PetscLogEventBegin(s_events[2],0,0,0,0);
#endif
	  ierr = KSPSolve(ksp, b, x); CHKERRQ(ierr);
#if defined(PETSC_USE_LOG)
	  PetscLogEventEnd(s_events[2],0,0,0,0);
#endif
	}
      }
      ierr = petscop.putPetscInChombo(x,phi2); CHKERRQ(ierr);
      ierr = KSPDestroy(&ksp); CHKERRQ(ierr); 
      ierr = VecDestroy(&x); CHKERRQ(ierr);
      ierr = VecDestroy(&b); CHKERRQ(ierr);
      ierr = MatDestroy(&L); CHKERRQ(ierr);

      ierr = plotAll( phi2,rhs2,exact,amgerr[iMaxLev],"AMG.",
		      cdx,grids,refratios,domains,nLev-1);CHKERRQ(ierr);

      // print total real cells for weak scaling
      nn = pow(s_fine_box_size,SpaceDim);

      pout() << iMaxLev << " :AMG-err= " << amgerr[iMaxLev][0] << " NUM_PATCHES_TOTAL= " << (double)M/(double)nn << endl;

      if (s_verbosity>5) {
	// this does not work -- error at process boundary of coarse grid?
	resnorm = CHamrSolver.computeAMRResidual(exact2, phi2, rhs2, iMaxLev, 1, true, true);
	PetscPrintf(PETSC_COMM_WORLD,"[%D] |r|_AMG=%g, |r|_AMG/|b|=%g\n",0,resnorm,resnorm/resnorm0);
	if (s_verbosity>6) {
	  Real dummy[2];
	  ierr = plotAll( phi2,rhs2,exact,dummy,"AMGRESIDUAL.",
			  cdx,grids,refratios,domains,nLev-1);CHKERRQ(ierr);
	}
      }
    }

    // keep last (real) solve in new stage
#if defined(PETSC_USE_LOG)
    if (iMaxLev==nGrids) {
      PetscLogStagePop();
      CH_STOP(t1);
    }
#endif
  } // 

  pout() << "\nRates (Inf-norm 1-norm) Errors: ";
  if (s_fmg)          pout() << " FAS-F= " << std::setw(8) << fasfullerr[nGrids][0];
  if (s_vcycle_type)  pout() << " |FAS-V= " << std::setw(8) << fasverr[nGrids][0];
  if (s_chmg)         pout() << " |Chombo= " << std::setw(8) << chomboerr[nGrids][0];
  if (s_amg)          pout() << " |CH-AMG= "<< std::setw(8) << amgerr[nGrids][0];
  pout() << endl;
  pout() << "--------------------------------------------------------------------------------------------------------------\n";
  pout() << std::fixed << std::setw(16) << std::setprecision(2);

  for (ilev=0,nn=0,status=0; ilev<nGrids-s_inc; ilev+=s_inc) {
    Real ratio;
    pout() << "                               ";
    // FAS-FMG
    if (s_fmg) {
      for (int j=0;j<2;j++) {
	ratio = fasfullerr[ilev][j]/fasfullerr[ilev+s_inc][j];
	convergeRate[j] = log(ratio)*log2r;
	pout() << "     " << abs(convergeRate[j]);
	if (ilev>2) {
	  nn++;
	  if (convergeRate[j] < targetConvergeRate) status += 1;
	}
      }
    }
    if (s_vcycle_type) {
      pout() << " |";
      for (int j=0;j<2;j++) {
	ratio = fasverr[ilev][j]/fasverr[ilev+s_inc][j];
	convergeRate[j] = log(ratio)*log2r;
	pout() << "     " << abs(convergeRate[j]);
      }	
    }
    if (s_chmg) {
      pout() << " |";
      for (int j=0;j<2;j++) {
	ratio = chomboerr[ilev][j]/chomboerr[ilev+s_inc][j];
	convergeRate[j] = log(ratio)*log2r;
	pout() << "     " << abs(convergeRate[j]);
	if (!s_fmg && ilev>2) {
	  nn++;
	  if (convergeRate[j] < targetConvergeRate) status += 1;
	}
      }
    }
    if (s_amg) {
      pout() << " |";
      for (int j=0;j<2;j++) {
	ratio = amgerr[ilev][j]/amgerr[ilev+s_inc][j];
	convergeRate[j] = log(ratio)*log2r;
	pout() << "     " << abs(convergeRate[j]);
      }
    }
    pout() << endl;
  }
  if (nn) {
    if (status==0) pout() <<  "All " << nn << " FAS FMG tests passed\n";
    else pout() << status << " tests failed of "<< nn << endl;
  }

  PetscFunctionReturn(0);
}
#endif
//
// main
//
int main(int argc, char* argv[])
{
#ifdef CH_USE_PETSC
  PetscInt nlev,coord[3]={1,1,1},dim=SpaceDim;
  PetscErrorCode ierr;
  char str[64] = "uniform", smstr[64] = "chebyshev";
  PetscBool set;
  PetscMPIInt rank,nprocs;

  ierr = PetscInitialize(&argc,&argv,(char*)0,help);CHKERRQ(ierr);
  MPI_Comm_rank(PETSC_COMM_WORLD,&rank);
  MPI_Comm_size(PETSC_COMM_WORLD,&nprocs);

#if defined(PETSC_USE_LOG)
  PetscLogEventRegister("FAS-FMG-solve",0,&s_events[0]);
  PetscLogEventRegister("Chombo-solve",0,&s_events[1]);
  PetscLogEventRegister("PETSc-AMG-solve",0,&s_events[2]);
  PetscLogEventRegister("FAS-Vcycle-solve",0,&s_events[3]);
  PetscLogEventRegister("Plotting",0,&s_events[4]);
  PetscLogEventRegister("Grid setup",0,&s_events[5]);
  PetscLogEventRegister("Apply M-Free",0,&s_events[6]);
  PetscLogStageRegister("The solve", &s_stage);
#endif
  ierr = PetscOptionsGetInt(PETSC_NULL,"-crs_box_size",&s_crs_box_size,PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(PETSC_NULL,"-fine_box_size",&s_fine_box_size,PETSC_NULL);CHKERRQ(ierr);
  if (s_fine_box_size < s_crs_box_size) MayDay::Error("fine_box_size < crs_box_size"); 
  s_num_inter = log2(s_fine_box_size/s_crs_box_size); // assumes rr=2
  ierr = PetscOptionsGetInt(PETSC_NULL,"-n_amr_refine",&s_n_amr_refine,PETSC_NULL);CHKERRQ(ierr);
  //if (s_n_amr_refine==1)  MayDay::Error("can not do one refinment level!!!");
  if (s_n_amr_refine<0) s_n_amr_refine = 0;
  ierr = PetscOptionsGetInt(PETSC_NULL,"-order",&s_order,PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(PETSC_NULL,"-bc_order",&s_bcorder,PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetReal(PETSC_NULL,"-damping_factor",&s_damping_factor,PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(PETSC_NULL,"-verbosity",&s_verbosity,PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(PETSC_NULL,"-chmg",&s_chmg,PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(PETSC_NULL,"-amg",&s_amg,PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(PETSC_NULL,"-fmg",&s_fmg,PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(PETSC_NULL,"-chombo_bottom_amg",&s_chombo_bottom_amg,PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(PETSC_NULL,"-repartition",&s_repartition,PETSC_NULL);CHKERRQ(ierr);

  ierr = PetscOptionsGetInt(PETSC_NULL,"-num_repeat",&s_num_repeat,PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(PETSC_NULL,"-vcycle_type",&s_vcycle_type,PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(PETSC_NULL,"-n_coarse_patches",&s_n_coarse_patches,PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(PETSC_NULL,"-n_inc",&s_inc,PETSC_NULL);CHKERRQ(ierr);
 
  ierr = PetscOptionsGetInt(PETSC_NULL,"-npre",&s_npre,PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetInt(PETSC_NULL,"-npost",&s_npost,PETSC_NULL);CHKERRQ(ierr);
  ierr = PetscOptionsGetBool(PETSC_NULL,"-box_stencil",&s_boxstencil,PETSC_NULL);CHKERRQ(ierr);
  if (s_boxstencil && s_order!=2) MayDay::Error("boxstencil && order!=2");

  nlev = 2;
  ierr = PetscOptionsGetInt(PETSC_NULL,"-nlevels",&nlev,PETSC_NULL);CHKERRQ(ierr);
  if (nlev<0) MayDay::Error("num levels < 0");
  if (nlev>30) MayDay::Error("num levels > 30");

  set = PETSC_FALSE;
  ierr = PetscOptionsGetBool(PETSC_NULL,"-pc_gamg_repartition",&set,PETSC_NULL);CHKERRQ(ierr);
  if (set) s_n_extra_nnz = 150;
  else s_n_extra_nnz = 0;

  PetscOptionsGetString(PETSC_NULL,"-refinement_type",str,64,&set);
  if (set) {
    if (strcmp(str,"uniform")==0) s_ref_type = uniform_rt; 
    else {
      if (s_n_amr_refine==0)  MayDay::Error("no refinement specified with refinement");
      if (strcmp(str,"half")==0) s_ref_type = half_rt;
      else if (strcmp(str,"line")==0) s_ref_type = line_rt;
      else if (strcmp(str,"plane")==0) s_ref_type = plane_rt;
      else if (strcmp(str,"point")==0) s_ref_type = point_rt;
      else CH_assert(0);
    }
  }

  PetscOptionsGetString(PETSC_NULL,"-smoother_type",smstr,64,&set);
  if (set) {
    if (strcmp(smstr,"chebyshev")==0) s_smoothertype = FAS_CHEBY;
    else if (strcmp(smstr,"richardson")==0) s_smoothertype = FAS_RICH;
    else if (strcmp(smstr,"gsrb")==0) {
      s_smoothertype = FAS_GSRB;
      s_damping_factor = 1.; // avoid this confusion
    }
    else CH_assert(0);
  }

  ierr = PetscOptionsGetIntArray(PETSC_NULL,"-domain_length",coord,&dim,&set);CHKERRQ(ierr);
  s_iLength[0] = coord[0]; s_iLength[1] = coord[1];
  if (SpaceDim>2) s_iLength[2]=coord[2];

  // need to go faster for point & full refinement
  if (s_ref_type!=point_rt || s_n_amr_refine<=nlev) s_inc = 1;

  pout() << nprocs << " proc, " << 
    (SpaceDim==2?(s_boxstencil?"2D 9-point":"2D 5-point"):(s_boxstencil?"3D 27-point":"3D 7-point"))<<
    " stencil, V("<<s_npre<<","<<s_npost<< ") cycles, " <<
    smstr << " smoother, V type: "<< s_vcycle_type << ", BC order: " << s_bcorder << ", " << 
    string(str) << " AMR, " << nlev << " levels" << ", crs box size=" << s_crs_box_size << ", fine box size=" << s_fine_box_size << 
    ", num AMR=" << (s_ref_type==uniform_rt ? 0 : s_n_amr_refine) <<  ", inc=" << s_inc << endl;

  // do it
  ierr = go(nlev); CHKERRQ(ierr);

  CH_TIMER_REPORT();

  ierr = PetscFinalize(); CHKERRQ(ierr);
#else
  printf("need PETSc : USE_PETSC=TRUE");
  exit(99);
#endif
  return 0;
}
